<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Send Notifications</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Send Notifications</h1>

	<div id="body">
		<?php echo validation_errors(); ?>
        
        <?php echo form_open('index.php/api/notify'); ?>
        <h5>Message</h5>
        <input type="hidden" name="device_token" value="APA91bEaYUIrcAcnC1dZq9um7PTk424xuHAVKyFjAie9hjq4J5N4ALKA5LH5zBVcwI32gYhHu_b59ZQ-0PeN0LiTV_VxTyAsmLemtRsTnzEB_zd-juu-WluBMpWxfWaEhQEsq0q4SOmjGbKAyJqUYbf13PPKMtWIxA">
        <textarea name="message" cols="40" rows="10" ></textarea>
        <p>
        <input type="radio" name="device_type" value="android" checked>Android
        <input type="radio" name="device_type" value="iphone">iPhone
        <input type="submit" value="Send" />
        </p>
        
        <?php echo form_close(); ?>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>