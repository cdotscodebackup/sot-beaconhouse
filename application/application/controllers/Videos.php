<?php
//application/controllers/Test.php


// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'libraries/REST_Controller.php';

class Videos extends REST_Controller
{
	
	private $api_key 		= "X5Ne0km7852Q1ykny9FfcIK5y9kVV5v6";
	private $api_secret 	= "Q1X5NeknkyV5v6Vkm78y9FfcI0K5y952";
	private $auth_token;
	
	private $signup_params 	= array('email');
	private $event_id;
	private $schedule_id;
	private $user_id;
	private $user_password;
	private $user_email;
	private $sms_response;
	private $request_id;
	private $email_address;
	private $notification_id;
	private $last_refresh_time;
	
	
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
		
		ini_set('always_populate_raw_post_data', -1);
		// Load api model by default
		$this->load->model('api_model', '', TRUE);
		$this->load->model('event_model', '', TRUE);
		$this->load->model('programs_model', '', TRUE);
		$this->load->model('videos_model', '', TRUE);

        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		
		header('Access-Control-Allow-Origin: *');        
		header("Access-Control-Allow-Headers: api_key, api_secret, device_token, device_identifier, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");        
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");        
		$method = $_SERVER['REQUEST_METHOD'];        
		if ($method == "OPTIONS") {            
			die();        
		}
		//ssl_redirect();	
    }
	
	# List of programs of an event
	public function programs_get() 
	{}
	
	# List of programs of an event
	public function videos_get() 
	{
		/*$db = $this->db->query("INSERT INTO bssdata.event_videos (SCHEDULE_ID, VIDEO_URL, VIDEO_TYPE,STATUS,VIDEO_ID) VALUES ('246','https://youtu.be/KLYsrJLn7Gs','I','Y','11')");
		
		$db = $this->db->query("INSERT INTO bssdata.event_videos (SCHEDULE_ID, VIDEO_URL, VIDEO_TYPE,STATUS,VIDEO_ID) VALUES ('253','https://youtu.be/vYavB2iDWfs','I','Y','12')");
		
		var_dump($db);
		die();*/
		
		$response 	= array();

		//validate api ky 
        //if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			//$this->validate_get(array('event_id', 'user_id'));
			
			$refresh_time = $this->post('last_refresh_time');
			
			$search = $this->input->get('q');
			
			if(empty($search))
			{
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again.';
				$this->response($response, 200);
				die();
			}
			
			//$this->input->get('event_id');
			//$this->input->get('user_id');
			$result = $this->programs_model->get_video_search('13', '25591', $refresh_time);
			
			//$findKey = $this->search_revisions($result,'A Symbiotic Future', 'theme_desc');
			$findKey = $this->search_revisions($result,$search,'test');
			$schedules = array();
			if(count($findKey) > 0)
			{
				foreach($findKey as $gotit)
				{
					$final = $result[$gotit];
					//var_dump($final['schedule_id']);
					$schedules[] = $final['schedule_id'];
				}
			}
			
			//var_dump(implode("','",$schedules));
			//implode("','",$schedules);
			$ids = array_unique($schedules);
			$ids = implode("','",$ids);
			//print_r(array_unique($schedules));
			$datatwo = $this->videos_model->get_search_videos($ids);
			//var_dump($datatwo);			
			//die();
			
			$this->last_refresh_time = $this->programs_model->get_current_db_time();
			
			if($datatwo) {
				$response['result']  = array('videos' => $datatwo, 'last_refresh_time' => $this->last_refresh_time);
				$response['success'] = 1;
				$response['message'] = 'List of video.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']  = array();
				$response['success'] = 1;
				$response['message'] = 'Information Not Yet Available.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
			
		}
		/*else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}*/
	}
	
	public function all_get()
	{
		
		$response 	= array();
		
		$refresh_time = $this->input->get('last_refresh_time');
		//$refresh_time = $this->post_get('last_refresh_time');		
		//$this->validate_post(array('user_id'));
		
		$this->user_id = $this->input->get('user_id');
		
		//$result = $this->api_model->get_events($this->user_id, $refresh_time);
		$result = $this->videos_model->get_all_videos();
		//var_dump($result);
		//die();

		$this->last_refresh_time = $this->api_model->get_current_db_time();
		
		$response['result'] = array('videos' => $result, 'last_refresh_time' => $this->last_refresh_time, 'sources' => $this->api_model->get_sources());
		$response['success'] = 1;
		$response['message'] = 'List of videos.';
		//echo '<pre>';
		//print_r($response);
		//die();
		//echo json_encode($response);
		//die();
		$this->response($response, 200); // 200 being the HTTP response code
	
	}
	
	private function verify_api_key($key, $secret)
	{
	
		if($this->api_key == $key and $this->api_secret == $secret){
			
			return true;
			
		}else{
			
			if($key == 'beams' and $secret == 'app'){
				return true;
			} else {				
				$response['success'] = 0;
				$response['message'] = 'Invalid API key.';
				$this->response($response, 200);
			}
		}
		
		/*if($this->api_key != $key){
			$response['success'] = 0;
			$response['message'] = 'Invalid API key.';
			$this->response($response, 200);
		}else{
			return true;
		}*/
	   
	}
	
	# validate post parameters
	private function validate_post($array_to_validate=array())
	{	
		if($array_to_validate && count($array_to_validate))
		{
			foreach($array_to_validate as $ele){
				if(empty($this->post($ele))){
					$response['success'] = 0;
					$response['message'] = 'Value missing for '.$ele;
					$this->response($response, 200);
				}
			}
		}
	}
	
	public function all()
	{
		
		
		$response 	= array();
			
		//validate api ky 
        //if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$refresh_time = $this->input->get('last_refresh_time');
			//$refresh_time = $this->post_get('last_refresh_time');
			
			//$this->validate_post(array('user_id'));
			
			$this->user_id = $this->input->get('user_id');
			
			$result = $this->event_model->get_events($this->user_id, $refresh_time);
			//var_dump($result);
			//die();
			
			$this->last_refresh_time = $this->api_model->get_current_db_time();
						
			$response['result'] = array('events' => $result, 'last_refresh_time' => $this->last_refresh_time, 'sources' => $this->api_model->get_sources());
			$response['success'] = 1;
			$response['message'] = 'List of events.';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} 
		/*else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 404);
		}*/
	}
	
	// validate post parameters
	private function validate_get($array_to_validate=array())
	{	
		if($array_to_validate && count($array_to_validate)){
			foreach($array_to_validate as $ele){
				if(empty($this->get($ele))){
					$response['success'] = 0;
					$response['message'] = 'Value missing for '.$ele;
					$this->response($response, 200);
				}
			}
		}
	}
	
	function search_revisions($dataArray, $search_value, $key_to_search) {
        // This function will search the revisions for a certain value
        // related to the associative key you are looking for.
        $keys = array();
        foreach ($dataArray as $key => $cur_value) 
		{
			//print_r($key);
			//print_r($cur_value);
			
			foreach($cur_value as $key1 => $value1)
			{
				if(is_array($value1))
				{
					foreach($value1 as $participant_key => $participant_name)
					{
						//var_dump($participant_key);
						if(!empty($participant_name['participant_name']))
						{
							//var_dump($participant_name['participant_name']);
							$participant = strpos($participant_name['participant_name'], $search_value);
							if ($participant === false) 
							{
							} else 
							{
								$keys[] = $key;
							}
						}
					}
					//die();
				} else {
					$pos = strpos($value1, $search_value);
					if ($pos === false) 
					{
						
					} else {
							$keys[] = $key;
					}
					
				}
				
			}
        }
        return $keys;
    }
	
	private function arrays($array)
	{
		
	}
}
?>