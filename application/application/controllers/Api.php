<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Registration
 *
 * This is a registration controller to handle registration process including signup verification, authentication
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Shahzad Rafique
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'libraries/REST_Controller.php';

class Api extends REST_Controller
{
	
	private $api_key 		= "X5Ne0km7852Q1ykny9FfcIK5y9kVV5v6";
	private $api_secret 	= "Q1X5NeknkyV5v6Vkm78y9FfcI0K5y952";
	private $auth_token;
	
	private $signup_params 	= array('email');
	private $event_id;
	private $schedule_id;
	private $user_id;
	private $user_password;
	private $user_email;
	private $sms_response;
	private $request_id;
	private $email_address;
	private $notification_id;
	private $last_refresh_time;
	
	
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
		
		ini_set('always_populate_raw_post_data', -1);
		// Load api model by default
		$this->load->model('api_model', '', TRUE);

        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		
		header('Access-Control-Allow-Origin: *');        
		header("Access-Control-Allow-Headers: api_key, api_secret, device_token, device_identifier, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");        
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");        
		$method = $_SERVER['REQUEST_METHOD'];        
		if ($method == "OPTIONS") {            
			die();        
		}
		//ssl_redirect();	
    }
    
	function app_version_post($device_type = 'android') {
		
		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
			
			$result = $this->api_model->get_app_version($device_type);
			
			$response['result']  = $result;
			$response['success'] = 1;
			$response['message'] = 'Something went wrong, please try again.';
			$this->response($response, 200);
			
		}
	}
	
	public function insert_user_device_post() 
	{
		
		$response 	= array();
		$post = array();
		//validate parameters 
		$this->validate_post(array('user_id', 'manufacturer', 'model', 'platform','version', 'token'));

		//validate api ky 

        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
	
			$posted_values = $this->post();

			$post['USER_ID'] 		= isset($posted_values['user_id']) ? $posted_values['user_id'] : '';
			$post['MANUFACTURER'] 	= isset($posted_values['manufacturer']) ? $posted_values['manufacturer'] : '';
			$post['MODEL'] 			= isset($posted_values['model']) ? $posted_values['model'] : '';
			$post['PLATFORM'] 		= isset($posted_values['platform']) ? $posted_values['platform'] : '';
			$post['SERIAL'] 		= isset($posted_values['serial']) ? $posted_values['serial'] : '-';
			$post['VERSION'] 		= isset($posted_values['version']) ? $posted_values['version'] : '';
			$post['UUID'] 			= isset($posted_values['uuid']) ? $posted_values['uuid'] : '-';
			$post['TOKEN'] 			= isset($posted_values['token']) ? $posted_values['token'] : ''; 
			$post['STATUS'] 		= isset($posted_values['status']) ? $posted_values['status'] : 'A';
			
			$status = $this->api_model->insert_device_of_user($post);

			if($status) {
					
				$response['result'] = array('status'=> $status);
				$response['success'] = 1;
				$response['message'] = 'User device has been updated successfully.';
				$this->response($response, 200); // 200 being the HTTP response code
				
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong, please try again.';
				$this->response($response, 200);
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong, please try again.';
			$this->response($response, 200);
		}
	}
	
	# Pending Polls & Evaluations
	public function check_pending_poll_eval_count_post() 
	{
		$response 	= array();

    	if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
    	{
			//validate parameters 
			$this->validate_post(array('user_id'));
			
			$this->user_id 	= $this->post('user_id');
			$this->event_id = (null != $this->post('event_id')) 		? $this->post('event_id') : '13';
			
			$result = $this->api_model->get_pending_poll_eval_count($this->user_id, $this->event_id);
			
			$response['result']	 = $result;
			$response['success'] = 1;
			$response['message'] = 'Data has been loaded successfully.';
			$this->response($response, 200); // 200 being the HTTP response code
		}
			
	}
	
	# Event Sources
	public function sources_post() 
	{
		$response 	= array();

    	if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
    	{
			$sources = $this->api_model->get_sources();
			
			$response['result']	 = $sources;
			$response['success'] = 1;
			$response['message'] = 'Your account has been created successfully.';
			$this->response($response, 200); // 200 being the HTTP response code
		}
			
	}
	
	# Update user sources  
	public function update_user_sources_post()
    {
    	$response 	= array();

    	if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
    	{
    		$this->validate_post(array('user_id', 'sources'));

    		$resources = explode(',',$this->post('sources')); 
			
			$this->event_id = (null != $this->post('event_id')) 		? $this->post('event_id') : '';
			
    		$status = $this->api_model->update_sources($resources, $this->post('user_id'), $this->event_id,  $this->post('other_desc'));
				
    		if($status == 1)
    		{
				$response['success'] = 1;
				$response['message'] = 'Your profile sources has been updated successfully.';
				$this->response($response, 200);

    		} else {

    			$response['success'] = 0;
            	$response['message'] = 'Somethig went wrong. Please try again.';
            	$this->response($response, 200);

    		}

    	} else {			

			$response['success'] = 0;
            $response['message'] = 'Somethig went wrong. Please try again.';
            $this->response($response, 200);

        }
    }
	
	# New user signup
    function signup_post()
    {
        $response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
		
			//validate parameters 
			$this->validate_post(array('email'));
		
			$this->user_email = trim($this->post('email'));
			
			$is_unique_email = $this->api_model->verify_email_address($this->user_email);
		
			if($is_unique_email) 
			{
				// Log signup/registration request in raw format
				if($this->post('login_with') == 'email') 
				{
					$this->validate_post(array('email', 'password', 'first_name', 'last_name', 'phone', 'login_with', 'is_associate'));
				}
				
				$device_identifier 		= $this->head('device_identifier');
				$device_token 			= $this->head('device_token');
				
				$request = $this->save_signup_request($this->post(), $device_identifier, $device_token);
				
				if($request AND count($request) > 0) {
					
					$this->user_id 		= $request['user_id'];
					$this->user_email	= $request['email'];
					
					$qr_code_data = array(
										'user_id' => $this->user_id, 
										'data' => '{USER_ID:'.$this->user_id.',USER_NAME:'.$this->post('first_name').' ' . $this->post('last_name').'}'
									);
					
					@$this->generate_qr_code($qr_code_data);
					
					if($this->post('login_with') == 'email') {
						if(null !=$this->post('image')) {
							@$this->saveImage($this->post('image'));
						}
					}
										
					$result = $this->api_model->get_user_info($this->user_id, '');
					
					//$this->send_email($this->email_address, 'MySOT App', $email_text);
					$response['result']	 = array('user' => $result); 
					$response['success'] = 1;
					$response['message'] = 'Your account has been created successfully.';
					$this->response($response, 200); // 200 being the HTTP response code
				} else {
					$response['success'] = 0;
					$response['message'] = serialize($this->post()).' Something went wrong. Please try again.';
					$this->response($response, 200);
				}
			} else {			
				
				if($this->post('login_with') == 'email') {					
					$response['success'] = 0;
					$response['message'] = 'Email address already exists. Please try with a different email address.';
					$this->response($response, 200);
				} else {
					
					$result = $this->api_model->get_user_info('', $this->user_email);

					$response['result']	 = array('user' => $result);
					$response['success'] = 1;
					$response['message'] = 'Your account has been created successfully.';
					$this->response($response, 200); // 200 being the HTTP response code
				}
			}
			
		} else {			
        	$response['result']  = $this->post();
			$response['success'] = 0;
            $response['message'] = 'Somethig went wrong. Please try again.';
            $this->response($response, 200);
        }
    }
	
	function signup_settings_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
			
			$result = $this->api_model->get_signup_settings();
			
			$response['result']  = $result;
			$response['success'] = 1;
			$response['message'] = 'Signup settings.';
			$this->response($response, 200);
			
		} else {			
        	$response['result']  = $this->post();
			$response['success'] = 0;
            $response['message'] = 'Somethig went wrong. Please try again.';
            $this->response($response, 200);
        }
	}
	
	# Complete profile information
	public function complete_profile_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('user_id', 'first_name', 'last_name', 'phone', 'is_associate',  'age_group_id', 'profile_update_status'));
			
			$this->user_id = $this->post('user_id');
						
			$data = array(
						'FIRST_NAME' 		=> $this->post('first_name'),
						'LAST_NAME' 		=> $this->post('last_name'),
						'GENDER' 			=> $this->post('gender'),
						'DOB'				=> $this->post('date_of_birth'),
						'AGE_GROUP_ID'		=> $this->post('age_group_id'),
						'IMAGE'				=> $this->post('image'),
						'PHONE' 			=> $this->post('phone'),
						'CITY_ID' 			=> $this->post('city_id'),
						'LOCATION_ID' 		=> $this->post('location_id'),
						'OTHER_LOCATION'	=> $this->post('other_location'),
						'BSS_AFFILIATE'		=> $this->post('is_associate'),
						'ASSOCIATION_ID'	=> $this->post('association_id'),
						'OTHER_ASSOCIATION'	=> $this->post('other_association'),
						'SOURCES'			=> $this->post('sources'),
						'OTHER_DESC'		=> $this->post('other_source'),
						
						'PROFILE_UPDATE_STATUS'		=> $this->post('profile_update_status')
					);
			
			if($this->post('image') && $this->post('image') != '') 
			{
				@$this->saveImage($this->post('image'));
			}
			
			$qr_code_data = array(
								'user_id' => $this->user_id, 
								'data' => '{USER_ID:'.$this->user_id.',USER_NAME:'.$this->post('first_name').' ' . $this->post('last_name').'}'
							);
			
			@$this->generate_qr_code($qr_code_data);
			
			$status = $this->api_model->complete_profile($this->user_id, $data);
					
			if($status) {
				
				$response['result']	 = array('user' => $this->api_model->get_user_info($this->user_id, ''));
				$response['success'] = 1;
				$response['message'] = 'Profile information has been updated successfully..';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again later.';
				$this->response($response, 200);
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again later.';
			$this->response($response, 200);
		}
	}
	
	# App settings
	function app_settings_post() 
	{		
		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
			
			$this->validate_post(array('user_id'));
			
			$result = $this->api_model->get_app_settings($this->post('user_id'));
			
			$response['result']  = $result;
			$response['success'] = 1;
			$response['message'] = 'Event app settings.';
			$this->response($response, 200);
			
		}
	}
	
	# Save app settings
	public function save_app_settings_post() 
	{		
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
			
			//validate parameters 
			$this->validate_post(array('user_id'));
			
			$this->user_id	= $this->post('user_id');
			
			$data = array(
					  	'ALLOW_ADD_NETWORK'				=> $this->post('allow_add_network'),
					  	'RECEIVE_NOTIFICATION_SMS'		=> $this->post('receive_notification_sms'),
					 	'RECEIVE_NOTIFICATION_EMAIL'	=> $this->post('receive_notification_email'),
					  	'RECEIVE_NOTIFICATION_APP'		=> $this->post('receive_notification_app'),
					  	'SESSION_REMINDER_ALERT'		=> $this->post('session_reminder_alert')
					);
					
			$result = $this->api_model->save_app_settings($this->user_id, $data);

			if($result) {
	
				$response['result']  = array('app_settings' => $result);
				$response['success'] = 1;
				$response['message'] = 'Settings has been updated successfully.';
				$this->response($response, 200); // 200 being the HTTP response code
				
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again.';
				$this->response($response, 200);
			}			
		}
	}
	
	function signup_x_post()
    {
        $response 	= array();
		
		//validate parameters 
		// this function will validate the values of paramerter required for user varification
		$this->validate_post($this->signup_params);

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
		
			
			$this->user_email = trim($this->post('email'));
			
			$is_unique_email = $this->api_model->verify_email_address($this->user_email);
		
			if($is_unique_email) {
				// Log signup/registration request in raw format
				$request = $this->save_signup_request($this->post());
				
				if($request AND count($request) > 0) {
					
					$this->user_id 		= $request['user_id'];
					$this->user_email	= $request['email'];
					
					$qr_code_data = array(
										'user_id' => $this->user_id, 
										'data' => '{USER_ID:'.$this->user_id.',USER_NAME:'.$this->post('first_name').' ' . $this->post('last_name').'}'
									);
					
					@$this->generate_qr_code($qr_code_data);
					
					if($this->post('login_with') == 'email') {
						if($this->post('image')) {
							@$this->saveImage($this->post('image'));
						}
					}
										
					$email_text = "Thank you for signing up with MySOT App.";
					
					//$this->send_email($this->email_address, 'MySOT App', $email_text);
					$response['result']	 = array('user' => $result); 
					$response['success'] = 1;
					$response['message'] = 'Signup successfull.';
					$this->response($response, 200); // 200 being the HTTP response code
				} else {
					$response['success'] = 0;
					$response['message'] = serialize($this->post()).' Something went wrong. Please try again.';
					$this->response($response, 200);
				}
			} else {			
				
				if($this->post('login_with') == 'email') {					
					$response['success'] = 0;
					$response['message'] = 'Email address already exists. Please try with a different email address.';
					$this->response($response, 200);
				} else {
					$response['success'] = 1;
					$response['message'] = 'Signup successfull.';
					$this->response($response, 200); // 200 being the HTTP response code
				}
			}
			
		} else {			
        	$response['result']  = $this->post();
			$response['success'] = 0;
            $response['message'] = 'Somethig went wrong. Please try again.';
            $this->response($response, 200);
        }
    }
	
	# Log new user signup request
	private function save_signup_request($data_post = array(), $device_identifier = '', $device_token = '')
	{
		$post = array();
		
		if($data_post && count($data_post) > 0){
						
			/*foreach($data_post as $ele => $val){
					
					$post[strtoupper($ele)] = $val; //$this->post($ele); echo '<br>';
			}*/
			
			$genders = array('male' => 'M', 'M' => 'M', 'female' => 'F', 'F' => 'F', 'other' => 'O', 'O' => 'O');

			$post['EMAIL'] 				= isset($data_post['email']) ? $data_post['email'] : '';
			$post['PASSWORD'] 			= isset($data_post['password']) ? $data_post['password'] : '';
			$post['FIRST_NAME'] 		= isset($data_post['first_name']) ? $data_post['first_name'] : '';
			$post['LAST_NAME'] 			= isset($data_post['last_name']) ? $data_post['last_name'] : '';
			$post['DOB'] 				= isset($data_post['date_of_birth']) ? $data_post['date_of_birth'] : '';
			$post['AGE_GROUP_ID'] 		= isset($data_post['age_group_id']) ? $data_post['age_group_id'] : '';
			$post['GENDER'] 			= isset($data_post['gender']) ? $data_post['gender'] : '';
			$post['PHONE'] 				= isset($data_post['phone']) ? $data_post['phone'] : '';
			$post['IMAGE'] 				= isset($data_post['image']) ? $data_post['image'] : '';
			$post['LOGIN_WITH'] 		= isset($data_post['login_with']) ? $data_post['login_with'] : '';
			$post['CITY_ID'] 			= isset($data_post['city_id']) ? $data_post['city_id'] : '';
			$post['LOCATION_ID'] 		= isset($data_post['location_id']) ? $data_post['location_id'] : '';
			$post['OTHER_LOCATION'] 	= isset($data_post['other_location']) ? $data_post['other_location'] : '';
			$post['BSS_AFFILIATE']		= isset($data_post['is_associate']) ? $data_post['is_associate'] : '';
			$post['ASSOCIATION_ID'] 	= isset($data_post['association_id']) ? $data_post['association_id'] : '';
			$post['OTHER_ASSOCIATION'] 	= isset($data_post['other_association']) ? $data_post['other_association'] : '';
			$post['SOURCES']			= isset($data_post['sources']) ? $data_post['sources'] : '';
			$post['OTHER_DESC']			= isset($data_post['other_source']) ? $data_post['other_source'] : '';
			$post['PROFILE_UPDATE_STATUS']	= (null !=$this->post('profile_update_status')) 		? $this->post('profile_update_status') : 'N';
			$post['CREATE_IP']			= $this->input->ip_address();
		}
			
		$result = $this->api_model->save_signup_request($post, $device_identifier, $device_token);

		if($result) {
			
			return $result;
			
		} else {
			//$response['result'] = $this->post();
			$response['success'] = 0;
			$response['message'] = 'Something went wrong, please try again.';
			$this->response($response, 200);
		}
	}
	
	# User Login
	public function user_login_post() 
	{		
		$response 	= array();
		
		//validate parameters 
		$this->validate_post(array('email', 'password'));
		//$this->validate_head(array('device_identifier', 'device_token'));
		
		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
		
			
			$this->user_email 		= $this->post('email');
			$this->user_password	= $this->post('password');
			
			$device_identifier 		= $this->head('device_identifier');
			$device_token 			= $this->head('device_token');
			$device_type			= (null != $this->head('device_type')) 		? $this->head('device_type') : 'Android';

			$result = $this->api_model->get_user_login($this->user_email, $this->user_password, $device_identifier, $device_token, $device_type);
			
			if($result) {
					
				$response['result'] = array('user'=> $result);
				$response['success'] = 1;
				$response['message'] = 'Login successfull.';
				$this->response($response, 200); // 200 being the HTTP response code
				
			} else {
				$response['success'] = 0;
				$response['message'] = 'Please enter a valid email or password.';
				$this->response($response, 200);
			}
			
		}
	}
	
	# User Login
	public function user_login_log_post() 
	{		
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
		
			//validate parameters 
			$this->validate_post(array('user_id'));
			$this->validate_head(array('device_identifier', 'device_token'));
			
			$this->user_id 			= $this->post('user_id');
			
			$device_identifier 		= $this->head('device_identifier');
			$device_token 			= $this->head('device_token');
			$device_type			= (null !=$this->head('device_type')) 		? $this->head('device_type') : 'Android';
			
			$status = $this->api_model->save_user_login_log($this->user_id, $device_token, $device_identifier, $device_type);
			
			if($status) {
					
				$response['result'] = array('status'=> $status);
				$response['success'] = 1;
				$response['message'] = 'User login log has been saved successfully.';
				$this->response($response, 200); // 200 being the HTTP response code
				
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong...';
				$this->response($response, 200);
			}
			
		}
	}
	
	# User Info
	public function user_info_post() 
	{		
		$response 	= array();
		
		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
		
			//validate parameters 
			$this->validate_post(array('user_id'));
			
			$this->user_id 		= $this->post('user_id');
			
			$result = $this->api_model->get_user_info($this->user_id);
			
			if($result) {
					
				$response['result'] = array('user'=> $result);
				$response['success'] = 1;
				$response['message'] = 'User information loaded successfully.';
				$this->response($response, 200); // 200 being the HTTP response code
				
			} else {
				$response['success'] = 0;
				$response['message'] = 'Please enter a valid email or password.';
				$this->response($response, 200);
			}
			
		}
	}
	
	# Forget Password
	public function forget_password_post() 
	{		
		$response 	= array();
		
		//validate parameters 
		$this->validate_post(array('email'));

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
		
			
			$this->user_email	= $this->post('email');
			
			$result = $this->api_model->get_reset_password($this->user_email);

			if($result AND $result['status'] == true) {
				
				$user_name 	= $result['user_name'];
				$password	= $result['password'];
				
				$email_text = "<b>Dear $user_name,<br><br>
You recently requested to reset your password for your SOT Events App. <br><br>
Your new password is: $password<br><br>
Thanks,<br>
SOT Team</b>";
				
				
				//$this->send_email($this->user_email, 'SOT Events App | New Password', $email_text);
				@$this->api_model->send_email($this->user_email, 'SOT Events App | New Password', $email_text);
	
				//$response['result'] = $result;
				$response['success'] = 1;
				$response['message'] = 'Password has been reset. Please check your email.';
				$this->response($response, 200); // 200 being the HTTP response code
				
			} else {
				$response['success'] = 0;
				$response['message'] = 'Please enter a valid email address.';
				$this->response($response, 200);
			}			
		}
	}
	
	# Change Password
	public function change_password_post() 
	{		
		$response 	= array();
		
		//validate parameters 
		$this->validate_post(array('user_id', 'current_password', 'new_password'));

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
		
			
			$this->user_id		= $this->post('user_id');
			$current_passwod	= $this->post('current_password');
			$new_password		= $this->post('new_password');
			
			$result = $this->api_model->change_password($this->user_id, $current_passwod, $new_password);

			if($result AND $result['status'] == true) {
	
				//$response['result'] = $result;
				$response['success'] = 1;
				$response['message'] = 'Password has been reset.';
				$this->response($response, 200); // 200 being the HTTP response code
				
			} else {
				$response['success'] = 0;
				$response['message'] = 'Invalid current password.';
				$this->response($response, 200);
			}			
		}
	}
	
	# Update profile information
	public function update_profile_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			
			$this->validate_post(array('user_id'));
			
			$this->user_id = $this->post('user_id');
			
			$data = array(
						'FIRST_NAME' 		=> $this->post('first_name'),
						'LAST_NAME' 		=> $this->post('last_name'),
						'GENDER' 			=> $this->post('gender'),
						'DATE_OF_BIRTH'		=> $this->post('date_of_birth'),
						'AGE_GROUP_ID'		=> $this->post('age_group_id'),
						'IMAGE'				=> $this->post('image'),
						'PHONE' 			=> $this->post('phone'),
						'LOCATION_ID' 		=> $this->post('location_id'),
						'OTHER_LOCATION'	=> $this->post('other_location'),
						'ORGANIZATION' 		=> $this->post('organization'),
						'ROLE' 				=> $this->post('role'),
						'BIO' 				=> $this->post('bio'),
						'BLOG_URL' 			=> $this->post('blog_url')
					);
					
			if($this->post('image') && $this->post('image') != '') 
			{
				@$this->saveImage($this->post('image'));
			}
						
			$result = $this->api_model->save_profile_info($this->user_id, $data);
			
			if($result) {
				
				$response['result']	 = array('user' => $result);
				$response['success'] = 1;
				$response['message'] = 'Profile information has been updated successfully..';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again later.';
				$this->response($response, 200);
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again later.';
			$this->response($response, 200);
		}
	}
	
	# Final profile information
	public function final_profile_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			
			$this->validate_post(array('user_id'));
			
			$user_id = $this->post('user_id');
			
			$data = array(
						'ORGANIZATION' 		=> (null !=$this->post('organization')) 	? $this->post('organization') : '',
						'ROLE' 				=> (null !=$this->post('role')) 			? $this->post('role') : '',
						'BIO' 				=> (null !=$this->post('bio')) 				? $this->post('bio') : '',
						'BLOG_URL' 			=> (null !=$this->post('blog_url')) 		? $this->post('blog_url') : ''
					);
			
			$result = $this->api_model->final_profile_info($user_id, $data);
			
			if($result) {
				
				$response['result']	 = array('user' => $result);
				$response['success'] = 1;
				$response['message'] = 'Profile information has been updated successfully..';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again later.';
				$this->response($response, 200);
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again later.';
			$this->response($response, 200);
		}
	}
	
	# List of all events
	public function events_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$refresh_time = $this->post('last_refresh_time');
			
			$this->validate_post(array('user_id'));
			
			$this->user_id = $this->post('user_id');
			
			$result = $this->api_model->get_events($this->user_id, $refresh_time);
			
			$this->last_refresh_time = $this->api_model->get_current_db_time();
						
			$response['result'] = array('events' => $result, 'last_refresh_time' => $this->last_refresh_time, 'sources' => $this->api_model->get_sources());
			$response['success'] = 1;
			$response['message'] = 'List of events.';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of participant of an event
	public function event_participants_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('event_id'));
			
			$refresh_time = $this->post('last_refresh_time');
			
			$event_id = $this->post('event_id');
			
			$participants = $this->api_model->get_event_participants($event_id, $refresh_time);
			
			$roles = $this->api_model->get_program_roles($event_id); 
			
			$this->last_refresh_time = $this->api_model->get_current_db_time();
			
			$response['result'] = array('participants' => $participants, 'participant_roles' => $roles, 'last_refresh_time' => $this->last_refresh_time);
			$response['success'] = 1;
			$response['message'] = 'List of participants.';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of activity types of an event
	public function event_activity_types_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			//$this->validate_post(array('event_id'));
			
			$refresh_time = $this->post('last_refresh_time');
			
			$result = $this->api_model->get_event_activity_types($this->post('event_id'), $refresh_time);
			
			$this->last_refresh_time = $this->api_model->get_current_db_time();
			
			$response['result'] = array('activity_types' => $result, 'last_refresh_time' => $this->last_refresh_time);
			$response['success'] = 1;
			$response['message'] = 'List of activity types.';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# User Polls and Evaluations
	public function user_cron_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{			
			$this->validate_post(array('user_id'));
			
			$this->user_id = $this->post('user_id');
			
			$result = $this->api_model->get_schedule_info($this->user_id);
						
			$response['result'] = array('app_cron_info' => $result);
			$response['success'] = 1;
			$response['message'] = 'User cron info has been loaded.';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	# List of programs of an event
	public function programs_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('event_id', 'user_id'));
			
			$refresh_time = $this->post('last_refresh_time');
			
			$result = $this->api_model->get_programs($this->post('event_id'), $this->post('user_id'), $refresh_time);
			
			$this->last_refresh_time = $this->api_model->get_current_db_time();
			
			if($result) {
				$response['result']  = array('programs' => $result, 'last_refresh_time' => $this->last_refresh_time);
				$response['success'] = 1;
				$response['message'] = 'List of programs.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']  = array();
				$response['success'] = 1;
				$response['message'] = 'Information Not Yet Available.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of timewise programs of an event
	public function programs_timewise_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('event_id', 'user_id'));
			
			$refresh_time = $this->post('last_refresh_time');
			
			$result = $this->api_model->get_programs_timewise($this->post('event_id'), $this->post('user_id'), $refresh_time);
			
			$this->last_refresh_time = $this->api_model->get_current_db_time();
			
			$response['result'] = array('programs' => $result, 'last_refresh_time' => $this->last_refresh_time);
			$response['success'] = 1;
			$response['message'] = 'List of programs.';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Register a user in a program
	public function register_user_program_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('user_id', 'schedule_id', 'activity_date'));
			
			$this->user_id 		= $this->post('user_id');
			$this->schedule_id 	= $this->post('schedule_id');
			$schedule_date 		= $this->post('activity_date');
			
			//$this->last_refresh_time = $this->api_model->get_current_db_time();
			$existing_timeslot = $this->api_model->user_program_timeslot($this->user_id, $this->schedule_id, $schedule_date);
			
			if($existing_timeslot) 
			{
				$response['result']  = array('status' => 0);
				$response['success'] = 0;
				$response['message'] = 'Another program is already registered starting at the same time.';
				$this->response($response, 200);
			}			
			
			$status = $this->api_model->register_user_program($this->user_id, $this->schedule_id, $schedule_date);
			
			if($status) {								
				$response['result'] = array('status' => 1);
				$response['success'] = 1;
				$response['message'] = 'You have successfully registered in this program.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result'] = array('status' => 0);
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again.';
				$this->response($response, 200);				
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Unregister a user from a program
	public function unregister_user_program_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('user_id', 'schedule_id'));
			
			$this->user_id 		= $this->post('user_id');
			$this->schedule_id 	= $this->post('schedule_id');
			
			$status = $this->api_model->unregister_user_program($this->user_id, $this->schedule_id);
			
			if($status) {								
				$response['result'] = array('status' => 1);
				$response['success'] = 1;
				$response['message'] = 'You have successfully un-registered from this program.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result'] = array('status' => 0);
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again.';
				$this->response($response, 200);
				
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of polls of a program
	public function program_polls_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('schedule_id', 'user_id'));
			
			$this->schedule_id 	= $this->post('schedule_id');
			$this->user_id		= $this->post('user_id');
			
			$result = $this->api_model->get_program_polls($this->schedule_id, $this->user_id);
			
			$response['result']  = array('program_polls' => $result);
			$response['success'] = 1;
			$response['message'] = 'List of polls of a program';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Save program poll
	public function save_program_polls_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('user_id', 'poll_id', 'response'));
			
			$result = $this->post('response');
			
			$res 	= array();
			
			foreach($result as $rs) {
				$data = array(
							'USER_ID'		=> $this->post('user_id'),
							'POLL_ID'		=> $this->post('poll_id'),
							'QUESTION_ID'	=> $rs['question_id'],
							'OPTION_ID'		=> $rs['user_selection']
						);	
						
				array_push($res, $data);				
			}
			
			$status = $this->api_model->save_program_polls($res);
			
			if($status) {
				$response['result']	 = array('status' => $status);
				$response['success'] = 1;
				$response['message'] = 'Your response has been saved successfully.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']	 = array('status' => 0);
				$response['success'] = 0;
				$response['message'] = 'Your response could not be saved. Please try again later.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List event evaluation
	public function event_evaluation_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('event_id', 'user_id'));
			
			$this->event_id		= $this->post('event_id');
			$this->user_id 		= $this->post('user_id');
						
			$result = $this->api_model->get_event_evaluation($this->event_id, $this->user_id);
			
			$response['result']  = array('event_evaluation' => $result);
			$response['success'] = 1;
			$response['message'] = 'Event evaluation';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Save event evaluation 
	public function save_event_evaluation_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('user_id', 'evaluation_id', 'response'));
			
			$result = $this->post('response');
			
			$res 	= array();
			
			foreach($result as $rs) {
				$data = array(
							'USER_ID'		=> $this->post('user_id'),
							'EVALUATION_ID'	=> $this->post('evaluation_id'),
							'QUESTION_ID'	=> $rs['question_id'],
							'OPTION_ID'		=> $rs['user_selection']
						);	
						
				array_push($res, $data);				
			}
			
			$status = $this->api_model->save_event_evaluation($res);
			
			if($status) {
				$response['result']	 = array('status' => $status);
				$response['success'] = 1;
				$response['message'] = 'Your response has been saved successfully.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']	 = array('status' => 0);
				$response['success'] = 0;
				$response['message'] = 'Your response could not be saved. Please try again later.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List session evaluation
	public function session_evaluation_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('event_id', 'schedule_id', 'user_id'));
			
			$this->event_id		= $this->post('event_id');
			$this->schedule_id	= $this->post('schedule_id');
			$this->user_id 		= $this->post('user_id');
						
			$result = $this->api_model->get_session_evaluation($this->event_id, $this->schedule_id, $this->user_id);
			
			$response['result']  = array('session_evaluation' => $result);
			$response['success'] = 1;
			$response['message'] = 'Session evaluation';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Save session evaluation 
	public function save_session_evaluation_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('user_id', 'schedule_id', 'evaluation_id', 'response'));
			
			$result = $this->post('response');
			
			$res 	= array();
			
			foreach($result as $rs) {
				$data = array(
							'USER_ID'		=> $this->post('user_id'),
							'EVALUATION_ID'	=> $this->post('evaluation_id'),
							'SCHEDULE_ID'	=> $this->post('schedule_id'),
							'QUESTION_ID'	=> $rs['question_id'],
							'OPTION_ID'		=> $rs['user_selection']
						);	
						
				array_push($res, $data);				
			}
			
			$status = $this->api_model->save_session_evaluation($res);
			
			if($status) {
				$response['result']	 = array('status' => $status);
				$response['success'] = 1;
				$response['message'] = 'Your response has been saved successfully.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']	 = array('status' => 0);
				$response['success'] = 0;
				$response['message'] = 'Your response could not be saved. Please try again later.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of contributions
	public function contributions_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id', 'user_id'));

			$result = $this->api_model->get_contributions($this->post('event_id'), $this->post('user_id'));
			
			if($result) {
				$response['result']  = $result;
				$response['success'] = 1;
				$response['message'] = 'List of event contributions';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']  = array();
				$response['success'] = 1;
				$response['message'] = 'Information Not Yet Available.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Save contributions 
	public function save_contribution_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('user_id', 'event_id', 'type_id', 'cont_title'));
			
			$type_id = $this->post('type_id');
			
			if($type_id == 1) // 1 . Link
			{
				$this->validate_post(array('cont_link'));
				
				$status = $this->api_model->save_contribution_link($this->post());
				
				if($status) {
					$response['result']	 = array('status' => $status);
					$response['success'] = 1;
					$response['message'] = 'Your contribution has been saved successfully.';
					$this->response($response, 200); // 200 being the HTTP response code
				} else {
					$response['result']	 = array('status' => 0);
					$response['success'] = 0;
					$response['message'] = 'Your response could not be saved. Please try again later.';
					$this->response($response, 200); // 200 being the HTTP response code
				}
				
			}
			
			if($type_id == 2) // 2 . Picture
			{
				$this->validate_post(array('cont_picture'));
				
				$result = $this->upload_cnt_picture($this->post('cont_picture'), $this->post('user_id'));
				
				if($result['status']) 
				{
					$status = $this->api_model->save_contribution_picture($this->post(), $result['file_name'], $result['file_path']);
					
					if($status) {
						$response['result']	 = array('status' => $status);
						$response['success'] = 1;
						$response['message'] = 'Your contribution has been saved successfully.';
						$this->response($response, 200); // 200 being the HTTP response code
					} else {
						$response['result']	 = array('status' => 0);
						$response['success'] = 0;
						$response['message'] = 'Your response could not be saved. Please try again later.';
						$this->response($response, 200); // 200 being the HTTP response code
					}
				} else {
					$response['success'] = 0;
					$response['message'] = $result['message'];
					$this->response($response, 200); // 200 being the HTTP response code
				}
			}
			
			if($type_id == 3) // 3 . Video
			{
				//$this->validate_post(array('cont_video'));
				
				$result = $this->upload_cnt_video($this->post('user_id'));
				
				if($result['status']) 
				{
					$status = $this->api_model->save_contribution_video($this->post(), $result['file_name'], $result['file_path']);
					
					if($status) {
						$response['result']	 = array('status' => $status);
						$response['success'] = 1;
						$response['message'] = 'Your information has been saved successfully.';
						$this->response($response, 200); // 200 being the HTTP response code
					} else {
						$response['result']	 = array('status' => 0);
						$response['success'] = 0;
						$response['message'] = 'Your response could not be saved. Please try again later.';
						$this->response($response, 200); // 200 being the HTTP response code
					}
				} else {
					$response['success'] = 0;
					$response['message'] = $result['message'];
					$this->response($response, 200); // 200 being the HTTP response code
				}
			}
			
			$response['success'] = 0;
			$response['message'] = 'You have not selected an appropriate contribution type.';
			$this->response($response, 200);			
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	// Upload contribution image
	private function upload_cnt_picture($base64img, $user_id)
	{
		define('UPLOAD_DIR', 'uploads/contributions/pictures/');
	
		$base64img = str_replace('[removed]', '', $base64img);
		$data = base64_decode($base64img);
		$file_name = $user_id.'_'.time().'.jpg';
		$file = UPLOAD_DIR . $file_name;
		$status = file_put_contents($file, $data);
		
		if($status) {
			return array('status' => true, 'file_name' => $file_name, 'file_path' => base_url().'uploads/contributions/pictures/');
		} else {
			return array('status' => false, 'message' => 'Unable to upload picture. Please try again.');
		}
	}
	
	public function upload_cnt_video($user_id) 
	{		
		$configVideo['upload_path'] 	= 'uploads/contributions/videos'; # check path is correct
		$configVideo['max_size'] 		= '90000';
		$configVideo['allowed_types'] 	= 'mp4|3gp|ogg|mkv|wav|m4a|m4v|mov|wmv'; # add video extenstion on here
		$configVideo['overwrite'] 		= FALSE;
		$configVideo['remove_spaces'] 	= TRUE;
		$video_name = random_string('numeric', 5);
		$configVideo['file_name'] 		= $user_id.'_'.$video_name;
		
		$this->load->library('upload', $configVideo);
		$this->upload->initialize($configVideo);
		
		if (!$this->upload->do_upload('cont_video')) # form input field attribute
		{
			# Upload Failed
			return array('status' => false, 'message' => $this->upload->display_errors());
		}
		else
		{
			# Upload Successfull
			return array('status' => true, 'file_name' => $this->upload->data('file_name'), 'file_path' => base_url().'/'.$configVideo['upload_path'].'/');
			//$url = 'http://wstest.beams.beaconhouse.net/sotevents/uploads/videos'.$video_name;			
		}	
	}
	
	# List user network
	public function user_network_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('user_id'));

			$result = $this->api_model->get_user_network($this->post('user_id'));
			
			$response['result']  = array('my_network' => $result);
			$response['success'] = 1;
			$response['message'] = 'List user network';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Save to user network 
	public function save_user_network_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('user_id', 'add_user_id'));
			
			$status = $result = $this->api_model->save_user_network($this->post('user_id'), $this->post('add_user_id'));
						
			if($status) {
				$response['result']  = array('status' => 1);
				$response['success'] = 1;
				$response['message'] = 'Your request has been sent successfully.';
				$this->response($response, 200);
			} else {
				$response['success'] = 0;
				$response['message'] = 'Your request cannot be sent at the moment. Please try again later.';
				$this->response($response, 200);
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Accept user network request
	public function accept_user_network_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('network_id', 'user_id'));
			
			$status = $result = $this->api_model->accept_user_network($this->post('network_id'), $this->post('user_id'));
						
			if($status) {
				$response['result']  = array('status' => 1);
				$response['success'] = 1;
				$response['message'] = 'Your have accepted network request.';
				$this->response($response, 200);
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again later.';
				$this->response($response, 200);
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Decline user network request
	public function decline_user_network_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('network_id', 'user_id'));
			
			$status = $result = $this->api_model->decline_user_network($this->post('network_id'), $this->post('user_id'));
						
			if($status) {
				$response['result']  = array('status' => 1);
				$response['success'] = 1;
				$response['message'] = 'Your have declined network request.';
				$this->response($response, 200);
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again later.';
				$this->response($response, 200);
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List user search
	public function user_search_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('user_id', 'keyword'));

			$result = $this->api_model->get_user_search($this->post('user_id'), $this->post('keyword'));
			
			$response['result']  = array('user_search' => $result);
			$response['success'] = 1;
			$response['message'] = 'List user network.';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List user messages
	public function user_messages_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('from_user_id', 'to_user_id'));

			$result = $this->api_model->get_user_messages($this->post('from_user_id'), $this->post('to_user_id'));
			
			$response['result']  = array('user_messages' => $result);
			$response['success'] = 1;
			$response['message'] = 'List user messages';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Save user messages 
	public function save_user_messages_post()
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			$this->validate_post(array('from_user_id', 'to_user_id', 'message'));
			
			$status = $this->api_model->save_user_messages($this->post());
			//$result = $this->api_model->get_user_messages($this->post('from_user_id'), $this->post('to_user_id'));
						
			if($status) {
				$response['result']  = array('status' => 1);
				$response['success'] = 1;
				$response['message'] = 'Your message has been sent successfully.';
				$this->response($response, 200);
			} else {
				$response['success'] = 0;
				$response['message'] = 'Your message cannot be sent at the moment. Please try again later.';
				$this->response($response, 200);
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}

	# Media coverage
	public function press_coverage_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_press_coverage($this->post('event_id'));
			
			$response['result']  = array('media_coverage' => $result);
			$response['success'] = 1;
			$response['message'] = 'List of media coverages.';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Event Enntertainments
	public function event_entertainment_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id', 'user_id'));
			
			$this->event_id = $this->post('event_id');
			$this->user_id	= $this->post('user_id');
			
			$result = $this->api_model->get_event_entertainment($this->event_id, $this->user_id);

			$response['result']  = array('entertainments' => $result);
			$response['success'] = 1;
			$response['message'] = 'Entertainment list has been loaded.';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Event Enntertainments
	public function event_entertainment_request_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id', 'entertainment_id', 'user_id'));
			
			$event_id 			= $this->post('event_id');
			$entertainment_id 	= $this->post('entertainment_id');
			$user_id 			= $this->post('user_id');
			
			$status = $this->api_model->save_event_entertainment_request($event_id, $entertainment_id, $user_id);

			$response['result']  = array('status' => $status);
			$response['success'] = 1;
			$response['message'] = 'The request has been submitted successfully.';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of sponsors
	public function sponsors_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_sponsors($this->post('event_id'));
			
			$response['result']  = $result;
			
			$response['success'] = 1;
			$response['message'] = 'List of event sponsors';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of user notifications
	public function user_notifications_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('user_id', 'read_status'));
			
			$notification_id = '';
			
			if($this->post('notification_id')) 
			{
				$notification_id = $this->post('notification_id');
			}
			
			$result = $this->api_model->get_user_notifications($this->post('user_id'), $notification_id, $this->post('read_status'));
			
			if($result) {
				$response['result']  = $result;
				$response['success'] = 1;
				$response['message'] = 'List of user notifications.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']  = array();
				$response['success'] = 1;
				$response['message'] = 'Information Not Yet Available.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Mark user notification as read
	public function user_notification_read_post() 
	{
		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
			
			//validate parameters 
			$this->validate_post(array('user_id', 'notification_id'));
			
			$this->user_id 		= $this->post('user_id');
			$this->notification_id 	= $this->post('notification_id');
			
			$status = $this->api_model->user_notification_read($this->user_id, $this->notification_id, 'Y');

			if($status) {
				$response['result']	 = array('status_read' => 1);
				$response['success'] = 1;
				$response['message'] = 'Notification status updated successfully.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']	 = array('status_read' => 0);
				$response['success'] = 1;
				$response['message'] = 'Notification status cannot be updated.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
		}
	}
	
	# List of exhibitors
	public function exhibitors_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_exhibitors($this->post('event_id'));
			
			$response['result']  = $result;
			
			$response['success'] = 1;
			$response['message'] = 'List of event exhibitors';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of FULLSTEAM
	public function fullsteam_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_fullsteam($this->post('event_id'));
			
			$response['result']  = $result;
			
			$response['success'] = 1;
			$response['message'] = 'List of event FULLSTEAM';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of EVENT TEAMS
	public function event_teams_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_event_teams($this->post('event_id'));
			
			if($result) {
				$response['result']  = array('event_teams' => $result);
				$response['success'] = 1;
				$response['message'] = 'Event teams has been loaded.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']  = array();
				$response['success'] = 1;
				$response['message'] = 'Information Not Yet Available.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of EVENT FAQS
	public function event_faqs_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_event_faqs($this->post('event_id'));
			
			$response['result']  = array('event_faqs' => $result);
			
			$response['success'] = 1;
			$response['message'] = 'List of event FULLSTEAM';
			$this->response($response, 200); // 200 being the HTTP response code
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of happenings
	public function happenings_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_happenings($this->post('event_id'));
			
			if($result) {
				$response['result']  = $result;
				$response['success'] = 1;
				$response['message'] = 'List of event happenings';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']  = array();
				$response['success'] = 1;
				$response['message'] = 'Information Not Yet Available.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of resources
	public function resources_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_resources($this->post('event_id'));
			
			$response['result']  = array('resources' => $result);
			$response['success'] = 1;
			$response['message'] = 'List of event resources';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# List of Venues
	public function venues_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_happenings($this->post('event_id'));
			
			$response['result']  = $result;
			$response['success'] = 1;
			$response['message'] = 'List of event happenings';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Information Desk
	public function info_desk_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			$this->validate_post(array('event_id'));

			$result = $this->api_model->get_info_desk($this->post('event_id'));
			
			if($result) {
				$response['result']  = array('info_desk' => $result);
				$response['success'] = 1;
				$response['message'] = 'Information desk has been loaded.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['result']  = array();
				$response['success'] = 1;
				$response['message'] = 'Information Not Yet Available.';
				$this->response($response, 200); // 200 being the HTTP response code
			}
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# About Us
	public function about_us_post() 
	{		
		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{						
			
			$result = array(
							'name' 		=> 'Kasim Kasuri',
							'desig'		=> 'CE Beaconhouse',
							'desc_1'	=> 'The Beaconhouse group has around 274,000 fulltime students in eight countries and is possibly the largest school network of its kind in the world. Established in November 1975 as the Les Anges Montessori Academy with 19 toddlers, Beaconhouse has since grown into an international network of private schools, imparting distinctive and meaningful learning to students all the way from birth  through its partnership in Pakistan with Gymboree Play &amp; Music to postgraduation, through the Beaconhouse National University in Lahore.',
							'desc_2'	=> 'Of these students, close to 105,000 study at the groups flagship network, the Beaconhouse School System, in Pakistan as well as',
							'desc_3'	=> 'Our Mission',
							'desc_4'		=> 'The Beaconhouse group has around 274,000 fulltime students in eight countries and is possibly the largest school network of its kind in the world.',
							'picture'	=> 'http://www.thepeak.com.my/wp-content/uploads/2016/09/Untitled-Session699415.jpg'
						);
						
			$response['result']  = $result;
			$response['success'] = 1;
			$response['message'] = 'About Beaconhouse';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}	
	}
	
	# Ask a question
	public function event_ask_question_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{			
			$this->validate_post(array('event_id', 'question_title', 'question_detail'));
			
			$data = array(
						'QUESTION_ID'		=> $this->post('question_id'),
						'EVENT_ID'			=> $this->post('event_id'),
						'SCHEDULE_ID'		=> $this->post('schedule_id'),
						'QUESTION_TITLE'	=> $this->post('question_title'),
						'QUESTION_DETAIL'	=> $this->post('question_detail')
					);
			
			if($this->post('participant_id')) 
			{
				$data['PARTICIPANT_ID']	= $this->post('participant_id');
			}
			
			if($this->post('user_id')) 
			{
				$data['USER_ID']	= $this->post('user_id');
			}
			
			if($this->post('schedule_id')) 
			{
				$data['SCHEDULE_ID']	= $this->post('schedule_id');
			}
			
			$status = $this->api_model->save_ask_question($data);
			
			if($status) {				
				$response['success'] = 1;
				$response['message'] = 'Thank You.';
				$this->response($response, 200); // 200 being the HTTP response code
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong. Please try again.';
				$this->response($response, 200); // 200 being the HTTP response code
			}			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	# Speaker's Itinerary
	public function speaker_itinerary_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{			
			$this->validate_post(array('event_id', 'participant_id'));
			
			$result = $this->api_model->get_speaker_itinerary($this->post('event_id'), $this->post('participant_id'));
			
			$response['result']  = $result;
			$response['success'] = 1;
			$response['message'] = 'Speaker itinerary detail';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Something went wrong. Please try again.';
			$this->response($response, 200);
		}
	}
	
	
	public function new_function_post() 
	{
		$response 	= array();

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret')))
		{
			
			
			
			$response['success'] = 1;
			$response['message'] = 'Password has been reset. Please check your email.';
			$this->response($response, 200); // 200 being the HTTP response code
			
		} else {
			$response['success'] = 0;
			$response['message'] = 'Please enter a valid email address.';
			$this->response($response, 200);
		}
	}
	
	# Generate QR Code
	private function generate_qr_code($params = array()) 
	{
		
		$this->load->library('ci_qr_code');
		$this->config->load('qr_code');
		$qr_code_config = array(); 
		$qr_code_config['cacheable'] 	= false; //$this->config->item('cacheable');
		$qr_code_config['cachedir'] 	= $this->config->item('cachedir');
		$qr_code_config['imagedir'] 	= $this->config->item('imagedir');
		$qr_code_config['errorlog'] 	= $this->config->item('errorlog');
		$qr_code_config['ciqrcodelib'] 	= $this->config->item('ciqrcodelib');
		$qr_code_config['quality'] 		= $this->config->item('quality');
		$qr_code_config['size'] 		= 148; //$this->config->item('size');
		$qr_code_config['black'] 		= $this->config->item('black');
		$qr_code_config['white'] 		= $this->config->item('white');

		$this->ci_qr_code->initialize($qr_code_config);

		$image_name = $params['user_id'].'.png';

		//$params['data'] 	= time().'_This QR Code was generated.';
		$params['level'] 	= 'H';
		$params['size'] 	= 10;
		$params['savename'] = FCPATH.$qr_code_config['imagedir'].$image_name;

		//header("Content-Type: image/png"); 
		$this->ci_qr_code->generate($params);
		
		//return true; //base_url().$qr_code_config['imagedir'].$image_name;
			
	}
	
	// Save Profile image
	private function saveImage($base64img)
	{
		define('UPLOAD_DIR', 'uploads/profile/');
		
		if($base64img AND $base64img != '') 
		{
			$base64img = str_replace('[removed]', '', $base64img);
			$data = base64_decode($base64img);
			$file = UPLOAD_DIR . $this->user_id.'.jpg';
			$status = file_put_contents($file, $data);
			
			if($status) {
				return $this->user_id.'.jpg'; //$file;
			} else {
				return $status;
			}
		}
	}
	
	# Set user password
	public function user_password_post() 
	{
		
		$response 	= array();
		
		//validate parameters 
		$this->validate_post(array('user_id', 'sms_number', 'password'));

		//validate api ky 
        if($this->verify_api_key($this->head('api_key'), $this->head('api_secret'))){
		
			
			$this->sms_number 	= $this->post('sms_number');
			$this->user_id 		= $this->post('user_id');
			$this->password		= $this->post('password');
			
			$status = $this->api_model->set_user_password($this->sms_number, $this->user_id, $this->password);
			
			if($status == true) {
					
				$response['result'] = array('status'=> $status);
				$response['success'] = 1;
				$response['message'] = 'Password updated successfull.';
				$this->response($response, 200); // 200 being the HTTP response code
				
			} else {
				$response['success'] = 0;
				$response['message'] = 'Something went wrong, please try again.';
				$this->response($response, 200);
			}
			
		}
	}
	
	# Verify api key
	private function verify_api_key($key, $secret)
	{
	
		if($this->api_key == $key and $this->api_secret == $secret){
			
			return true;
			
		}else{
			
			if($key == 'beams' and $secret == 'app'){
				return true;
			} else {				
				$response['success'] = 0;
				$response['message'] = 'Invalid API key.';
				$this->response($response, 200);
			}
		}
		
		/*if($this->api_key != $key){
			$response['success'] = 0;
			$response['message'] = 'Invalid API key.';
			$this->response($response, 200);
		}else{
			return true;
		}*/
	   
	}	
	
	# validate post parameters
	private function validate_post($array_to_validate=array())
	{
	
		if($array_to_validate && count($array_to_validate)){
			foreach($array_to_validate as $ele){
				if(empty($this->post($ele))){
					$response['success'] = 0;
					$response['message'] = 'Value missing for '.$ele;
					$this->response($response, 200);
				}
			}
		}
	}
	
	# Validate head parameters
	private function validate_head($array_to_validate=array())
	{
	
		if($array_to_validate && count($array_to_validate)){
			foreach($array_to_validate as $ele){
				if(empty($this->head($ele))){
					$response['success'] = 0;
					$response['message'] = 'Value missing for '.$ele;
					$this->response($response, 200);
				}
			}
		}
	}
	
	private function randomNumber($length) {
		$result = '';
	
		for($i = 0; $i < $length; $i++) {
			$result .= mt_rand(0, 9);
		}
	
		return $result;
	}
	
	# Send activation code to new signup user
	private function send_sms($sms_number, $message)
	{
        		
		$mobile = preg_replace("/[^0-9]/","",$sms_number); //preg_replace("/[^0-9]/","", $sms_number);
 
		if(preg_match("#^[0-9]{10,11}$#",$mobile)) {

			$mobile = '92' . ltrim($mobile, 0);
		} 

		$type = "xml"; 
		$id = "beaconhouse"; 
		$pass = "school44";  
		$lang = "English"; 
		$mask = "Beaconhouse";                                                                                                                                               
		
		$to = $mobile; 
		$message = urlencode($message); 
		$data = "id=".$id."&pass=".$pass."&msg=".$message."&to=".$to."&lang=".$lang."&mask=".$mask."&type=".$type;                                                                     
		$ch = curl_init('http://www.sms4connect.com/api/sendsms.php/sendsms/url'); 
		curl_setopt($ch, CURLOPT_POST, true); curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		$result = curl_exec($ch);
		$xml = simplexml_load_string($result);
		$api_response  =             $xml->code;
		curl_close($ch);      
       
		return $api_response;
	}
	
	# Send email
	private function send_email($email_address, $subject, $message, $attachments = '')
	{
		$param_array = array(
							'from_name' 	=> 'SOT Team',			
							'from_email' 	=> 'sot.team@bh.edu.pk',			
							'to_array' 		=> array($email_address),							
							//'bcc_array' 	=> array('samee.ullah@bh.edu.pk'),			
							'subject' 		=> $subject,
							'body' 			=> $body,
							'batch_id' 		=> time(),
							'function_name' => 'SOT Events App'
						);
		
		//@$this->api_model->send_mail_send_grid($param_array);
		@$this->api_model->send_email($email_address, $subject, $message);
		
		return true;
		
		/*$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'ssl://smtp.gmail.com';
		$config['smtp_port'] = '465';
		$config['smtp_user'] = 'beaconhouseapp.noreply@bh.edu.pk'; 
		$config['smtp_pass'] = 'Beacon@4545'; 
		$config['mailtype'] = 'html';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard
	
		// Load email library and passing configured values to email library
		$this->load->library('email', $config);
		
		// Sender email address
		$this->email->from('beaconhouseapp.noreply@bh.edu.pk', 'SOT Events App');
		// Receiver email address.for single email
		$this->email->to($email_address);
		// Subject of email
		$this->email->subject($subject);
		// Message in email
		$this->email->message($message);
		
		if($attachments && $attachments != '') {
			$this->email->attach($attachments);
		}
		// It returns boolean TRUE or FALSE based on success or failure
		if($this->email->send()) {
			return true;
		} else {
			return false;
		}*/
	}	
	
	# Update SMS API response
	private function update_sms_response($request_id, $response)
	{
		
		$this->api_model->update_sms_response($request_id, $response);
	}
	
	public function notify_post() 
	{
		
		$device_token	= $this->post('device_token');
		$device_type 	= $this->post('device_type');
		$message 		= $this->post('message');
		
		$status = '';
		
		//echo $this->notify_android($device_token, $message);
		//echo $this->notify_iphone($device_token, $message);
		//exit;
		switch($device_type) {
			
			case 'android'	: 
					$status = $this->notify_android($device_token, $message);
			break;
			
			case 'iphone'	: 
					$status = $this->notify_iphone($device_token, $message); 
			break;
		}

		$response['status']	 = json_decode($status);
		$response['success'] = 1;
		$response['message'] = 'Success';
		$this->response($response, 200);
	}
	
	private function notify_android($device_token, $message) 
	{
        
		//$apiKey = 'AIzaSyCepSZPqamoJ_oebtXkGqjkkwBChC5dDZk'; //$this->api_key;
		$apiKey = 'AIzaSyAcw04AUCO0pXfqfzSwRONkayGWisFgiPM'; //$this->api_key;
        // Message to be sent
        //$message = "";
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $registration_ids[] = $device_token;

        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => array("message" => $message),
        );

        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

		// Open connection
        $ch = curl_init();

		// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		// Execute post
        $result = curl_exec($ch);

		// Close connection
        curl_close($ch);
        
        return $result;
    }
	
	private function notify_iphone($device_token, $message, $arr = array()) 
	{

		// Put your device token here (without spaces):
		//        $deviceToken = '26c60defdeefe1232c8429ecbcf39452599546f63f451208b95fa68369f3b158';
        $deviceToken = str_replace(array(" ","<",">"), array("","",""), $device_token);

		// Put your private key's passphrase here:
        $passphrase = 'pass';

		// Put your alert message here:
		//        $message = 'My first push notification!';
        $message = $message;

		////////////////////////////////
        if(ENVIRONMENT=='development'){
            $pem_file = 'ck.pem';
        }else{
            $pem_file = 'ck_prod.pem';
        }
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pem_file);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        if(ENVIRONMENT=='development'){
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        }else{
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        }

        if (!$fp){
            $status = false;
        }
		//            exit("Failed to connect: $err $errstr" . PHP_EOL);

		//        echo 'Connected to APNS' . PHP_EOL;

		// Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );
        $body['params'] = /*$arr*/ array(
            'sender'=>1,
            'receiver'=>2,
            'message'=>"my message"
        );

		// Encode the payload as JSON
        $payload = json_encode($body);

		// Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result){
			// echo 'Message not delivered' . PHP_EOL;
            $status = false;
        }else{
			//  echo 'Message successfully delivered' . PHP_EOL;
            $status = true;
        }

		// Close the connection to the server
        fclose($fp);
        return $status;
    }
}