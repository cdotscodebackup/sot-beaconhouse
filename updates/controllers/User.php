<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class User extends REST_Controller
{
    private $api_key = "X5Ne0km7852Q1ykny9FfcIK5y9kVV5v6";
    private $api_secret = "Q1X5NeknkyV5v6Vkm78y9FfcI0K5y952";
    private $auth_token;

    private $signup_params = array('email');
    private $event_id;
    private $schedule_id;
    private $user_id;
    private $user_password;
    private $user_email;
    private $sms_response;
    private $request_id;
    private $email_address;
    private $notification_id;
    private $last_refresh_time;


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        ini_set('always_populate_raw_post_data', -1);
        // Load api model by default
        $this->load->model('api_model', '', TRUE);
        $this->load->model('user_model', '', TRUE);

        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: api_key, api_secret, device_token, device_identifier, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
    }


    public function insert_user($data_post = array(), $device_identifier = '', $device_token = '')
    {
        $post = array();

        if ($data_post && count($data_post) > 0) {
            $genders = array('male' => 'M', 'M' => 'M', 'female' => 'F', 'F' => 'F', 'other' => 'O', 'O' => 'O');

            $post['EMAIL'] = isset($data_post['email']) ? $data_post['email'] : '';
            $post['PASSWORD'] = isset($data_post['password']) ? $data_post['password'] : '';
            $post['FIRST_NAME'] = isset($data_post['first_name']) ? $data_post['first_name'] : '';
            $post['LAST_NAME'] = isset($data_post['last_name']) ? $data_post['last_name'] : '';
            $post['DOB'] = isset($data_post['date_of_birth']) ? $data_post['date_of_birth'] : '';
            $post['AGE_GROUP_ID'] = isset($data_post['age_group_id']) ? $data_post['age_group_id'] : '';
            $post['GENDER'] = isset($data_post['gender']) ? $data_post['gender'] : '';
            $post['PHONE'] = isset($data_post['phone']) ? $data_post['phone'] : '';
            $post['IMAGE'] = isset($data_post['image']) ? $data_post['image'] : '';
            $post['LOGIN_WITH'] = isset($data_post['login_with']) ? $data_post['login_with'] : '';
            $post['CITY_ID'] = isset($data_post['city_id']) ? $data_post['city_id'] : '';
            $post['LOCATION_ID'] = isset($data_post['location_id']) ? $data_post['location_id'] : '';
            $post['OTHER_LOCATION'] = isset($data_post['other_location']) ? $data_post['other_location'] : '';
            $post['BSS_AFFILIATE'] = isset($data_post['is_associate']) ? $data_post['is_associate'] : '';
            $post['ASSOCIATION_ID'] = isset($data_post['association_id']) ? $data_post['association_id'] : '';
            $post['OTHER_ASSOCIATION'] = isset($data_post['other_association']) ? $data_post['other_association'] : '';
            $post['SOURCES'] = isset($data_post['sources']) ? $data_post['sources'] : '';
            $post['OTHER_DESC'] = isset($data_post['other_source']) ? $data_post['other_source'] : '';
            $post['PROFILE_UPDATE_STATUS'] = (null != $this->post('profile_update_status')) ? $this->post('profile_update_status') : 'N';
            $post['CREATE_IP'] = $this->input->ip_address();
        }

        $result = $this->user_model->insert($post, $device_identifier, $device_token);

        if ($result) {

            return $result;

        } else {
            //$response['result'] = $this->post();
            $response['success'] = 0;
            $response['message'] = 'Something went wrong, please try again.';
            $this->response($response, 200);
        }
    }


}
