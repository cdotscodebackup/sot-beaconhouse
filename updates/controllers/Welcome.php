<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('form_validation'));
		ssl_redirect();	
	}
	
	public function index()
	{
		
		//$this->load->view('notification_form_view');
		$this->load->view('feedback_form_view');
	}
	
	public function notify() {
		
		$device_token	= $this->post('device_token');
		$device_type 	= $this->post('device_type');
		$message 		= $this->post('message');
		
		$status = '';
		
		//echo $this->notify_android($device_token, $message);
		//echo $this->notify_iphone($device_token, $message);
		//exit;
		switch($device_type) {
			
			case 'android'	: 
					$status = $this->notify_android($device_token, $message);
			break;
			
			case 'iphone'	: 
					$status = $this->notify_iphone($device_token, $message); 
			break;
		}

		$response['status']	 = json_decode($status);
		$response['success'] = 1;
		$response['message'] = 'Success';
		$this->response($response, 200);
	}
	
	private function notify_android($device_token, $message) {
        
		$apiKey = 'AIzaSyCepSZPqamoJ_oebtXkGqjkkwBChC5dDZk';
		// Project ID: bss-ppa-95111 Project Number: 105314639760

        // Message to be sent
        //$message = "";
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $registration_ids[] = $device_token;

        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => array("message" => $message),
        );

        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

		// Open connection
        $ch = curl_init();

		// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		// Execute post
        $result = curl_exec($ch);

		// Close connection
        curl_close($ch);
        
        return $result;
    }
	
	private function notify_iphone($device_token, $message, $arr = array()) {

		// Put your device token here (without spaces):
		//        $deviceToken = '26c60defdeefe1232c8429ecbcf39452599546f63f451208b95fa68369f3b158';
        $deviceToken = str_replace(array(" ","<",">"), array("","",""), $device_token);

		// Put your private key's passphrase here:
        $passphrase = 'pass';

		// Put your alert message here:
		//        $message = 'My first push notification!';
        $message = $message;

		////////////////////////////////
        if(ENVIRONMENT=='development'){
            $pem_file = 'ck.pem';
        }else{
            $pem_file = 'ck_prod.pem';
        }
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pem_file);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        if(ENVIRONMENT=='development'){
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        }else{
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        }

        if (!$fp){
            $status = false;
        }
		//            exit("Failed to connect: $err $errstr" . PHP_EOL);

		//        echo 'Connected to APNS' . PHP_EOL;

		// Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );
        $body['params'] = /*$arr*/ array(
            'sender'=>1,
            'receiver'=>2,
            'message'=>"my message"
        );

		// Encode the payload as JSON
        $payload = json_encode($body);

		// Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result){
			// echo 'Message not delivered' . PHP_EOL;
            $status = false;
        }else{
			//  echo 'Message successfully delivered' . PHP_EOL;
            $status = true;
        }

		// Close the connection to the server
        fclose($fp);
        return $status;
    }
	public function send_sms($mobile_number, $message){
                
				$type = "xml"; 
                $id = "beaconhouse"; 
                $pass = "school44";  
                $lang = "English"; 
                $mask = "Beaconhouse";                                                                                                                                                             
                
                $to = $mobile_number; 
                $message = urlencode($message); 
                $data = "id=".$id."&pass=".$pass."&msg=".$message."&to=".$to."&lang=".$lang."&mask=".$mask."&type=".$type;                                                                     
                $ch = curl_init('http://www.sms4connect.com/api/sendsms.php/sendsms/url'); 
                curl_setopt($ch, CURLOPT_POST, true); curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
                $result = curl_exec($ch);
                $xml = simplexml_load_string($result);
                $api_response  =             $xml->code;
                curl_close($ch);                
                return $api_response;
	}
	
	public function get_invoice() { 
		
		$data = "invoice_num=2140004996235&system_id=6110000246424&app=Y"; 
		
		$CurlConnect = curl_init();
		curl_setopt($CurlConnect, CURLOPT_URL, 'http://beams.beaconhouse.edu.pk/students/ism/ism_report_invoice_printed_new.php?user_id=4009&company_id=1');
		curl_setopt($CurlConnect, CURLOPT_POST,   1);
		curl_setopt($CurlConnect, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($CurlConnect, CURLOPT_POSTFIELDS, $data);
		$Result = curl_exec($CurlConnect);
		
		$this->load->helper('download');	
		force_download('6110004917764.pdf', $Result); 
		
		
		/*header('Cache-Control: public'); 
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="new.pdf"');
		header('Content-Length: '.strlen($Result));*/
	}
	
	public function email_invoice() { 
		
		$this->load->helper('file');
		$this->load->library('curl');
		
		$data = $this->curl->simple_get('http://beams.beaconhouse.edu.pk/students/ism/ism_report_invoice_printed_new.php?user_id=4009&company_id=1&invoice_num=2140004996235&system_id=6110000246424&app=Y');
		
		
		if ( !write_file("./uploads/" . '6110004917764.pdf', $data)) {
			
			echo 'Unable to write the file';
		} else {
			echo 'File writtensss!'; 
		}
		
		exit;
		
		$data = "invoice_num=6110004917764&system_id=6110000246424&app=Y"; 
		
		$CurlConnect = curl_init();
		curl_setopt($CurlConnect, CURLOPT_URL, 'http://192.168.0.56/students/ism/ism_report_invoice_printed_new.php?user_id=4009&company_id=1');
		curl_setopt($CurlConnect, CURLOPT_POST,   1);
		curl_setopt($CurlConnect, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($CurlConnect, CURLOPT_POSTFIELDS, $data);
		$Result = curl_exec($CurlConnect);
		
		if ( !write_file("./uploads/" . '6110004917764.pdf', $Result)) {
			
			echo 'Unable to write the file';
		} else {
			echo 'File written!'; 
		}
		
	}
}
