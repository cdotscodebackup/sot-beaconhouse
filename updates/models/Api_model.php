<?php

/**
 * API Model
 *
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Model
 * @author		Shahzad Rafique
*/

class Api_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
		
		public function get_app_version($device_type = 'android') {
			
			$result = array();
			
			$query = @$this->db->query("SELECT app_name,
										  version_name,
										  version_code,
										  app_type,
										  app_url,
										  TO_CHAR(release_date, 'dd/mm/yyyy') release_date
										FROM BSSDATA.app_version_log
										WHERE release_date = (SELECT MAX(release_date) FROM app_version_log)");
										//AND lower(app_type) = lower('$device_type')->row();
									 
			if(!$query) {
				//print_r($this->db->error()); exit;
				$this->log_db_error($this->db->error(), 'get_app_version', '');
				
				return 0;
			}
			
			if($query AND $query->num_rows() > 0) 
			{
				$row = $query->row();
			
				$result = array(
						'app_name'		=> $row->APP_NAME,
						'version_name'	=> $row->VERSION_NAME,
						'version_code'	=> $row->VERSION_CODE,
						'app_type'		=> $row->APP_TYPE,
						'app_url'		=> $row->APP_URL,
						'release_date'	=> $row->RELEASE_DATE
					  );
			}

			return $result;
		}
		
		public function get_app_settings($user_id) {
			
			$result = 	array(
							'user_id' 						=> $user_id, 
							'allow_add_network' 			=> 'N', 
							'receive_notification_sms' 		=> 'Y',
							'receive_notification_email' 	=> 'Y',
							'receive_notification_app' 		=> 'Y',
							'session_reminder_alert' 		=> 'Y'
						);
			
			$query = @$this->db->query("SELECT USER_ID,
										  ALLOW_ADD_NETWORK,
										  RECEIVE_NOTIFICATION_SMS,
										  RECEIVE_NOTIFICATION_EMAIL,
										  RECEIVE_NOTIFICATION_APP,
										  SESSION_REMINDER_ALERT
										FROM EVENT_SETTINGS
										WHERE USER_ID = '$user_id'");
									 
			if(!$query) {

				$this->log_db_error($this->db->error(), 'get_app_settings', '');
			}
			
			if($query AND $query->num_rows() > 0) 
			{
				$row = $query->row();
			
				$result = 	array(
								'user_id' 						=> $row->USER_ID, 
								'allow_add_network' 			=> $row->ALLOW_ADD_NETWORK, 
								'receive_notification_sms' 		=> $row->RECEIVE_NOTIFICATION_SMS, 
								'receive_notification_email' 	=> $row->RECEIVE_NOTIFICATION_EMAIL, 
								'receive_notification_app' 		=> $row->RECEIVE_NOTIFICATION_APP, 
								'session_reminder_alert' 		=> $row->SESSION_REMINDER_ALERT 
							);
			} else {
				$db2 = $this->load->database('trans', TRUE);	
				
				$db2->set('USER_ID', $user_id);
				$db2->set('CREATE_DATE', '(SELECT SYSDATE FROM DUAL)', FALSE);
				@$db2->insert('EVENT_SETTINGS');
				$db2->close();
			}

			return $result;
		}
		
		# Save app settings
		public function save_app_settings($user_id, $data) 
		{			
			if($user_id) {
				
				$db2= $this->load->database('trans', TRUE);
					
					
				$db2->set('UPDT_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);					
				$db2->where('USER_ID', $user_id);
				
				$status = @$db2->update("EVENT_SETTINGS", $data);
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'save_app_settings', 'Unable to update app settings.');
				}

			} 
			
			return $this->get_app_settings($user_id);
		}
		
        public function verify_email_address($email)
        {
			$result = array();
			
			if($email) {
				
				$db2= $this->load->database('trans', TRUE);	
				
				$query = @$db2->query("SELECT COUNT(*) cnt  FROM BSSDATA.event_users WHERE trim(email) = '$email'");
				//echo $this->db->last_query(); exit;													
				if(!$query) 
				{
					$this->log_db_error($db2->error(), 'verify_email_address', 'check unique email address');
					
					return false;
				}
				
				if ($query AND $query->row()->CNT == 0) {
					
					return true;
					
				} else {
					return false;
				}
			} else {
				return false;
			}
        }
		
		public function insert_device_of_user($data){
			
			$db2= $this->load->database('trans', TRUE);	
			/*echo "SELECT *
										FROM EVENT_USER_DEVICES
										WHERE USER_ID = ".$data['USER_ID']." AND MANUFACTURER = '".$data['MANUFACTURER']."' AND MODEL = '".$data['MODEL']."' AND TOKEN = '".$data['TOKEN']."'"; exit;*/
			$users = @$db2->query("SELECT *
										FROM EVENT_USER_DEVICES
										WHERE USER_ID = ".$data['USER_ID']." AND MANUFACTURER = '".$data['MANUFACTURER']."' AND MODEL = '".$data['MODEL']."' AND TOKEN = '".$data['TOKEN']."'");
			
			if( $users AND $users->num_rows() > 0 ) 
			{
				$mobile_status = $data['STATUS'];
				$user_id = $data['USER_ID'];
				$manufacturer = $data['MANUFACTURER'];
				$model = $data['MODEL'];
				$token = $data['TOKEN'];


				$db2->set('STATUS', $mobile_status);
				$db2->set('UPDATE_DATE', '(SELECT SYSDATE FROM DUAL)', FALSE);
				$db2->where('USER_ID', $user_id);
				$db2->where('MANUFACTURER', $manufacturer);
				$db2->where('MODEL', $model);
				$db2->where('TOKEN', $token);
				$status = @$db2->update('EVENT_USER_DEVICES');
				//$db2->close();
				
			} else {

				$db2->set('ENTRY_DATE', '(SELECT SYSDATE FROM DUAL)', FALSE);
				$status = @$db2->insert('EVENT_USER_DEVICES', $data);
			}
			
			return $status;	
		}
		
		public function get_user_info($user_id = '', $email = '')
        {
			$result = array();

				$db2= $this->load->database('trans', TRUE);	
				
				$query = @$db2->query("SELECT user_id,
										  NVL(participant_id, 0) participant_id,
										  first_name,
										  last_name,
										  email,
										  phone,
										  TO_CHAR(date_of_birth, 'mm/dd/yyyy') date_of_birth,
										  age_group_id,
										  image,
										  qr,
										  DECODE(upper(gender), 'MALE', 'M', DECODE(upper(gender), 'FEMALE', 'F', DECODE(upper(gender), 'OTHER', 'O', UPPER(GENDER)))) gender,
										  city_id,
										  location_id,
										  other_location,
										  bss_affiliate,
										  association_id,
										  other_association,
										  organization,
										  role,
										  bio,
										  blog_url,
										  login_with,
										  age_group_id,
										  profile_update_status,
										  nvl(is_admin, 'N') is_admin
										FROM BSSDATA.event_users
										WHERE user_id = '$user_id'
										OR email      = '$email'");
				//echo $db2->last_query(); exit;													
				if(!$query) 
				{
					$this->log_db_error($db2->error(), 'get_user_info', '');
					return false;
				}
				
				if ($query AND $query->num_rows() > 0) {
					
					$row = $query->row();
					//$qr_code_url = base_url().'uploads/qr_codes/'.$row->QR;
					
					$qr_code_file = $row->QR;

					if(file_exists(FCPATH.'uploads/qr_codes/'.$row->USER_ID.'.png') == false) {
						
						$qr_code_data = array(
											'user_id' => $row->USER_ID, 
											'data' => '{USER_ID:'.$row->USER_ID.',USER_NAME:'.ucwords($row->FIRST_NAME.' ' . $row->LAST_NAME).'}'
										);
									
						@$this->generate_qr_code($qr_code_data);
						
						$qr_code_file = base_url().'uploads/qr_codes/'.$row->USER_ID.'.png';
					}
					else {
						$qr_code_file = $row->QR;
					}

					$data = array(
								'user_id' 		=> $row->USER_ID,
								'participant_id'=> $row->PARTICIPANT_ID,	
								'first_name' 	=> $row->FIRST_NAME,
								'last_name' 	=> $row->LAST_NAME,
								'email' 		=> $row->EMAIL,
								'date_of_birth'	=> $row->DATE_OF_BIRTH,
								'age_group_id'	=> $row->AGE_GROUP_ID,
								'phone'			=> $row->PHONE,
								'image' 		=> $row->IMAGE,
								'qrcode'		=> $qr_code_file,
								'gender'		=> $row->GENDER,
								'city_id'		=> $row->CITY_ID,
							  	'location_id'	=> $row->LOCATION_ID,
							  	'other_location'=> $row->OTHER_LOCATION,
								'is_associate'	=> $row->BSS_AFFILIATE,
								'association_id'	=> $row->ASSOCIATION_ID,
								'other_association'	=> $row->OTHER_ASSOCIATION,
								'is_admin'		=> $row->IS_ADMIN,
								'sources'			=> $this->get_user_source($row->USER_ID), 
							  	'organization'	=> $row->ORGANIZATION,
							  	'role'			=> $row->ROLE,
							  	'bio'			=> $row->BIO,
							  	'blog_url'		=> $row->BLOG_URL,
								'login_with'	=> $row->LOGIN_WITH,
								'profile_update_status' => $row->PROFILE_UPDATE_STATUS,
								'app_settings'	=> $this->get_app_settings($row->USER_ID),
								'app_cron_info'	=> $this->get_schedule_info($row->USER_ID)
							);
							
					return $data;
					
				} else {
					return false;
				}
			
        }
		
		public function get_schedule_info($user_id)
		{
			$response = array('event_id' => 0, 'cron_time' => 1200, 'polls' => array(), 'evaluations' => array());
			
			if($user_id)
			{
				$query = @$this->db->query("SELECT sc.event_id,
											  sc.user_id,
											  rtrim (xmlagg (xmlelement (e, sc.schedule_id
											  || ',')).extract ('//text()'), ',') schedule_ids
											FROM
											  (SELECT DISTINCT es.schedule_id,
												es.event_id,
												att.user_id
											  FROM event_schedule es,
												event_user_schedule_qr att
											  WHERE es.schedule_id = att.schedule_id
											  AND es.event_id      = att.event_id
											  AND es.event_id      =
												(SELECT e.event_id
												FROM BSSDATA.events e
												WHERE upper(NVL(e.visible_to_mob_app, 'Y')) = 'Y'
												AND UPPER(NVL(e.active, 'N'))               = 'Y'
												AND e.event_catagory                        = 'SOT'
												)
											  AND att.user_id = '$user_id'
											  ) sc
											GROUP BY sc.event_id,
											  sc.user_id");	
				if(!$query)
				{
					$this->log_db_error($this->db->error(), 'get_schedule_info', '');	
				}
				
				if($query AND $query->num_rows() > 0) 
				{
					$row = $query->row();
					
					$event_id 	= $row->EVENT_ID;
					$user_id 	= $row->USER_ID;
					$schedules	= $row->SCHEDULE_IDS;
					
					$response['event_id'] = intval($event_id);
					
					/*$polls = $this->db->query("SELECT poll.*
												FROM
												  (SELECT EP.poll_id,
													EP.poll_name,
													NVL(EP.status, 'N') poll_status,
													EP.schedule_id,
													NVL(RE.user_id, -1) user_id
												  FROM EVENT_POLL EP,
													EVENT_POLL_RESPONSE RE
												  WHERE EP.poll_id               = RE.poll_id (+)
												  AND EP.schedule_id            IN ($schedules)
												  AND upper(NVL(EP.status, 'N')) = 'Y'
												  ) poll
												WHERE poll.user_id < 0");*/
					
					$polls = @$this->db->query("SELECT poll.*
												FROM
												  (SELECT EP.poll_id,
													EP.poll_name,
													NVL(EP.status, 'N') poll_status,
													EP.schedule_id,
													NVL(RE.user_id, -1) user_id
												  FROM EVENT_POLL EP,
													(SELECT DISTINCT USER_ID,
													  POLL_ID
													FROM EVENT_POLL_RESPONSE
													WHERE USER_ID = '$user_id'
													AND poll_id  IN
													  (SELECT poll_id FROM event_poll WHERE schedule_id IN ($schedules)
													  )
													) RE
												  WHERE EP.poll_id               = RE.poll_id (+)
												  AND EP.schedule_id            IN ($schedules)
												  AND upper(NVL(EP.status, 'N')) = 'Y'
												  ) poll
												WHERE poll.user_id < 0");
					if(!$polls)
					{
						$this->log_db_error($this->db->error(), 'get_schedule_info', '');
					}
					
					if($polls AND $polls->num_rows() > 0)
					{
						foreach($polls->result() as $poll)
						{
							$poll_data = 	array(
												'event_id' 		=> $event_id,
												'schedule_id'	=> $poll->SCHEDULE_ID,
												'user_id'		=> $user_id,
												'poll_id'		=> $poll->POLL_ID,
												'poll_status'	=> $poll->POLL_STATUS
											);
							array_push($response['polls'], $poll_data);
						}
						
						$response['cron_time'] = 300;
					}
					
					// Pending Evaluations
					$evaluations = @$this->db->query("SELECT EVALUATION.*
														FROM
														  (SELECT EP.evaluation_id,
															NVL(EP.status, 'N') evaluation_status,
															EP.schedule_id,
															NVL(RE.user_id, -1) user_id
														  FROM EVENT_EVALUATION_SESSIONS EP,
															(SELECT DISTINCT USER_ID,
															  EVALUATION_ID
															FROM EVENT_EVALUATION_RESPONSE
															WHERE USER_ID      = '$user_id'
															AND EVALUATION_ID IN
															  (SELECT EVALUATION_ID
															  FROM EVENT_EVALUATION
															  WHERE schedule_id IN ($schedules)
															  )
															) RE
														  WHERE EP.EVALUATION_ID         = RE.EVALUATION_ID (+)
														  AND EP.schedule_id            IN ($schedules)
														  AND upper(NVL(EP.status, 'N')) = 'Y'
														  ) EVALUATION
														WHERE EVALUATION.user_id < 0");
					if(!$evaluations)
					{
						$this->log_db_error($this->db->error(), 'get_schedule_info', '');
					}
					
					if($evaluations AND $evaluations->num_rows() > 0)
					{
						foreach($evaluations->result() as $evaluation)
						{
							$schedule_id = $evaluation->SCHEDULE_ID;
							
							$show_evaluation = false;
							
							$att = $this->db->query("SELECT DECODE(att.time_in, NULL, 'N', 'Y') time_in,
														  DECODE(att.time_out, NULL, 'N', 'Y') time_out,
														  ROUND(NVL(att.time_out - att.time_in, 0 ) * 24 * 60 )time_spent
														FROM
														  (SELECT
															(SELECT MIN(create_date)
															FROM event_user_schedule_qr
															WHERE user_id       = '$user_id'
															AND schedule_id     = '$schedule_id'
															AND event_id        = '$event_id'
															AND attendance_type = 'IN'
															) time_in,
															(SELECT MAX(create_date)
															FROM event_user_schedule_qr
															WHERE user_id       = '$user_id'
															AND schedule_id     = '$schedule_id'
															AND event_id        = '$event_id'
															AND attendance_type = 'OUT'
															) time_out
														  FROM dual
														  ) att")->row();
							
							if($att->TIME_IN == 'Y' AND $att->TIME_OUT == 'N')
							{
								$show_evaluation = true;
							}
							if($att->TIME_IN == 'Y' AND $att->TIME_OUT == 'Y')
							{
								$show_evaluation = true;
							}
							if($att->TIME_SPENT >= 20)
							{
								$show_evaluation = true;
							}
							
							if($show_evaluation == true)
							{
								$evaluation_data = 	array(
														'event_id' 			=> $event_id,
														'schedule_id'		=> $evaluation->SCHEDULE_ID,
														'user_id'			=> $user_id,
														'evaluation_id'		=> $evaluation->EVALUATION_ID,
														'evaluation_status'	=> $evaluation->EVALUATION_STATUS
													);
								array_push($response['evaluations'], $evaluation_data);
							}
						}
					} 
					
				}
			}
			return $response;
		}
		
		# Generate QR Code
		private function generate_qr_code($params = array()) 
		{
			
			$this->load->library('ci_qr_code');
			$this->config->load('qr_code');
			$qr_code_config = array(); 
			$qr_code_config['cacheable'] 	= false; //$this->config->item('cacheable');
			$qr_code_config['cachedir'] 	= $this->config->item('cachedir');
			$qr_code_config['imagedir'] 	= $this->config->item('imagedir');
			$qr_code_config['errorlog'] 	= $this->config->item('errorlog');
			$qr_code_config['ciqrcodelib'] 	= $this->config->item('ciqrcodelib');
			$qr_code_config['quality'] 		= $this->config->item('quality');
			$qr_code_config['size'] 		= 148; //$this->config->item('size');
			$qr_code_config['black'] 		= $this->config->item('black');
			$qr_code_config['white'] 		= $this->config->item('white');
	
			$this->ci_qr_code->initialize($qr_code_config);
	
			$image_name = $params['user_id'].'.png';
	
			//$params['data'] 	= time().'_This QR Code was generated.';
			$params['level'] 	= 'H';
			$params['size'] 	= 10;
			$params['savename'] = FCPATH.$qr_code_config['imagedir'].$image_name;
	
			//header("Content-Type: image/png"); 
			$this->ci_qr_code->generate($params);
			
			$db2 = $this->load->database('trans', TRUE);
			
			$db2->set('QR', base_url().'uploads/qr_codes/'.$image_name);
			$db2->where('USER_ID', $params['user_id']);
			@$db2->update('EVENT_USERS');
			//return true; //base_url().$qr_code_config['imagedir'].$image_name;				
		}
	
		public function get_reset_password($email) 
		{			
			if($email) {
				
				$db2= $this->load->database('trans', TRUE);
								
				$user = @$db2->query("SELECT user_id, first_name||' '||last_name user_name  FROM EVENT_USERS WHERE email ='$email'"); //->row()->USER_ID;
				
				if(!$user) 
				{
					$this->log_db_error($db2->error(), 'get_reset_password', '');
				}
					
				if($user AND $user->num_rows() > 0) {
					
					$user_id 	= $user->row()->USER_ID;
					$user_name	= $user->row()->USER_NAME;
					
					$password = (int)random_string('numeric', 4);
					//$db2->set('AUTH_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);
					$db2->set('PASSWORD', "(SELECT COMMON.enc_password('$password') FROM DUAL)", FALSE);					
					$db2->where('USER_ID', $user_id);
					
					$status = @$db2->update("EVENT_USERS");
					
					if(!$status) 
					{
						$this->log_db_error($db2->error(), 'get_reset_password', 'Unable to update new password.');
						
						return array('status' => false);
					}					
					return array('status' => true, 'user_name' => $user_name, 'email' => $email, 'password' => $password);	
									
				} else {
					return array('status' => false);
				}
			} else {
				return array('status' => false);
			}
		}
		
		public function change_password($user_id, $current_password, $new_password) 
		{			
			if($user_id) {
				
				$db2= $this->load->database('trans', TRUE);
								
				$user = @$db2->query("SELECT count(*) cnt  FROM EVENT_USERS WHERE user_id ='$user_id' AND password = COMMON.enc_password('$current_password')");
				
				if(!$user) 
				{
					$this->log_db_error($db2->error(), 'change_password', '');
				}
					
				if($user AND $user->row()->CNT > 0) {
					
					$db2->set('PASSWORD', "(SELECT COMMON.enc_password('$new_password') FROM DUAL)", FALSE);					
					$db2->where('USER_ID', $user_id);
					
					$status = @$db2->update("EVENT_USERS");
					
					if(!$status) 
					{
						$this->log_db_error($db2->error(), 'change_password', 'Unable to update new password.');
						
						return array('status' => false);
					}					
					return array('status' => true);	
									
				} else {
					return array('status' => false);
				}
			} else {
				return array('status' => false);
			}
		}
		
		public function save_signup_request($data = array(), $device_identifier = '', $device_token = '')
        {
			// Check if user had already been registered and signup request is from the same device
			// Generate User ID

			$db2= $this->load->database('trans', TRUE);				

			$db2->trans_begin();
							
			$user_id = $db2->query('SELECT EVENT_APP_SEQ_USER_ID.nextVal AS USER_ID FROM dual')->row()->USER_ID;
			//$user_id = $db2->query('SELECT nvl(max(USER_ID),0)+1 USER_ID FROM EVENT_USERS')->row()->USER_ID;

			$sources 	= $data['SOURCES']; 	unset($data['SOURCES']);
			$other_desc	= $data['OTHER_DESC'];	unset($data['OTHER_DESC']);

			$data['USER_ID']	= $user_id;
			$data['QR']			= base_url().'uploads/qr_codes/'.$user_id.'.png';
			
			if($data['LOGIN_WITH'] == 'email') {
				if(isset($data['IMAGE']) && trim($data['IMAGE']) != '') {
					$data['IMAGE'] = base_url().'uploads/profile/'.$user_id.'.jpg?img='.time();
				}
			}
			
			if($data['DOB'] != '') {
				$db2->set('DATE_OF_BIRTH',"to_date('".$data['DOB']."','DD-MM-YYYY')",false);
			}

			if($data['AGE_GROUP_ID'] != '') {
				$db2->set('AGE_GROUP_ID', $data['AGE_GROUP_ID']);
			}
			
			if(isset($data['PASSWORD']) && $data['PASSWORD'] != '') {
				$data['PASSWORD'] = $db2->query("SELECT COMMON.enc_password('".$data['PASSWORD']."') PWD FROM dual")->row()->PWD;
			}
			
			$data['REGISTER_SOURCE'] = 'M';
			
			$status = @$db2->insert('EVENT_USERS', $data);

			if(!$status) 
			{
				$this->log_db_error($db2->error(), 'log_signup_request', '');
				
				return false;
			}
			
			if($sources AND count($sources) > 0)
			{
				foreach($sources as $src) 
				{
					$source = array(
								'USER_ID'		=> $user_id,
								'SOURCE_ID'		=> $src,
								'OTHER_DESC'	=> ($src == 99) ? $other_desc : ''
							  );
					
					@$db2->insert('EVENT_USER_SOURCE', $source);
				}
			}
							
			if($status) {				
				
				$result = array('user_id' => $user_id, 'email' => $data['EMAIL']);
				@$this->func_log_user_event($db2, $user_id);
				
				if ($db2->trans_status() === FALSE)
				{					
					$db2->trans_rollback();
					return false;					
				} 
				else 
				{
					$db2->set('USER_ID', $user_id);
					$db2->set('LOGIN_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);
					$db2->set('IP_ADDRESS', $this->input->ip_address());
					$db2->set('DEVICE_TOKEN', $device_token);
					$db2->set('DEVICE_IDENTIFIER', $device_identifier);
					$db2->set('AUTH_TOKEN', md5($user_id.time()));
					
					@$db2->insert("EVENT_APP_LOGIN_LOG");	
				
					$db2->trans_commit();				
					return  $result; 
				}
				 				
			} else {
				return false;	
			}
        }
		
		public function get_signup_settings() 
		{			
			$response = array('cities' => array());	
			
			$response['associations'] 	= $this->get_associations();
			$response['sources'] 		= $this->get_sources();
			$response['age_groups'] 	= $this->get_age_groups();
			
			$cities = @$this->db->query("SELECT city_id,
										  city_name,
										  country_id,
										  state_id,
										  CASE
											WHEN city_id < 4
											THEN 1
											ELSE 2
										  END sort_order
										FROM common.com_cities
										WHERE country_id = 1
										AND city_id     IN (1, 2, 3, 4, 5, 6, 8, 11, 17)
										ORDER BY sort_order,
										  city_name ASC");
										
			if($cities AND $cities->num_rows() > 0) {
				
				foreach($cities->result() as $city) {
					
					$country_id = $city->COUNTRY_ID;
					$state_id 	= $city->STATE_ID; 
					$city_id 	= $city->CITY_ID;
					
					$data = array(
								'city_id' 	=> $city_id,
								'city_name'	=> $city->CITY_NAME,
								'towns'		=> $this->get_towns($country_id, $state_id, $city_id)
							);	
					
					array_push($response['cities'], $data);
				}
			}
			
			return $response;
			
		}
		
		public function get_age_groups() 
		{
			$response = array();
			
			$age_group = $this->db->query("SELECT
										    age_group_id, age_group
										FROM
										    bssdata.event_age_groups
										WHERE
										    upper(nvl(active_status,'N') ) = 'Y'
										ORDER BY
										    display_order");

			foreach ($age_group->result() as $age)
			{
				$data = array(
							'age_group_id' 	=> $age->AGE_GROUP_ID,
							'age_group'		=> $age->AGE_GROUP
						);
				array_push($response, $data);
			}

			
			return $response;
		}
		
		public function complete_profile($user_id, $data = array())
        {
			// Check if user had already been registered and signup request is from the same device
			// Generate User ID
			$db2= $this->load->database('trans', TRUE);				

			$db2->trans_begin();

			$sources 	= $data['SOURCES']; 	unset($data['SOURCES']);
			$other_desc	= $data['OTHER_DESC'];	unset($data['OTHER_DESC']);
			
			if(isset($data['IMAGE']) && $data['IMAGE'] != '') 
			{
				$data['IMAGE'] = base_url().'uploads/profile/'.$user_id.'.jpg?img='.time();
			} else {
				unset($data['IMAGE']);
			}
			
			$data['QR'] = base_url().'uploads/qr_codes/'.$user_id.'.png';
			
			$db2->where('USER_ID', $user_id);
				
			$status = @$db2->update("EVENT_USERS", $data);
			
				
			if(!$status) 
			{
				$this->log_db_error($db2->error(), 'complete_profile', '');
				
				return false;
			}
			
			$db2->where('USER_ID', $user_id);
			@$db2->delete("EVENT_USER_SOURCE");
		
			foreach($sources as $src) 
			{
				$source = array(
							'USER_ID'		=> $user_id,
							'SOURCE_ID'		=> $src,
							'OTHER_DESC'	=> ($src == 99) ? $other_desc : ''
						  );
				
				@$db2->insert('EVENT_USER_SOURCE', $source);

			}
							
			if($status) {				
				
				@$this->func_log_user_event($db2, $user_id);
				
				if ($db2->trans_status() === FALSE)
				{					
					$db2->trans_rollback();
					return false;
					
				} 
				else 
				{
					$db2->trans_commit();					
					return  true; 
				}
				 				
			} else {
				return false;	
			}
        }
		
		public function save_signup2_request($data = array(), $sources = array(), $other_source = '')
        {
			// Check if user had already been registered and signup request is from the same device
			
			// Generate User ID
			$db2= $this->load->database('trans', TRUE);				
			
			$db2->trans_begin();
							
			$user_id = $db2->query('SELECT NVL(MAX(USER_ID),0)+1 USER_ID  from EVENT_USERS')->row()->USER_ID;

			$data['USER_ID']	= $user_id;
			$data['QR']			= $user_id.'.png';

			$status = @$db2->insert('EVENT_USERS', $data);

			if(!$status) 
			{
				$this->log_db_error($db2->error(), 'log_signup_request', '');
				
				return false;
			}
				
			if($status) {				
				
				$result = array('user_id' => $user_id, 'phone' =>  $data['PHONE'], 'email' => $data['EMAIL']);
				
				if($sources and count($sources) > 0) {
					$data = array();
					foreach($sources as $src) {
						
						$other_desc = ($src == 99) ? $other_source : ''; 
						
						$arr = array('USER_ID' => $user_id, 'SOURCE_ID' => $src, 'OTHER_DESC' => $other_desc);
						
						array_push($data, $arr);							
					}
					
					@$db2->insert_batch('EVENT_USER_SOURCE', $data);
				}
				
				if ($db2->trans_status() === FALSE){
					
					$db2->trans_rollback();
					
					return false;
					
				} else {
					$db2->trans_commit();
					
					return  $result; 
				}
				 				
			} else {
				return false;	
			}
        }
		
		public function get_user_login($email, $password, $identifier = '', $device_token = '', $device_type = 'Android')
        {
			$response = array();
			
			$db2= $this->load->database('trans', TRUE);	
			
			// Verify user_id and password alongwith sms number
			$count = @$db2->query("SELECT USER_ID
									FROM BSSDATA.event_users
									WHERE lower(trim(email)) = lower(trim('$email'))
									AND password             = COMMON.enc_password('$password')");			
			if(!$count) 
			{
				$this->log_db_error($db2->error(), 'get_user_login', '');
				
				return false;
			}
			
			if($count AND $count->num_rows() > 0) {

				/*$db2->set('LAST_LOGIN_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);
				
				$db2->where('USER_ID', $user_id);
				
				$status = @$db2->update("APP_USERS"); 
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'get_user_login', '');
					
					return false;
				}
			
				if($status) {
					return true;
				} else {
					return false;	
				}*/
				
				// Save user login log
				$user_id = $count->row()->USER_ID;
				
				@$this->func_log_user_event($db2, $user_id);
	
				$db2->set('USER_ID', $user_id);
				$db2->set('LOGIN_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);
				$db2->set('IP_ADDRESS', $this->input->ip_address());
				$db2->set('DEVICE_TOKEN', $device_token);
				$db2->set('DEVICE_IDENTIFIER', $identifier);
				$db2->set('AUTH_TOKEN', md5($user_id.time()));
				$db2->set('DEVICE_TYPE', $device_type);
				
				$status = @$db2->insert("EVENT_APP_LOGIN_LOG");				
				
				$result = $this->get_user_info($user_id, $email);
				
				return $result;
				
			} else {
				return false;
			}
        }
		
		public function save_profile_info($user_id, $data = array()) 
		{			
			$db2= $this->load->database('trans', TRUE);	
			
			if($user_id AND count($data) > 0) {
				
				$db2->set('DATE_OF_BIRTH',"to_date('".$data['DATE_OF_BIRTH']."','DD-MM-YYYY')",false);
				
				unset($data['DATE_OF_BIRTH']);
				
				if(isset($data['IMAGE']) && $data['IMAGE'] != '') 
				{
					$data['IMAGE'] = base_url().'uploads/profile/'.$user_id.'.jpg?img='.time();
				} else {
					unset($data['IMAGE']);
				}
				
				$db2->where('USER_ID', $user_id);
				
				$status = @$db2->update("EVENT_USERS", $data);
				//echo $db2->last_query(); exit;
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'save_profile_info', '');
					
					return false;
				} else {
					return $this->get_user_info($user_id);
				}
			} else {
				return false;
			}
		}
		
		public function final_profile_info($user_id, $data = array()) 
		{			
			$db2= $this->load->database('trans', TRUE);	
			
			if($user_id AND count($data) > 0) {
				
				$db2->where('USER_ID', $user_id);
				
				$status = @$db2->update("EVENT_USERS", $data);
				//echo $db2->last_query(); exit;
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'final_profile_info', '');
					
					return false;
				} else {
					return $this->get_user_info($user_id);
				}
			} else {
				return false;
			}
		}
		
		public function get_events($user_id, $refresh_time = '') 
		{
			$response = array();
			
			if($refresh_time AND $refresh_time != '') {
				
				
			}
			
			$events = @$this->db->query("SELECT e.event_id,
										  e.event_title,
										  e.event_desc,
										  e.active,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM event_evaluation
										  WHERE event_id              = e.event_id
										  AND upper(NVL(status, 'N')) = 'Y'
										  ) evaluation_exists,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM event_evaluation
										  WHERE event_id              = e.event_id
										  AND upper(NVL(status, 'N')) = 'Y'
										  ) evaluation_active,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM EVENT_EVALUATION EV,
											EVENT_EVALUATION_RESPONSE EVR
										  WHERE EV.EVALUATION_ID = EVR.EVALUATION_ID
										  AND EV.EVENT_ID        = e.event_id
										  AND EVR.USER_ID        = '$user_id'
										  AND NVL(EVR.schedule_id, 0) = 0
										  ) evaluation_submitted,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM EVENT_USER_SOURCE
										  WHERE EVENT_ID        = e.event_id
										  AND USER_ID        = '$user_id'
										  ) source_updated
										  /*, TO_CHAR(e.start_date, 'ddth Month') start_date,
										  TO_CHAR(e.end_date, 'ddth Month') end_date,
										  TO_CHAR(e.start_date, 'YYYY') event_year,
										  TO_CHAR(e.start_date, 'ddth Month')
										  || ' '
										  || TO_CHAR(e.end_date, 'ddth Month')
										  || ' '
										  || TO_CHAR(e.start_date, 'YYYY') display_date,
										  TO_CHAR(e.start_date, 'dd/mm/yyyy') from_date,
										  TO_CHAR(e.end_date, 'dd/mm/yyyy') to_date*/
										FROM BSSDATA.events e
										WHERE upper(NVL(e.visible_to_mob_app, 'Y')) = 'Y'
										AND e.event_catagory = 'SOT'
										ORDER BY e.event_id DESC");			
			if(!$events) 
			{
				$this->log_db_error($this->db->error(), 'get_events', '');
				
				 return $response;
			}
			
			if($events AND $events->num_rows() > 0) {
				
				foreach ($events->result() as $event)
				{
					$event_id = $event->EVENT_ID;
					
					$event_dates = $this->db->query("SELECT NVL(start_date, event_start_date) start_date,
														  NVL(end_date, event_end_date) end_date,
														  CASE
															WHEN REPLACE(display_date,'-', '') IS NULL
															THEN event_display_date
															ELSE display_date
														  END display_date
														FROM
														  (SELECT TO_CHAR(MIN(es.activity_date), 'ddth Month') start_date,
															TO_CHAR(MAX(es.activity_date), 'ddth Month') end_date,
															TO_CHAR(MIN(es.activity_date), 'YYYY') event_year,
															TO_CHAR(MIN(es.activity_date), 'ddth Month')
															|| ' '
															|| TO_CHAR(MAX(es.activity_date), 'ddth Month')
															|| ' '
															|| TO_CHAR(MIN(es.activity_date), 'YYYY') display_date_,
															TRIM(TO_CHAR(MIN(es.activity_date), 'FmMonth')
															|| ' '
															|| TO_CHAR(MIN(es.activity_date), 'dd')
															|| ' - '
															|| TO_CHAR(MAX(es.activity_date), 'dd')) display_date,
															TO_CHAR(MIN(es.activity_date), 'dd/mm/yyyy') from_date,
															TO_CHAR(MAX(es.activity_date), 'dd/mm/yyyy') to_date,
															TO_CHAR(MIN(e.start_date), 'ddth Month') event_start_date,
															TO_CHAR(MAX(e.end_date), 'ddth Month') event_end_date,
															TO_CHAR(MIN(e.start_date), 'FmMonth')
															|| ' '
															|| TO_CHAR(MIN(e.start_date), 'dd')
															|| ' - '
															|| TO_CHAR(MAX(e.end_date), 'dd') event_display_date
														  FROM bssdata.events e,
															bssdata.event_schedule es
														  WHERE e.event_id = es.event_id (+)
														  AND e.event_id   = '$event_id'
														  )")->row();
					
					/*$taglines = $this->db->query("SELECT COUNT(DISTINCT es.schedule_id) sessions,
													  (SELECT COUNT(DISTINCT p.participant_id)
														  FROM bssdata.event_participant p,
															bssdata.event_participants_shortlist sh,
															bssdata.event_schedule sc,
															bssdata.event_schedule_detail sd
														  WHERE p.participant_id = sh.participant_id
														  AND sh.participant_id  = sd.participant_id
														  AND sh.event_id        = sc.event_id
														  AND sh.event_id        = '$event_id'
														  AND NVL(sh.status,'N') = 'Y'
														  --AND p.publish_profile  = 1
													  ) speakers,
													  COUNT(DISTINCT es.activity_date) days,
													  es.event_id
													FROM BSSDATA.event_schedule es,
													  BSSDATA.event_schedule_detail ed
													WHERE es.schedule_id = ed.schedule_id
													AND es.event_id      = '$event_id'
													GROUP BY es.event_id")->row();*/
					
					$taglines = $this->db->query("SELECT COUNT(DISTINCT es.schedule_id) sessions,
													  (SELECT COUNT(DISTINCT p.participant_id)
													  FROM bssdata.event_participant p,
														bssdata.event_schedule sc,
														bssdata.event_schedule_detail sd
													  WHERE sc.schedule_id = sd.schedule_id
													  AND p.participant_id = sd.participant_id
													  AND sc.event_id      = '$event_id'
													  ) speakers,
													  COUNT(DISTINCT es.activity_date) days,
													  es.event_id
													FROM BSSDATA.event_schedule es,
													  BSSDATA.event_schedule_detail ed
													WHERE es.schedule_id = ed.schedule_id
													AND es.event_id      = '$event_id'
													GROUP BY es.event_id")->row();
													
					$session_tagline = 'No session found.';
					$speaker_tagline = 'No Speaker Found';
					
					if($taglines) {
						$session_tagline = $taglines->SESSIONS . ' sessions in ' . $taglines->DAYS . ' days';
						$speaker_tagline = $taglines->SPEAKERS . ' speakers';
					}
					
					$data = array(
								'event_id' 			=> $event->EVENT_ID,
								'event_title'		=> $event->EVENT_TITLE,
								'event_desc'		=> $event->EVENT_DESC,
								'active'			=> $event->ACTIVE,
								'start_date'		=> $event_dates->START_DATE,
								'end_date'			=> $event_dates->END_DATE,
								'display_date'		=> $event_dates->DISPLAY_DATE,
								'evaluation_exists'	=> $event->EVALUATION_EXISTS,
								'evaluation_active'	=> $event->EVALUATION_ACTIVE,
								'evaluation_submitted' => $event->EVALUATION_SUBMITTED,
								'info_source_updated'  => $event->SOURCE_UPDATED,
								'locations'			=> array(),
								//'programs'			=> $this->get_programs($event_id, $user_id),
								'timeslots'			=> $this->get_programs_timeslots($event_id),
								'activity_types' 	=> $this->get_event_activity_types($event_id),
								'dimensions'		=> $this->get_event_themes($event_id),
								'venues'			=> $this->get_event_venues($event_id),
								'event_dates'		=> array(),
								'session_tagline'	=> $session_tagline,
								'speaker_tagline'	=> $speaker_tagline,
								'fullsteam_desc' 	=> '<p>STEAM, an interdisciplinary spin on STEM that includes an "A" for art, is an integral part of influencing kids\' interest in STEM by allowing kids to explore these subjects through hands-on making. STEAM is a popular movement that was founded by Georgette Yakman in 2007 to promote and integrate design and art in STEM fields. She defined the movement as "Science and Technology, interpreted through Engineering and the Arts, all based in elements of Mathematics."</p><p>The STEAM movement has inspired new school systems, companies and organizations to create new and engaging learning experiences that have captured kids\' interest. STEAM compliments both sides of the brain. By incorporating all components into a lesson, unit, or project, STEAM education naturally differentiates instruction. It appeals to learning styles, interests, and learning capacity. This creates more ideas from the students, and allows them more creative input.</p>',
								'last_refresh_time' => $this->get_current_db_time()
							);
					
					$locations = @$this->db->query("SELECT loc.*,
													  c.city_name
													FROM BSSDATA.event_locations loc,
													  COMMON.com_cities c
													WHERE loc.city_id 	= c.city_id
													AND loc.status    	= 'Y'
													AND loc.event_id  	= '$event_id'
													--AND loc.location_id = 1
													ORDER BY loc.location_id ASC");
					if(!$locations) 
					{
						$this->log_db_error($this->db->error(), 'get_events', '');
					}
					
					if($locations AND $locations->num_rows() > 0) {
						
						foreach($locations->result() as $location) {
							
							$loc = array(
										'location_id' 	=> $location->LOCATION_ID,
										'location_name' => $location->LOCATION_NAME,
										'city_id' 		=> $location->CITY_ID,
										'city_name' 	=> $location->CITY_NAME,
										'status' 		=> $location->STATUS,
										'lat' 			=> $location->G_LAT,
										'long' 			=> $location->G_LONG,
										'lat_long'		=> $location->G_LAT.','.$location->G_LONG
									);	
							
							array_push($data['locations'], $loc);		
						}
					}
					
					$event_dates = @$this->db->query("SELECT DISTINCT TO_CHAR(activity_date, 'dd/mm/yyyy') program_date,
														TO_CHAR(activity_date, 'fmDay DD Mon, YYYY') event_display_date
													FROM event_schedule
													WHERE event_id     = '$event_id'
													AND activity_date IS NOT NULL
													ORDER BY program_date");
					if(!$event_dates) 
					{
						$this->log_db_error($this->db->error(), 'get_events', '');
					}
					
					if($event_dates AND $event_dates->num_rows() > 0) {
						
						$day_index = 1;
						foreach($event_dates->result() as $event_date) {
							
							$date = array(
										'event_day' 			=> $day_index,
										'event_date' 			=> $event_date->PROGRAM_DATE,
										'event_display_date' 	=> $event_date->EVENT_DISPLAY_DATE,
										'timeslots'				=> $this->get_programs_timeslots($event_id,$event_date->PROGRAM_DATE) 
									);	
							
							array_push($data['event_dates'], $date);
							$day_index++;	
						}
					}
					
					array_push($response, $data);
				}
			}
			
			return $response;
		}
		
		public function get_event_participants($event_id) 
		{
			$response = array();
			
			$ptc_role = array();
			
			/*$roles = $this->db->query("SELECT ed.participant_id,
										  rtrim (xmlagg (xmlelement (e, er.role_id
										  || ',')).extract ('//text()'), ',') role_id,
										  rtrim (xmlagg (xmlelement (e, er.role_desc
										  || ',')).extract ('//text()'), ',') role_desc
										FROM event_schedule_detail ed,
										  event_role er
										WHERE ed.role_id       = er.role_id
										AND ed.participant_id IS NOT NULL
										AND schedule_id       IN
										  (SELECT schedule_id FROM event_schedule WHERE event_id = '$event_id')
										GROUP BY ed.participant_id");*/
			
			$roles = @$this->db->query("SELECT ed.participant_id,
										  rtrim (xmlagg (xmlelement (e, er.role_id
										  || ',')).extract ('//text()'), ',') role_id,
										  rtrim (xmlagg (xmlelement (e, er.role_desc
										  || ',')).extract ('//text()'), ',') role_desc
										FROM
										  (SELECT DISTINCT p.participant_id,
											sd.role_id
										  FROM bssdata.event_participant p,
											bssdata.event_participants_shortlist sh,
											bssdata.event_schedule sc,
											bssdata.event_schedule_detail sd
										  WHERE p.participant_id = sh.participant_id
										  AND sh.participant_id  = sd.participant_id
										  AND sh.event_id        = sc.event_id
										  AND sh.event_id        = '$event_id'
										  AND NVL(sh.status,'N') = 'Y'
										  AND p.publish_profile  = 1
										  ) ed,
										  bssdata.event_role er
										WHERE ed.role_id       = er.role_id
										AND ed.participant_id IS NOT NULL
										GROUP BY ed.participant_id
										");
										
			if(!$roles) {
				$this->log_db_error($this->db->error(), 'get_event_participants', '');
			}
			
			if($roles AND $roles->num_rows() > 0) {
				
				foreach($roles->result() as $role) 
				{					
					$ptc_role[$role->PARTICIPANT_ID] = array_unique(explode(',', $role->ROLE_ID));	
				}
			}

			/*$participants = $this->db->query("SELECT DISTINCT --esd.schedule_id,
												  esd.participant_id,
												  ep.first_name
												  || ' '
												  || ep.middle_name
												  || ' '
												  || ep.last_name participant_name,
												  lower(ep.gender) gender,
												  ep.photo,
												  ep.DESIGNATION,
												  ep.SUB_DESIGNATION,
												  ep.EXPERTIES,
												  ep.PROFILE_DESC,
												  ep.TAGS,
												  esd.role_id,
												  er.role_desc,
												  ep.event_id
												FROM BSSDATA.event_schedule es,
												  bssdata.event_schedule_detail esd,
												  bssdata.event_participant ep,
												  bssdata.event_role er
												WHERE es.schedule_id   = esd.schedule_id 
												AND esd.participant_id = ep.participant_id
												AND esd.role_id          = er.role_id
												AND es.event_id          = '$event_id'");*/
			
			$participants = @$this->db->query("SELECT DISTINCT ep.participant_id,
												  ep.title,
												  ep.first_name,
												  ep.middle_name,
												  ep.last_name,
												  ep.title
												  || ' '
												  || ep.first_name
												  || ' '
												  || ep.middle_name
												  || ' '
												  || ep.last_name participant_name,
												  upper(ep.gender) gender,
												  ep.photo,
												  ep.DESIGNATION,
												  ep.SUB_DESIGNATION,
												  ep.EXPERTIES,
												  ep.PROFILE_DESC,
												  ep.TAGS,
												  ep.twitter_url,
												  ep.linkedin_url,
												  sc.event_id,
												  NVL(eu.user_id, 0) user_id,
												  UPPER(NVL(sh.featured, 'N')) featured,
												  NVL(st.allow_add_network, 'N') allow_add_network
												FROM bssdata.event_participant ep,
												  bssdata.event_participants_shortlist sh,
												  bssdata.event_schedule sc,
												  bssdata.event_schedule_detail sd,
												  bssdata.event_users eu,
												  bssdata.event_settings st
												WHERE ep.participant_id = sh.participant_id
												AND sh.participant_id   = sd.participant_id
												AND sh.event_id         = sc.event_id
												AND ep.participant_id   = eu.participant_id (+)
												AND eu.user_id          = st.user_id (+)
												AND sh.event_id         = '$event_id'
												AND NVL(sh.status,'N')  = 'Y'
												AND ep.publish_profile  = 1
												ORDER BY ep.last_name,
												ep.first_name
												");
												
			if(!$participants) 
			{
				$this->log_db_error($this->db->error(), 'get_event_participants', '');
				
				 return $response;
			}
			
			if($participants AND $participants->num_rows() > 0) {
				
				foreach ($participants->result() as $participant)
				{					
					$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/'.$participant->PHOTO;
							
					if($this->check_file($participant->PHOTO, 'http://beams.beaconhouse.edu.pk/admin/events/photo/')) {
						$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/'.$participant->PHOTO;
					} else {
						if($participant->GENDER == 'F' || $participant->GENDER == 'O') {
							$participant_photo = "http://beams.beaconhouse.edu.pk/admin/events/photo/avatar-female.jpg";
						} else {
							$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/avatar-male.jpg';
						}
					}
					
					$data = array(
								'event_id' 			=> $participant->EVENT_ID,
								'participant_id'	=> $participant->PARTICIPANT_ID,
								'user_id'			=> $participant->USER_ID,
								'title'				=> $participant->TITLE,
								'first_name'		=> $participant->FIRST_NAME,
								'middle_name'		=> $participant->MIDDLE_NAME,
								'last_name'			=> $participant->LAST_NAME,
								'participant_name'	=> $participant->PARTICIPANT_NAME,
								'gender'			=> $participant->GENDER,
								'photo'				=> $participant_photo,
								'featured'			=> $participant->FEATURED,
								'designation'		=> $participant->DESIGNATION, 
								'sub_designation'	=> $participant->SUB_DESIGNATION,
								'experties'			=> $participant->EXPERTIES,
								'profile_desc'		=> urldecode($participant->PROFILE_DESC),
								'profile_tags'		=> $participant->TAGS,
								'twitter_url'		=> $participant->TWITTER_URL,
								'linkedin_url'		=> $participant->LINKEDIN_URL,
								'roles'				=> $ptc_role[$participant->PARTICIPANT_ID],
								'allow_add_network'	=> $participant->ALLOW_ADD_NETWORK
								//'role_id'			=> $participant->ROLE_ID,
								//'role_desc'			=> $participant->ROLE_DESC
							);
							
					array_push($response, $data);
				}
			}
			return $response;
		}
		
		public function get_program_roles($event_id)
		{
			$response = array();
			
			$roles = @$this->db->query("SELECT role_id,
										  role_desc
										FROM event_role
										WHERE role_id IN
										  (SELECT DISTINCT role_id
										  FROM event_schedule_detail
										  WHERE participant_id IS NOT NULL
										  AND schedule_id      IN
											(SELECT schedule_id FROM event_schedule WHERE event_id = '$event_id')
										  )
										ORDER BY role_desc");
			if(!$roles)
			{
				$this->log_db_error($this->db->error(), 'get_program_roles', '');
			}
			
			if($roles AND $roles->num_rows() > 0) {
				
				foreach($roles->result() as $role) {
					
					$data = array(
								'role_id'	=> $role->ROLE_ID,
								'role_desc'	=> $role->ROLE_DESC
							);	
					array_push($response, $data);
				}
			}
			return $response;
		}
		
		public function get_event_activity_types($event_id = '', $refresh_time = '') 
		{
			$response = array();
			
			if($event_id) {			
				$activity_types = @$this->db->query("SELECT DISTINCT typ.activity_type_id,
													  typ.activity_desc
													FROM event_activity act,
													  event_activity_types typ
													WHERE act.activity_type_id = typ.activity_type_id
													AND act.event_id           = '$event_id'
													ORDER BY typ.activity_desc");
			} else {
				$activity_types = @$this->db->query("SELECT DISTINCT typ.activity_type_id,
													  typ.activity_desc
													FROM event_activity_types typ
													ORDER BY typ.activity_desc");
			}
			
			if(!$activity_types) 
			{
				$this->log_db_error($this->db->error(), 'get_event_activity_types', '');
				
				 return $response;
			}
			
			if($activity_types AND $activity_types->num_rows() > 0) {
				
				foreach ($activity_types->result() as $activity_type)
				{					
					$data = array(
								'activity_type_id' 	=> $activity_type->ACTIVITY_TYPE_ID,
								'activity_desc'		=> $activity_type->ACTIVITY_DESC
							);
							
					array_push($response, $data);
				}
			}
			
			return $response;
		}
		
		public function get_event_themes($event_id = '', $refresh_time = '') 
		{
			$response = array();
			
			if($event_id) {			
				$themes = @$this->db->query("SELECT theme_id,
											  theme_desc,
											  short_name,
											  color_code,
											  text_color_code
											FROM event_theme
											WHERE event_id = '$event_id'
											ORDER BY theme_desc");
			} else {
				$themes = @$this->db->query("SELECT theme_id,
											  theme_desc,
											  short_name,
											  color_code,
											  text_color_code
											FROM event_theme
											--WHERE event_id = '2'
											ORDER BY theme_desc");
			}
			
			if(!$themes) 
			{
				$this->log_db_error($this->db->error(), 'get_event_themes', '');
				
				 return $response;
			}
			
			if($themes AND $themes->num_rows() > 0) {
				
				foreach ($themes->result() as $theme)
				{					
					$data = array(
								'theme_id' 			=> $theme->THEME_ID,
								'theme_desc'		=> $theme->THEME_DESC,
								'short_name'		=> $theme->SHORT_NAME,
								'color_code'		=> $theme->COLOR_CODE,
								'text_color_code'	=> $theme->TEXT_COLOR_CODE
							);
							
					array_push($response, $data);
				}
			}
			
			return $response;
		}
		
		public function get_event_venues($event_id = '', $refresh_time = '') 
		{
			$response = array();
			
			$venues = @$this->db->query("SELECT loc.event_id,
										  v.location_id,
										  v.city_id,
										  c.city_name,
										  v.venue_id,
										  v.venue_title,
										  v.sort_order
										FROM BSSDATA.event_locations loc,
										  COMMON.com_cities c,
										  event_venue v
										WHERE loc.city_id   = c.city_id
										AND loc.location_id = v.location_id
										AND loc.status      = 'Y'
										--AND loc.location_id = 1
										AND loc.event_id    = '$event_id'
										ORDER BY v.sort_order ASC");
			
			if(!$venues) 
			{
				$this->log_db_error($this->db->error(), 'get_event_venues', '');
				
				 return $response;
			}
			
			if($venues AND $venues->num_rows() > 0) {
				
				foreach ($venues->result() as $venue)
				{					
					$data = array(
								'location_id'		=> $venue->LOCATION_ID,
								'venue_id' 			=> $venue->VENUE_ID,
								'venue_title'		=> $venue->VENUE_TITLE,
								'sort_order'		=> $venue->SORT_ORDER
							);
							
					array_push($response, $data);
				}
			}
			
			return $response;
		}
		
		public function user_program_timeslot($user_id, $schedule_id, $schedule_date)
        {
			$timeslot_count = $this->db->query("SELECT COUNT(*) TIMESLOT_COUNT
												FROM event_user_schedule
												WHERE USER_ID     = '$user_id'
												AND schedule_date =
												  (SELECT TO_DATE(TO_CHAR(activity_date, 'DD/MM/YYYY')
													|| ' '
													|| NVL(start_time, '00:00:00'), 'DD/MM/YYYY HH24:MI:SS') act_date
												  FROM event_schedule
												  WHERE schedule_id = '$schedule_id'
												  )")->row()->TIMESLOT_COUNT;
			if($timeslot_count > 0) 
			{
				return true;
			} 
			else {
				return false;
			}
        }
		
		public function register_user_program($user_id, $schedule_id, $schedule_date)
        {
			$db2= $this->load->database('trans', TRUE);				
			
			$db2->trans_begin();
			
			$schedule_date_time = $db2->query("SELECT TO_CHAR(activity_date, 'DD/MM/YYYY')
												  || ' '
												  || NVL(start_time, '00:00') schedule_date_time
												FROM event_schedule
												WHERE schedule_id      = '$schedule_id'")->row()->SCHEDULE_DATE_TIME;
			
			$db2->set('USER_ID', $user_id);
			$db2->set('SCHEDULE_ID', $schedule_id);				
			$db2->set('SCHEDULE_DATE', "TO_DATE('$schedule_date_time','DD/MM/YYYY HH24:MI')",FALSE);
			
			$status = @$db2->insert('EVENT_USER_SCHEDULE');
			//echo $db2->last_query(); exit;
			if(!$status) 
			{
				$this->log_db_error($db2->error(), 'register_user_program', '');
				
				return false;
			}
				
			if($status) {				
				
				if ($db2->trans_status() === FALSE){
					
					$db2->trans_rollback();
					return false;
					
				} else {
					$db2->trans_commit();					
					return  true;
				}
				 				
			} else {
				return false;	
			}
        }
		
		public function unregister_user_program($user_id, $schedule_id)
        {
			$db2= $this->load->database('trans', TRUE);				
			
			$db2->trans_begin();
			
			$db2->where('USER_ID', $user_id);
			$db2->where('SCHEDULE_ID', $schedule_id);				
			$status = @$db2->delete('EVENT_USER_SCHEDULE');
			//echo $db2->last_query(); exit;
			if(!$status) 
			{
				$this->log_db_error($db2->error(), 'unregister_user_program', '');
				
				return false;
			}
				
			if($status) {				
				
				if ($db2->trans_status() === FALSE){
					
					$db2->trans_rollback();
					return false;
					
				} else {
					$db2->trans_commit();					
					return  true;
				}
				 				
			} else {
				return false;	
			}
        }
		
		public function get_programs($event_id, $user_id, $refresh_time = '') 
		{
			$response = array();
			
			if($refresh_time AND $refresh_time != '') {
				
				$query = @$this->db->query("SELECT
											  CASE
												WHEN MAX(NVL(update_date, entry_date))-to_date($refresh_time,'YYYYMMDDHH24MISS') > 0
												THEN 1
												ELSE 0
											  END AS status
											FROM event_schedule
											WHERE event_id = '$event_id'");
				
				if($query AND $query->row()->STATUS == 0) {
					
					return $response;
				}
			}
			
			/*$programs = $this->db->query("SELECT es.*,
											  nvl(eus.user_id, -1) user_id,
											  CASE
												WHEN ROUND((sysdate - to_date((es.activity_date
												  || ' '
												  || NVL(es.end_time, '00:00')), 'dd/mm/yyyy hh24:mi'))*24*60, 0) > 60
												THEN 'Y'
												ELSE 'N'
											  END session_inactive_status
											FROM
											  (SELECT es.schedule_id,
												ct.city_id,
												ct.city_name,
												TO_CHAR(es.activity_date, 'dd/mm/yyyy') activity_date,
												TO_CHAR(es.activity_date, 'fmDdth Month') display_date,
												ev.venue_id,
												ev.venue_title,
												ev.sort_order,
												ea.activity_id,
												typ.activity_type_id,
												typ.activity_desc,
												ea.activity_title,
												SUBSTRB(DBMS_LOB.SUBSTR(es.abstract, 4000, 1 ), 1, 4000) abstract,
												es.start_time,
												es.end_time,
												es.duration,
												es.day_session,
												es.theme_id,
												et.theme_desc,
												et.short_name,
												DECODE(es.allow_registration, 0, 'N', 'Y') allow_registration,
												es.website_home,
												es.location_id,
												es.event_id
											  FROM event_schedule es,
												COMMON.com_cities ct,
												event_venue ev,
												event_activity ea,
												event_activity_types typ,
												event_theme et
											  WHERE es.city_id        = ct.city_id
											  AND ea.activity_type_id = typ.activity_type_id
											  AND es.venue_id         = ev.venue_id
											  AND es.activity_id      = ea.activity_id
											  AND es.theme_id         = et.theme_id
											  AND es.event_id         = '$event_id'
											  --AND es.location_id      = 1
											  ) es,
											  (SELECT DISTINCT us.user_id,
												  us.schedule_id
												FROM event_user_schedule us ,
												  event_user_schedule_qr usq
												WHERE us.user_id = usq.user_id
												AND us.user_id   = '$user_id'
											  ) eus
											WHERE es.schedule_id = eus.schedule_id (+)
											ORDER BY es.city_id,
											  es.activity_date,
											  es.start_time,
											  es.sort_order,
											  es.duration ASC");*/
			
			/*$programs = $this->db->query("SELECT es.*,
											  NVL(NVL(eus.user_id, att.user_id), -1) user_id,
											  NVL(att.attendance, 'N') attendance,
											  CASE
												WHEN ROUND((sysdate - to_date((es.activity_date
												  || ' '
												  || NVL(es.end_time, '00:00')), 'dd/mm/yyyy hh24:mi'))*24*60, 0) > 60
												THEN 'Y'
												ELSE 'N'
											  END session_inactive_status
											FROM
											  (SELECT es.schedule_id,
												ct.city_id,
												ct.city_name,
												TO_CHAR(es.activity_date, 'dd/mm/yyyy') activity_date,
												TO_CHAR(es.activity_date, 'fmDdth Month') display_date,
												ev.venue_id,
												ev.venue_title,
												ev.sort_order,
												ea.activity_id,
												typ.activity_type_id,
												typ.activity_desc,
												ea.activity_title,
												SUBSTRB(DBMS_LOB.SUBSTR(es.abstract, 4000, 1 ), 1, 4000) abstract,
												es.start_time,
												es.end_time,
												es.duration,
												es.day_session,
												es.theme_id,
												et.theme_desc,
												et.short_name,
												DECODE(es.allow_registration, 0, 'N', 'Y') allow_registration,
												es.website_home,
												es.location_id,
												es.event_id
											  FROM BSSDATA.event_schedule es,
												COMMON.com_cities ct,
												BSSDATA.event_venue ev,
												BSSDATA.event_activity ea,
												BSSDATA.event_activity_types typ,
												BSSDATA.event_theme et
											  WHERE es.city_id        = ct.city_id
											  AND ea.activity_type_id = typ.activity_type_id
											  AND es.venue_id         = ev.venue_id
											  AND es.activity_id      = ea.activity_id
											  AND es.theme_id         = et.theme_id
											  AND es.event_id         = '$event_id'
												--AND es.location_id      = 1
											  ) es,
											  (SELECT DISTINCT us.user_id,
												us.schedule_id
											  FROM BSSDATA.event_user_schedule us ,
												BSSDATA.event_user_schedule_qr usq
											  WHERE us.user_id = usq.user_id (+)
											  AND us.user_id   = '$user_id'
											  ) eus,
											  (SELECT DISTINCT att.user_id,
												att.schedule_id,
												DECODE(NVL(TO_CHAR(att.create_date, 'dd/mm/yyyy'), 'N'), 'N', 'N', 'Y') ATTENDANCE
											  FROM bssdata.event_user_schedule_qr att
											  WHERE ATT.USER_ID = '$user_id'
											  ) att
											WHERE es.schedule_id = eus.schedule_id (+)
											AND es.schedule_id   = att.schedule_id (+)
											ORDER BY es.city_id,
											  es.activity_date,
											  es.start_time,
											  es.sort_order,
											  es.duration ASC");*/
			
			$programs = @$this->db->query("SELECT
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM event_poll
										  WHERE schedule_id           = es.schedule_id
										  AND upper(NVL(status, 'N')) = 'N'
										  ) poll_exists,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM event_poll
										  WHERE schedule_id           = es.schedule_id
										  AND upper(NVL(status, 'N')) = 'Y'
										  ) poll_active,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM EVENT_POLL_RESPONSE
										  WHERE USER_ID = '$user_id'
										  AND poll_id  IN
											(SELECT poll_id FROM event_poll WHERE schedule_id = es.schedule_id
											)
										  ) poll_submitted,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM event_evaluation_sessions
										  WHERE schedule_id           = es.schedule_id
										  AND upper(NVL(status, 'N')) = 'N'
										  ) evaluation_exists,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM event_evaluation_sessions
										  WHERE schedule_id           = es.schedule_id
										  AND upper(NVL(status, 'N')) = 'Y'
										  ) evaluation_active,
										  (SELECT DECODE(COUNT(*), 0, 'N', 'Y')
										  FROM EVENT_EVALUATION_RESPONSE
										  WHERE USER_ID   = '$user_id'
										  AND schedule_id = es.schedule_id
										  ) evaluation_submitted,
										  es.*,
										  NVL(NVL(eus.user_id, att.user_id), -1) user_id,
										  NVL(att.attendance, 'N') attendance,
										  CASE
											WHEN ROUND((sysdate - to_date((es.activity_date
											  || ' '
											  || NVL(es.end_time, '00:00')), 'dd/mm/yyyy hh24:mi'))*24*60, 0) > 60
											THEN 'Y'
											ELSE 'N'
										  END session_inactive_status
										FROM
										  (SELECT es.schedule_id,
											ct.city_id,
											ct.city_name,
											TO_CHAR(es.activity_date, 'dd/mm/yyyy') activity_date,
											TO_CHAR(es.activity_date, 'fmDdth Month') display_date,
											ev.venue_id,
											ev.venue_title,
											ev.sort_order,
											ea.activity_id,
											typ.activity_type_id,
											typ.activity_desc,
											ea.activity_title,
											SUBSTRB(DBMS_LOB.SUBSTR(es.abstract, 4000, 1 ), 1, 4000) abstract,
											es.start_time,
											es.end_time,
											es.duration,
											es.day_session,
											es.theme_id,
											et.theme_desc,
											et.short_name,
											DECODE(es.allow_registration, 0, 'N', 'Y') allow_registration,
											es.website_home,
											es.location_id,
											es.event_id
										  FROM BSSDATA.event_schedule es,
											COMMON.com_cities ct,
											BSSDATA.event_venue ev,
											BSSDATA.event_activity ea,
											BSSDATA.event_activity_types typ,
											BSSDATA.event_theme et
										  WHERE es.city_id        = ct.city_id
										  AND ea.activity_type_id = typ.activity_type_id
										  AND es.venue_id         = ev.venue_id
										  AND es.activity_id      = ea.activity_id
										  AND es.theme_id         = et.theme_id
										  AND es.event_id         = '$event_id'
											--AND es.location_id      = 1
										  ) es,
										  (SELECT DISTINCT us.user_id,
											us.schedule_id
										  FROM BSSDATA.event_user_schedule us ,
											BSSDATA.event_user_schedule_qr usq
										  WHERE us.user_id = usq.user_id (+)
										  AND us.user_id   = '$user_id'
										  ) eus,
										  (SELECT DISTINCT att.user_id,
											att.schedule_id,
											DECODE(NVL(TO_CHAR(att.create_date, 'dd/mm/yyyy'), 'N'), 'N', 'N', 'Y') ATTENDANCE
										  FROM bssdata.event_user_schedule_qr att
										  WHERE ATT.USER_ID = '$user_id'
										  ) att
										WHERE es.schedule_id = eus.schedule_id (+)
										AND es.schedule_id   = att.schedule_id (+)
										AND es.schedule_id NOT IN (223)
										ORDER BY es.city_id,
										  es.activity_date,
										  es.start_time,
										  es.sort_order,
										  es.duration ASC");
			if(!$programs) 
			{
				$this->log_db_error($this->db->error(), 'get_programs', '');
				
				 return $response;
			}
			
			if($programs AND $programs->num_rows() > 0) {
				
				foreach ($programs->result() as $program)
				{					
					$schedule_id 	= $program->SCHEDULE_ID;
					$display_date 	= $program->DISPLAY_DATE;
					
					$display_date .= date(' (h:i a ', strtotime($program->START_TIME));
					$display_date .= date(' - h:i a)', strtotime($program->END_TIME));
					
					$data = array(
								'event_id' 			=> $program->EVENT_ID,
								'schedule_id'		=> $program->SCHEDULE_ID,
								'user_id'			=> $program->USER_ID,
								'poll_exists'		=> $program->POLL_EXISTS,
								'poll_active'		=> $program->POLL_ACTIVE,
								'poll_submitted'	=> $program->POLL_SUBMITTED,
								'evaluation_exists'	=> $program->EVALUATION_EXISTS,
								'evaluation_active'	=> $program->EVALUATION_ACTIVE,
								'evaluation_submitted'	=> $program->EVALUATION_SUBMITTED,
								'attendance'		=> $program->ATTENDANCE,
								'activity_type_id'	=> $program->ACTIVITY_TYPE_ID,
								'activity_desc'		=> $program->ACTIVITY_DESC,								
								'activity_id'		=> $program->ACTIVITY_ID,
								'activity_title'	=> $program->ACTIVITY_TITLE,
								'activity_date'		=> $program->ACTIVITY_DATE,
								'abstract'			=> $program->ABSTRACT,
								'start_time'		=> $program->START_TIME,
								'end_time'			=> $program->END_TIME,
								'inactive_status'	=> $program->SESSION_INACTIVE_STATUS, 
								'day_session'		=> $program->DAY_SESSION,
								'display_date'		=> $display_date, 
								'location_id'		=> $program->LOCATION_ID,
								'theme_id'			=> $program->THEME_ID,
								'theme_desc'		=> $program->THEME_DESC,
								'venue_id'			=> $program->VENUE_ID,
								'venue_title'		=> $program->VENUE_TITLE,
								'allow_register'	=> $program->ALLOW_REGISTRATION,
								'participants'		=> array(),
								'roles'				=> array()
							);
					
					$participants = @$this->db->query("SELECT esd.schedule_id,
														  esd.participant_id,
														  ep.first_name
														  || ' '
														  || ep.last_name participant_name,
														  upper(ep.gender) gender,
														  ep.photo,
														  ep.DESIGNATION,
														  ep.SUB_DESIGNATION,
														  ep.EXPERTIES,
														  ep.PROFILE_DESC,
														  ep.TAGS,												  
														  esd.role_id,
														  er.role_desc
														FROM bssdata.event_schedule_detail esd,
														  bssdata.event_participant ep,
														  bssdata.event_role er
														WHERE esd.participant_id = ep.participant_id
														AND esd.role_id          = er.role_id
														AND esd.schedule_id      = '$schedule_id'
														ORDER BY er.role_desc ASC");		
					if(!$participants) 
					{
						$this->log_db_error($this->db->error(), 'get_programs', '');
					}
					
					if($participants AND $participants->num_rows() > 0) {
						
						foreach ($participants->result() as $participant)
						{
							$participant_photo = '';
							
							if($this->check_file($participant->PHOTO, 'http://beams.beaconhouse.edu.pk/admin/events/photo/')) {
								$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/'.$participant->PHOTO;
							} else {
								if($participant->GENDER == 'F' || $participant->GENDER == 'O') {
									$participant_photo = "http://beams.beaconhouse.edu.pk/admin/events/photo/avatar-female.jpg";
								} else {
									$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/avatar-male.jpg';
								}
							}
							
							$participant_data = array(
													'participant_id'		=> $participant->PARTICIPANT_ID,
													'participant_name'		=> $participant->PARTICIPANT_NAME,
													'participant_photo'		=> $participant_photo,
													'designation'			=> $participant->DESIGNATION,
													'sub_designation'		=> $participant->SUB_DESIGNATION,
													'experties'				=> $participant->EXPERTIES,
													'profile_desc'			=> urldecode($participant->PROFILE_DESC),
													'profile_tags'			=> $participant->TAGS,
													'role_id'				=> $participant->ROLE_ID,
													'role_desc'				=> $participant->ROLE_DESC
												);
							array_push($data['participants'], $participant_data);
						}
					}
					/* ----------SPEAKERS WITH ROLE------------- */
					
					$roles = @$this->db->query("SELECT esd.*,
													  er.role_desc
													FROM
													  (SELECT schedule_id,
														role_id,
														ltrim(rtrim (xmlagg (xmlelement (e, participant_id
														|| ',')).extract ('//text()'), ','), ',') participant_ids
													  FROM event_schedule_detail
													  WHERE schedule_id = '$schedule_id'
													  AND participant_id IS NOT NULL
													  GROUP BY schedule_id,
														role_id
													  ) esd,
													  event_role er
													WHERE esd.role_id = er.role_id
													ORDER BY er.role_desc ASC");		
					if(!$roles) 
					{
						$this->log_db_error($this->db->error(), 'get_programs', '');
					}
					
					if($roles AND $roles->num_rows() > 0) {
						
						foreach ($roles->result() as $role)
						{							
							$role_data = array(
											'role_id'		=> $role->ROLE_ID,
											'role_desc'		=> $role->ROLE_DESC,
											'speakers'		=> array()
										 );
							
							$participant_ids  = $role->PARTICIPANT_IDS;
							
							$speakers = @$this->db->query("SELECT ep.participant_id,
																  ep.first_name
																  || ' '
																  || ep.last_name participant_name,
																  upper(ep.gender) gender,
																  ep.photo
																FROM bssdata.event_participant ep
																WHERE ep.participant_id IN($participant_ids)
																ORDER BY ep.first_name");	
							if(!$speakers) 
							{
								$this->log_db_error($this->db->error(), 'get_programs', '');
							}
							
							if($speakers AND $speakers->num_rows() > 0) {
								
								foreach ($speakers->result() as $speaker)
								{
	
									
									$speaker_data = array(
															'speaker_id'		=> $speaker->PARTICIPANT_ID,
															'speaker_name'		=> $speaker->PARTICIPANT_NAME
														);
														
									array_push($role_data['speakers'], $speaker_data);
								}
							}
							
							
							array_push($data['roles'], $role_data);
						}
					}
					//$data['program_polls'] = $this->get_program_polls($schedule_id, $user_id);
					array_push($response, $data);
				}
			}
			
			return $response;
		}
		
		public function get_programs_timeslots($event_id, $activity_date = '') 
		{
			$response = array();
			
			if($event_id) {
				
				if($activity_date != '') 
				{
					$timings = @$this->db->query("SELECT TO_CHAR(activity_date, 'dd/mm/yyyy') activity_date,
												  rtrim (xmlagg (xmlelement (e, schedule_id
												  || ',')).extract ('//text()'), ',') schedule_ids
												FROM
												  (SELECT activity_date,
													start_time,
													end_time,
													schedule_id
												  FROM event_schedule
												  WHERE event_id  = '$event_id'
												  AND activity_date = to_date('$activity_date', 'dd/mm/yyyy')
												  AND start_time IS NOT NULL
												  AND end_time   IS NOT NULL
												  )
												GROUP BY activity_date
												ORDER BY activity_date");
				} else {
					
					$timings = @$this->db->query("SELECT TO_CHAR(activity_date, 'dd/mm/yyyy') activity_date,
													  rtrim (xmlagg (xmlelement (e, schedule_id
													  || ',')).extract ('//text()'), ',') schedule_ids
													FROM
													  (SELECT activity_date,
														start_time,
														end_time,
														schedule_id
													  FROM event_schedule
													  WHERE event_id  = '$event_id'
													  --AND location_id = 1
													  AND start_time IS NOT NULL
													  AND end_time   IS NOT NULL
													  )
													GROUP BY activity_date
													ORDER BY activity_date");
				}
				
				if(!$timings) 
				{
					$this->log_db_error($this->db->error(), 'get_programs_timeslots', '');
					
					 return $response;
				}
			
				if($timings AND $timings->num_rows() > 0) {
					
					foreach ($timings->result() as $timing)
					{					
						$activity_date 	= $timing->ACTIVITY_DATE;
						$schedule_ids	= $timing->SCHEDULE_IDS;
						
						$data = array(
									'activity_date' 	=> $activity_date,
									'activity_time'		=> array()
								);
						
						$timeslots = @$this->db->query("SELECT DISTINCT start_time,
														  end_time,
														  start_time
														  || '-'
														  || end_time display_time
														FROM event_schedule
														WHERE schedule_id IN ($schedule_ids)
														ORDER BY start_time,
														  end_time");
						if($timeslots AND $timeslots->num_rows() > 0) {
							
							foreach($timeslots->result() as $timeslot) {
								
								$slots = array(
											'start_time'	=> $timeslot->START_TIME,
											'end_time'		=> $timeslot->END_TIME,
											'display_time'	=> $timeslot->DISPLAY_TIME
										 );	
								array_push($data['activity_time'], $slots);
							}
						}
						array_push($response, $data);
					}
				}
			}
			
			return $response;
		}
		
		public function get_programs_timewise($event_id, $user_id, $refresh_time = '') 
		{
			$response = array();
			
			if($refresh_time AND $refresh_time != '') {
				
				$query = @$this->db->query("SELECT
											  CASE
												WHEN MAX(NVL(update_date, entry_date))-to_date($refresh_time,'YYYYMMDDHH24MISS') > 0
												THEN 1
												ELSE 0
											  END AS status
											FROM event_schedule
											WHERE event_id = '$event_id'");
				
				if($query AND $query->row()->STATUS == 0) {
					
					return $response;
				}
			}
			
			$timings = @$this->db->query("SELECT start_time,
										  end_time,
										  rtrim (xmlagg (xmlelement (e, schedule_id
										  || ',')).extract ('//text()'), ',') schedule_ids
										FROM
										  (SELECT start_time,
											end_time,
											schedule_id
										  FROM event_schedule
										  WHERE event_id  = '$event_id'
										  AND start_time IS NOT NULL
										  AND end_time   IS NOT NULL
										  )
										GROUP BY start_time,
										  end_time");
			
			if(!$timings) 
			{
				$this->log_db_error($this->db->error(), 'get_programs_timewise', '');
				
				 return $response;
			}
			
			if($timings AND $timings->num_rows() > 0) {
				
				foreach ($timings->result() as $timing)
				{					
					$start_time 	= $timing->START_TIME;
					$end_time 		= $timing->END_TIME;
					$schedule_ids	= $timing->SCHEDULE_IDS;
					
					$programs = @$this->db->query("SELECT es.*,
											  nvl(eus.user_id, -1) user_id
											FROM
											  (SELECT es.schedule_id,
												ct.city_id,
												ct.city_name,
												TO_CHAR(es.activity_date, 'dd/mm/yyyy') activity_date,
												TO_CHAR(es.activity_date, 'fmDdth Month') display_date,
												ev.venue_id,
												ev.venue_title,
												ev.sort_order,
												ea.activity_id,
												typ.activity_type_id,
												typ.activity_desc,
												ea.activity_title,
												SUBSTRB(DBMS_LOB.SUBSTR(es.abstract, 4000, 1 ), 1, 4000) abstract,
												es.start_time,
												es.end_time,
												es.duration,
												es.day_session,
												es.theme_id,
												et.theme_desc,
												et.short_name,
												es.allow_registration,
												es.website_home,
												es.location_id,
												es.event_id
											  FROM event_schedule es,
												COMMON.com_cities ct,
												event_venue ev,
												event_activity ea,
												event_activity_types typ,
												event_theme et
											  WHERE es.city_id        = ct.city_id
											  AND ea.activity_type_id = typ.activity_type_id
											  AND es.venue_id         = ev.venue_id
											  AND es.activity_id      = ea.activity_id
											  AND es.theme_id         = et.theme_id
											  AND es.event_id         = '$event_id'
											  AND es.schedule_id     IN ($schedule_ids)
											  ) es,
											  (SELECT user_id, schedule_id FROM event_user_schedule WHERE user_id = '$user_id'
											  ) eus
											WHERE es.schedule_id = eus.schedule_id (+)
											ORDER BY es.city_id,
											  es.activity_date,
											  es.start_time,
											  es.sort_order,
											  es.duration ASC");
			
					if(!$programs) 
					{
						$this->log_db_error($this->db->error(), 'get_programs', '');
						
						 return $response;
					}
					
					if($programs AND $programs->num_rows() > 0) {
						
						foreach ($programs->result() as $program)
						{					
							$schedule_id 	= $program->SCHEDULE_ID;
							$display_date 	= $program->DISPLAY_DATE;
							
							$display_date .= date(' (h:i a ', strtotime($program->START_TIME));
							$display_date .= date(' - h:i a)', strtotime($program->END_TIME));
							
							$data = array(
										'event_id' 			=> $program->EVENT_ID,
										'schedule_id'		=> $program->SCHEDULE_ID,
										'user_id'			=> $program->USER_ID,
										'activity_type_id'	=> $program->ACTIVITY_TYPE_ID,
										'activity_desc'		=> $program->ACTIVITY_DESC,								
										'activity_id'		=> $program->ACTIVITY_ID,
										'activity_title'	=> $program->ACTIVITY_TITLE,
										'activity_date'		=> $program->ACTIVITY_DATE,
										'abstract'			=> $program->ABSTRACT,
										'start_time'		=> $program->START_TIME,
										'end_time'			=> $program->END_TIME,
										'day_session'		=> $program->DAY_SESSION,
										'display_date'		=> $display_date, 
										'theme_id'			=> $program->THEME_ID,
										'theme_desc'		=> $program->THEME_DESC,
										'venue_id'			=> $program->VENUE_ID,
										'venue_title'		=> $program->VENUE_TITLE,
										'participants'		=> array()
									);
							
							$participants = @$this->db->query("SELECT esd.schedule_id,
																  esd.participant_id,
																  ep.first_name
																  || ' '
																  || ep.last_name participant_name,
																  upper(ep.gender) gender,
																  ep.photo,
																  esd.role_id,
		
																  er.role_desc
																FROM bssdata.event_schedule_detail esd,
																  bssdata.event_participant ep,
																  bssdata.event_role er
																WHERE esd.participant_id = ep.participant_id
																AND esd.role_id          = er.role_id
																AND esd.schedule_id      = '$schedule_id'");		
							if(!$participants) 
							{
								$this->log_db_error($this->db->error(), 'get_programs', '');
							}
							
							if($participants AND $participants->num_rows() > 0) {
								
								foreach ($participants->result() as $participant)
								{
									$participant_photo = '';
									
									if($this->check_file($participant->PHOTO, 'http://beams.beaconhouse.edu.pk/admin/events/photo/')) {
										$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/'.$participant->PHOTO;
									} else {
										if($participant->GENDER == 'F' || $participant->GENDER == 'O') {
											$participant_photo = "http://beams.beaconhouse.edu.pk/admin/events/photo/avatar-female.jpg";
										} else {
											$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/avatar-male.jpg';
										}
									}
									
									$participant_data = array(
															'participant_id'		=> $participant->PARTICIPANT_ID,
															'participant_name'		=> $participant->PARTICIPANT_NAME,
															'participant_photo'		=> $participant_photo,
															'role_id'				=> $participant->ROLE_ID,
															'role_desc'				=> $participant->ROLE_DESC
														);
									array_push($data['participants'], $participant_data);
								}
							}
							
							$data['program_polls'] = $this->get_program_polls($schedule_id, $user_id);
							array_push($response, $data);
						}
					}
				}
				
			}
			
			
			return $response;
		}
		
		# Program Poll Cron
		public function get_program_polls_cron($user_id) 
		{
			$response = array();
			
			if($schedule_id) {
				
				$polls = @$this->db->query("SELECT poll_id,
											  poll_name,
											  NVL(status, 'N') poll_status,
											  schedule_id,
											  poll_code,
											  chart_type
											FROM EVENT_POLL
											WHERE schedule_id = '$schedule_id'
											AND upper(NVL(status, 'N')) = 'Y'
											");
				
				if(!$polls) 
				{
					$this->log_db_error($this->db->error(), 'get_program_polls', 'poll');
				}
				
				if($polls AND $polls->num_rows() > 0) {
					
					foreach($polls->result() as $poll) {
						
						$attendance = $this->db->query("SELECT COUNT(*) ATT
														FROM event_user_schedule_qr
														WHERE user_id   = '$user_id'
														AND schedule_id = '$schedule_id'")->row()->ATT;
						if($attendance == 0) {
							
							$response['status'] 	= 'N';
							$response['message']	= 'Program poll requires user attendance.';
							
							return $response;
						}
				
						$poll_id = $poll->POLL_ID;
						
						$data = array(
									'schedule_id' 	=> $poll->SCHEDULE_ID,
									'poll_id'		=> $poll->POLL_ID,
									'poll_name'		=> $poll->POLL_NAME,
									'poll_status'	=> $poll->POLL_STATUS,
									'poll_code'		=> $poll->POLL_CODE,
									'chart_type'	=> $poll->CHART_TYPE,
									'questions'		=> array()
								);
						
						$questions = @$this->db->query("SELECT QUESTION_ID,
														  QUESTION,
														  ENTRY_DATE,
														  POLL_ID,
														  ENTRY_USER_ID,
														  DECODE(TYPE_ID, 1, 'radio', 'checkbox') question_type
														FROM EVENT_POLL_QUESTIONS
														WHERE POLL_ID = '$poll_id'");
						
						if(!$questions) 
						{
							$this->log_db_error($this->db->error(), 'get_program_polls', 'poll question');
						}
						
						if($questions AND $questions->num_rows() > 0) {
							
							foreach($questions->result() as $question) {
								
								$question_id = $question->QUESTION_ID;
								
								$question_data = array(
												 	'question_id' 		=> $question->QUESTION_ID,
													'question_desc'		=> $question->QUESTION,
													'question_type'		=> $question->QUESTION_TYPE,
													'options'			=> array()
												 );
												 
								$options = @$this->db->query("SELECT * FROM EVENT_POLL_QUESTION_OPTIONS WHERE QUESTION_ID = '$question_id'");
								
								if(!$options) 
								{
									$this->log_db_error($this->db->error(), 'get_program_polls', 'question option');
								}
								
								if($options AND $options->num_rows() > 0){
									
									foreach($options->result() as $option) {
										
										$option_data = array(
													   		'option_id'		=> $option->OPTION_ID,
															'option_desc'	=> $option->OPTION_DESC
													   );
										array_push($question_data['options'], $option_data);
									}
								}
								array_push($data['questions'], $question_data);
							}
						}
						array_push($response, $data);
						$response['status'] 	= 'Y';
						$response['message']	= 'The program poll has been loaded.';
					}
				} else {
					$response['status'] 	= 'N';
					$response['message']	= 'The program poll is currently inactive.';
				}
			}
			
			return $response;			
		}
		
		# Program Polls
		public function get_program_polls($schedule_id, $user_id) 
		{
			$response = array();
			
			if($schedule_id) {
				
				$polls = @$this->db->query("SELECT poll_id,
											  poll_name,
											  NVL(status, 'N') poll_status,
											  schedule_id,
											  poll_code,
											  chart_type
											FROM EVENT_POLL
											WHERE schedule_id = '$schedule_id'
											AND upper(NVL(status, 'N')) = 'Y'
											");
				
				if(!$polls) 
				{
					$this->log_db_error($this->db->error(), 'get_program_polls', 'poll');
				}
				
				if($polls AND $polls->num_rows() > 0) {
					
					foreach($polls->result() as $poll) {
						
						$attendance = $this->db->query("SELECT COUNT(*) ATT
														FROM event_user_schedule_qr
														WHERE user_id   = '$user_id'
														AND schedule_id = '$schedule_id'")->row()->ATT;
						if($attendance == 0) {
							
							$response['status'] 	= 'N';
							$response['message']	= 'Program poll requires user attendance.';
							
							return $response;
						}
						
						$poll_id = $poll->POLL_ID;
				
						$submission = $this->db->query("SELECT COUNT(*) att
														FROM event_poll_response
														WHERE user_id = '$user_id'
														AND poll_id   = '$poll_id'")->row()->ATT;
						if($submission > 0) {
							
							$response['status'] 	= 'N';
							$response['message']	= 'You have already submitted this poll.';
							
							return $response;
						}
						
						$poll_id = $poll->POLL_ID;
						
						$data = array(
									'schedule_id' 	=> $poll->SCHEDULE_ID,
									'poll_id'		=> $poll->POLL_ID,
									'poll_name'		=> $poll->POLL_NAME,
									'poll_status'	=> $poll->POLL_STATUS,
									'poll_code'		=> $poll->POLL_CODE,
									'chart_type'	=> $poll->CHART_TYPE,
									'questions'		=> array()
								);
						
						$questions = @$this->db->query("SELECT QUESTION_ID,
														  QUESTION,
														  ENTRY_DATE,
														  POLL_ID,
														  ENTRY_USER_ID,
														  DECODE(TYPE_ID, 1, 'radio', 'checkbox') question_type
														FROM EVENT_POLL_QUESTIONS
														WHERE POLL_ID = '$poll_id'");
						
						if(!$questions) 
						{
							$this->log_db_error($this->db->error(), 'get_program_polls', 'poll question');
						}
						
						if($questions AND $questions->num_rows() > 0) {
							
							foreach($questions->result() as $question) {
								
								$question_id = $question->QUESTION_ID;
								
								$question_data = array(
												 	'question_id' 		=> $question->QUESTION_ID,
													'question_desc'		=> $question->QUESTION,
													'question_type'		=> $question->QUESTION_TYPE,
													'options'			=> array()
												 );
												 
								$options = @$this->db->query("SELECT * FROM EVENT_POLL_QUESTION_OPTIONS WHERE QUESTION_ID = '$question_id'");
								
								if(!$options) 
								{
									$this->log_db_error($this->db->error(), 'get_program_polls', 'question option');
								}
								
								if($options AND $options->num_rows() > 0){
									
									foreach($options->result() as $option) {
										
										$option_data = array(
													   		'option_id'		=> $option->OPTION_ID,
															'option_desc'	=> $option->OPTION_DESC
													   );
										array_push($question_data['options'], $option_data);
									}
								}
								array_push($data['questions'], $question_data);
							}
						}
						array_push($response, $data);
						$response['status'] 	= 'Y';
						$response['message']	= 'The program poll has been loaded.';
					}
				} else {
					$response['status'] 	= 'N';
					$response['message']	= 'The program poll is currently inactive.';
				}
			}
			
			return $response;			
		}
		
		# Save program poll
		public function save_program_polls($response)
		{
			$db2= $this->load->database('trans', TRUE);	
			
			$db2->trans_begin();
			//echo "<pre>";
			//print_r($data);			
			foreach($response as $pc) 
			{
				$poll_id 		= $pc['POLL_ID'];
				$user_id		= $pc['USER_ID'];
				$question_id 	= $pc['QUESTION_ID'];
				$statements		= $pc['OPTION_ID'];
				
				if(is_array($statements) && count($statements) > 1) {
	
					foreach($statements as  $val ) {

						$data = array(
									'POLL_ID'		=> $poll_id,
									'USER_ID'		=> $user_id,
									'QUESTION_ID'	=> $question_id,
									'OPTION_ID'		=> $val
								);
	
						$status = @$db2->insert('EVENT_POLL_RESPONSE', $data); 
						
						if(!$status) 
						{
							$this->log_db_error($db2->error(), 'save_program_polls', '');
						} 
					}
									
				} else {
					
					$data = array(
								'POLL_ID'		=> $poll_id,
								'USER_ID'		=> $user_id,
								'QUESTION_ID'	=> $question_id,
								'OPTION_ID'		=> $statements[0]
							);
	
					$status = @$db2->insert('EVENT_POLL_RESPONSE', $data); 
					
					if(!$status) 
					{
						$this->log_db_error($db2->error(), 'save_program_polls', '');

					}
				}
			} // end foreach
			
			if ($db2->trans_status() === FALSE){
							
				$db2->trans_rollback();
				
				$result = 0;
				
			} else {
				$db2->trans_commit();
				$result = 1;
			}
			//$result = $this->db->insert('APP_NOTIFICATION_RESPONSE', $arr_data);
			
			if($result) {				
				return true;
			} else {
				return false;	
			}
		}
		
		# Event Evaluation
		public function get_event_evaluation($event_id, $user_id) 
		{
			$response = array();
						
			$evaluations = @$this->db->query("SELECT EVALUATION_ID,
											  EVENT_ID,
											  EVALUATION_TITLE,
											  STATUS												  
											FROM EVENT_EVALUATION
											WHERE upper(NVL(status,'Y')) = 'Y'
											AND event_id                 = '$event_id'");
			
			if(!$evaluations) 
			{
				$this->log_db_error($this->db->error(), 'get_event_evaluation', '');
			}
			
			if($evaluations AND $evaluations->num_rows() > 0) {
				
				$attendance = $this->db->query("SELECT COUNT(*) ATT
												FROM event_user_attendance
												WHERE user_id   = '$user_id'
												AND event_id    = '$event_id'")->row()->ATT;
				if($attendance == 0) {
					
					$response['status'] 	= 'N';
					$response['message']	= 'Event evaluation requires user attendance.';
					
					return $response;
				}
				
				$submission = $this->db->query("SELECT COUNT(*) att
												FROM BSSDATA.event_evaluation ev,
												  bssdata.event_evaluation_response er
												WHERE ev.evaluation_id     = er.evaluation_id
												AND er.user_id             = '$user_id'
												AND NVL(er.schedule_id, 0) = 0
												AND ev.event_id            = '$event_id'")->row()->ATT;
				if($submission > 0) {
					
					$response['status'] 	= 'N';
					$response['message']	= 'You have already submitted the evaluation for this event.';
					
					return $response;
				}
				
				foreach($evaluations->result() as $evaluation) {
					
					$evaluation_id = $evaluation->EVALUATION_ID;
					
					$data = array(
								'evaluation_id' 	=> $evaluation->EVALUATION_ID,
								'event_id'			=> $evaluation->EVENT_ID,
								'evaluation_title'	=> $evaluation->EVALUATION_TITLE,
								'status'			=> $evaluation->STATUS,
								'questions'			=> array()
							);
					
					$questions = @$this->db->query("SELECT QUESTION_ID,
													  EVALUATION_ID,
													  QUESTION_NAME QUESTION,
													  STATUS,
													  DECODE(TYPE_ID, 1, 'radio', 'checkbox') question_type,
													  NVL(OPT_IMG_TYPE, 'N') OPT_IMG_TYPE,
													  NVL(OPT_LAYOUT, 'V') OPT_LAYOUT
													FROM EVENT_EVALUATION_QUESTION
													WHERE EVALUATION_ID         = '$evaluation_id'
													AND UPPER(NVL(STATUS, 'Y')) = 'Y'");
					
					if(!$questions) 
					{
						$this->log_db_error($this->db->error(), 'get_event_evaluation', 'event question');
					}
					
					if($questions AND $questions->num_rows() > 0) {
						
						foreach($questions->result() as $question) {
							
							$question_id = $question->QUESTION_ID;
							
							$question_data = array(
												'question_id' 		=> $question->QUESTION_ID,
												'question_desc'		=> $question->QUESTION,
												'question_type'		=> $question->QUESTION_TYPE,
												'opt_img_type'		=> $question->OPT_IMG_TYPE,
												'opt_layout'		=> $question->OPT_LAYOUT,
												'options'			=> array()
											 );
											 
							$options = @$this->db->query("SELECT QUESTION_ID,
															  OPTION_ID,
															  OPTION_NAME,
															  STATUS,
															  OPTION_DESC,
															  ICON_URL
															FROM EVENT_EVALUATION_OPTIONS
															WHERE QUESTION_ID           = '$question_id'
															AND UPPER(NVL(STATUS, 'Y')) = 'Y'");
							
							if(!$options) 
							{
								$this->log_db_error($this->db->error(), 'get_event_evaluation', 'question option');
							}
							
							if($options AND $options->num_rows() > 0){
								
								foreach($options->result() as $option) {
									
									$option_data = array(
														'option_id'		=> $option->OPTION_ID,
														'option_desc'	=> $option->OPTION_NAME,
														'icon_url'		=> base_url().$option->ICON_URL
												   );
									array_push($question_data['options'], $option_data);
								}
							}
							array_push($data['questions'], $question_data);
						}
					}
					array_push($response, $data);
				}
				
				$response['status'] 	= 'Y';
				$response['message']	= 'The session evaluation has been loaded.';
			} else {
				$response['status'] 	= 'N';
				$response['message']	= 'The event evaluation is currently inactive.';
			}
		
			
			return $response;			
		}
		
		# Save event evaluation
		public function save_event_evaluation($response)
		{
			$db2= $this->load->database('trans', TRUE);	
			
			$db2->trans_begin();
			//echo "<pre>";
			//print_r($data);			
			foreach($response as $pc) 
			{
				$evaluation_id	= $pc['EVALUATION_ID'];
				$user_id		= $pc['USER_ID'];
				$question_id 	= $pc['QUESTION_ID'];
				$statements		= $pc['OPTION_ID'];
				
				if(is_array($statements) && count($statements) > 1) {
	
					foreach($statements as  $val ) {

						$data = array(
									'EVALUATION_ID'	=> $evaluation_id,
									'USER_ID'		=> $user_id,
									'QUESTION_ID'	=> $question_id,
									'OPTION_ID'		=> $val
								);
	
						$status = @$db2->insert('EVENT_EVALUATION_RESPONSE', $data); 
						
						if(!$status) 
						{
							$this->log_db_error($db2->error(), 'save_event_evaluation', '');
						} 
					}
									
				} else {
					
					$data = array(
								'EVALUATION_ID'	=> $evaluation_id,
								'USER_ID'		=> $user_id,
								'QUESTION_ID'	=> $question_id,
								'OPTION_ID'		=> $statements[0]
							);
	
					$status = @$db2->insert('EVENT_EVALUATION_RESPONSE', $data); 
					
					if(!$status) 
					{
						$this->log_db_error($db2->error(), 'save_event_evaluation', '');

					}
				}
			} // end foreach
			
			if ($db2->trans_status() === FALSE){
							
				$db2->trans_rollback();
				
				$result = 0;
				
			} else {
				$db2->trans_commit();
				$result = 1;
			}
			//$result = $this->db->insert('APP_NOTIFICATION_RESPONSE', $arr_data);
			
			if($result) {				
				return true;
			} else {
				return false;	
			}
		}
		
		# Session Evaluation
		public function get_session_evaluation($event_id, $schedule_id, $user_id) 
		{
			$response = array();
			
			$evaluations = @$this->db->query("SELECT EE.EVALUATION_ID,
												  EE.EVENT_ID,
												  EE.EVALUATION_TITLE,
												  EE.STATUS,
												  EES.SCHEDULE_ID
												FROM EVENT_EVALUATION EE,
												  EVENT_EVALUATION_SESSIONS EES
												WHERE EE.EVALUATION_ID        = EES.EVALUATION_ID
												AND upper(NVL(EES.status,'Y')) = 'Y'
												AND EE.event_id               = '$event_id'
												AND EES.SCHEDULE_ID           = '$schedule_id'");				
			
			if(!$evaluations) 
			{
				$this->log_db_error($this->db->error(), 'get_session_evaluation', '');
			}
			
			if($evaluations AND $evaluations->num_rows() > 0) {
				
				$attendance = $this->db->query("SELECT COUNT(*) ATT
												FROM event_user_schedule_qr
												WHERE user_id   = '$user_id'
												AND schedule_id = '$schedule_id'")->row()->ATT;
				if($attendance == 0) {
					
					$response['status'] 	= 'N';
					$response['message']	= 'Session evaluation requires user attendance.';
					
					return $response;
				}				
				
				$submission = $this->db->query("SELECT COUNT(*) att
													FROM bssdata.event_evaluation_response
													WHERE user_id 		= '$user_id'
													AND schedule_id 	= '$schedule_id'")->row()->ATT;
				if($submission > 0) {
					
					$response['status'] 	= 'N';
					$response['message']	= 'You have already submitted the evaluation for this session.';
					
					return $response;
				}
							
				foreach($evaluations->result() as $evaluation) {
					
					$evaluation_id = $evaluation->EVALUATION_ID;

					$data = array(
								'event_id'			=> $evaluation->EVENT_ID,
								'schedule_id'		=> $evaluation->SCHEDULE_ID,
								'evaluation_id' 	=> $evaluation->EVALUATION_ID,
								'evaluation_title'	=> $evaluation->EVALUATION_TITLE,
								'status'			=> $evaluation->STATUS,
								'questions'			=> array()
							);
					
					$questions = @$this->db->query("SELECT QUESTION_ID,
													  EVALUATION_ID,
													  QUESTION_NAME QUESTION,
													  STATUS,
													  DECODE(TYPE_ID, 1, 'radio', 'checkbox') question_type,
													  NVL(OPT_IMG_TYPE, 'N') OPT_IMG_TYPE,
													  NVL(OPT_LAYOUT, 'V') OPT_LAYOUT
													FROM EVENT_EVALUATION_QUESTION
													WHERE EVALUATION_ID         = '$evaluation_id'
													AND UPPER(NVL(STATUS, 'Y')) = 'Y'
													ORDER BY QUESTION_ID ASC");
					
					if(!$questions) 
					{
						$this->log_db_error($this->db->error(), 'get_session_evaluation', 'event question');
					}
					
					if($questions AND $questions->num_rows() > 0) {
												
						foreach($questions->result() as $question) {
							
							$question_id = $question->QUESTION_ID;
							
							$question_data = array(
												'question_id' 		=> $question->QUESTION_ID,
												'question_desc'		=> $question->QUESTION,
												'question_type'		=> $question->QUESTION_TYPE,
												'opt_img_type'		=> $question->OPT_IMG_TYPE,
												'opt_layout'		=> $question->OPT_LAYOUT,
												'show'				=> 'Y', //(($question->QUESTION_ID == 11) ? 'Y' : 'N'),
												'options'			=> array()
											 );
											 
							$options = @$this->db->query("SELECT QUESTION_ID,
															  OPTION_ID,
															  OPTION_NAME,
															  STATUS,
															  OPTION_DESC,
															  ICON_URL
															FROM EVENT_EVALUATION_OPTIONS
															WHERE QUESTION_ID           = '$question_id'
															AND UPPER(NVL(STATUS, 'Y')) = 'Y'
															ORDER BY option_id ASC");
							
							if(!$options) 
							{
								$this->log_db_error($this->db->error(), 'get_session_evaluation', 'question option');
							}
							
							if($options AND $options->num_rows() > 0){
								
								$arr_ops = array(32, 33);
								
								foreach($options->result() as $option) {
									
									$show_next = (in_array($option->OPTION_ID, $arr_ops) || $question_id == 12) ? 'Y' : 'N';
									
									$option_data = array(
														'option_id'		=> $option->OPTION_ID,
														'option_desc'	=> $option->OPTION_NAME,
														'show_next'		=> 'Y', //$show_next,
														'icon_url'		=> base_url().$option->ICON_URL
												   );
									array_push($question_data['options'], $option_data);
								}
							}
							array_push($data['questions'], $question_data);
						}
					}
					array_push($response, $data);
				}
				$response['status'] 	= 'Y';
				$response['message']	= 'The session evaluation  has been loaded.';
			} else {
								
				$response['status'] 	= 'N';
				$response['message']	= 'The session evaluation  is currently inactive.';
			}
		
			
			return $response;			
		}
		
		# Save session evaluation
		public function save_session_evaluation($response)
		{
			$db2= $this->load->database('trans', TRUE);	
			
			$db2->trans_begin();
			//echo "<pre>";
			//print_r($data);			
			foreach($response as $pc) 
			{
				$evaluation_id	= $pc['EVALUATION_ID'];
				$schedule_id	= $pc['SCHEDULE_ID'];
				$user_id		= $pc['USER_ID'];
				$question_id 	= $pc['QUESTION_ID'];
				$statements		= $pc['OPTION_ID'];
				
				if(is_array($statements) && count($statements) > 1) {
	
					foreach($statements as  $val ) {

						$data = array(
									'EVALUATION_ID'	=> $evaluation_id,
									'SCHEDULE_ID'	=> $schedule_id,
									'USER_ID'		=> $user_id,
									'QUESTION_ID'	=> $question_id,
									'OPTION_ID'		=> $val
								);
	
						$status = @$db2->insert('EVENT_EVALUATION_RESPONSE', $data); 
						
						if(!$status) 
						{
							$this->log_db_error($db2->error(), 'save_event_evaluation', '');
						} 
					}
									
				} else {
					
					if(isset($statements[0]))
					{
						$data = array(
									'EVALUATION_ID'	=> $evaluation_id,
									'SCHEDULE_ID'	=> $schedule_id,
									'USER_ID'		=> $user_id,
									'QUESTION_ID'	=> $question_id,
									'OPTION_ID'		=> $statements[0]
								);
		
						$status = @$db2->insert('EVENT_EVALUATION_RESPONSE', $data); 
						
						if(!$status) 
						{
							$this->log_db_error($db2->error(), 'save_event_evaluation', '');
	
						}
					}
				}
			} // end foreach
			
			if ($db2->trans_status() === FALSE){
							
				$db2->trans_rollback();
				
				$result = 0;
				
			} else {
				$db2->trans_commit();
				$result = 1;
			}
			//$result = $this->db->insert('APP_NOTIFICATION_RESPONSE', $arr_data);
			
			if($result) {				
				return true;
			} else {
				return false;	
			}
		}
		
		# Event Resources
		public function get_sponsors($event_id) 
		{
			$response = array('categories' => array(), 'sponsors' => array());
			
			if($event_id) {
								
				$categories = @$this->db->query("SELECT category_id,
												  category_name,
												  status,
												  logo_width,
												  logo_height
												FROM event_sponsors_category
												WHERE upper(nvl(status, 'Y')) = 'Y'
												ORDER BY category_name");
				//echo $this->db->last_query(); exit;
				if(!$categories) 
				{
					$this->log_db_error($this->db->error(), 'get_sponsors', '');
				}
				
				if($categories AND $categories->num_rows() > 0) {
					
					foreach ($categories->result() as $category)
					{					
						$category_id 	= $category->CATEGORY_ID;
						
						$data = array(
									'category_id' 	=> $category->CATEGORY_ID,
									'category_name'	=> $category->CATEGORY_NAME,
									'status'		=> $category->STATUS,
									'logo_width'	=> $category->LOGO_WIDTH,
									'logo_height'	=> $category->LOGO_HEIGHT
								);
						
						array_push($response['categories'], $data);
					}
				}
				
	
				$sponsors = @$this->db->query("SELECT sponsor_id,
												  category_id,
												  event_id,
												  title,
												  SUBSTRB(DBMS_LOB.SUBSTR(description, 4000, 1 ), 1, 4000) description,
												  website,
												  logo,
												  status,
												  featured,
												  sort_order
												FROM event_sponsors
												WHERE event_id              = '$event_id'
												AND upper(NVL(status, 'Y')) = 'Y'
												ORDER BY sort_order");
				//echo $this->db->last_query(); exit;
				if(!$sponsors) 
				{
					$this->log_db_error($this->db->error(), 'get_sponsors', '');
				}
				
				if($sponsors AND $sponsors->num_rows() > 0) {
					
					foreach ($sponsors->result() as $sponsor)
					{					
						$category_id 	= $category->CATEGORY_ID;
						
						$s_data = array(
									'sponsor_id'	=> $sponsor->SPONSOR_ID,
									'category_id'	=> $sponsor->CATEGORY_ID,
									'event_id'		=> $sponsor->EVENT_ID,
									'title'			=> $sponsor->TITLE,
									'description'	=> $sponsor->DESCRIPTION,
									'website'		=> $sponsor->WEBSITE,
									'logo'			=> 'http://beams.beaconhouse.edu.pk/admin/events/upload/sponsors/'.$sponsor->LOGO,
									'status'		=> $sponsor->STATUS,
									'featured'		=> $sponsor->FEATURED,
									'sort_order'	=> $sponsor->SORT_ORDER
								);
						
						array_push($response['sponsors'], $s_data);
					}
				}
			}

			return $response;
		}
		
		# Event Contributions
		public function get_contributions($event_id, $user_id) 
		{
			$response = array('contributions' => array(), 'contribution_types' => array());
						
			if($event_id) {
				
				if($user_id == "") { $user_id = 0; }
				
				$contributions = @$this->db->query("SELECT ec.cont_id,
													  ec.event_id,
													  ec.user_id,
													  ecd.type_id,
													  ect.type_desc,
													  ecd.cont_title,
													  ecd.cont_link,
													  ecd.cont_file,
													  ecd.file_path,
													  ecd.status,
													  DECODE(ec.user_id, $user_id, 1, 0) my_cont
													FROM event_contributions ec,
													  event_contributions_detail ecd,
													  event_contribution_type ect
													WHERE ec.cont_id                = ecd.cont_id
													AND ecd.type_id                 = ect.type_id
													AND ec.event_id                 = '$event_id'
													AND UPPER(NVL(ecd.status, 'Y')) = 'Y'");
				//echo $this->db->last_query(); exit;
				if(!$contributions) 
				{
					$this->log_db_error($this->db->error(), 'get_contributions', '');
				}
				
				if($contributions AND $contributions->num_rows() > 0) {
					
					foreach ($contributions->result() as $contribution)
					{					
						$contribution_id 	= $contribution->CONT_ID;
						
						$data = array(
									'cont_id' 		=> $contribution->CONT_ID,
									'user_id'		=> $contribution->USER_ID,
									'type_id'		=> $contribution->TYPE_ID,
									'type_desc'		=> $contribution->TYPE_DESC,
									'cont_title'	=> $contribution->CONT_TITLE,
									'cont_link'		=> $contribution->CONT_LINK,
									'cont_file'		=> $contribution->FILE_PATH.$contribution->CONT_FILE,
									'status'		=> $contribution->STATUS,
									'my_cont'		=> $contribution->MY_CONT
								);
						
						array_push($response['contributions'], $data);
					}
				}
			}
			
			# Contribution types
			$types = @$this->db->query("SELECT type_id, type_desc FROM event_contribution_type WHERE UPPER(NVL(STATUS, 'Y')) = 'Y' ORDER BY type_id");
			//echo $this->db->last_query(); exit;
			
			if(!$types) 
			{
				$this->log_db_error($this->db->error(), 'get_contributions', '');
			}
			
			if($types AND $types->num_rows() > 0) {
				
				foreach ($types->result() as $type)
				{										
					$x_data = array(
								'type_id'	=> $type->TYPE_ID,
								'type_desc'	=> $type->TYPE_DESC
							);					
					array_push($response['contribution_types'], $x_data);
				}
			}
				
			return $response;
		}
		
		# Save contribution link
		public function save_contribution_link($data) 
		{
			$db2= $this->load->database('trans', TRUE);
	
			if($data AND count($data) > 0)
			{
				// Prepare event contribution data
				$cont_id = $db2->query('SELECT EVENT_APP_SEQ_CONT_ID.nextVal AS CONT_ID FROM dual')->row()->CONT_ID;
				
				$ec_data = array(
								'USER_ID'	=> $data['user_id'],
								'CONT_ID'	=> $cont_id,
								'EVENT_ID'	=> $data['event_id']
							);
				
				$status = @$db2->insert('EVENT_CONTRIBUTIONS', $ec_data);
				
				if($status) {
					
					// Prepare event contribution detail data
					$ecd_data = array(
									'CONT_ID'		=> $cont_id,
									'TYPE_ID'		=> $data['type_id'],
									'CONT_TITLE'	=> $data['cont_title'],
									'CONT_LINK'		=> $data['cont_link'],
									'STATUS'		=> 'N'
								);	
					$status = @$db2->insert('EVENT_CONTRIBUTIONS_DETAIL', $ecd_data);
					
					if($status) { 
						return true;
					} else {
						return false;
					}
				}
				
			} else {
				return false;
			}
		}
		
		# Save contribution picture
		public function save_contribution_picture($data, $file, $path) 
		{
			$db2= $this->load->database('trans', TRUE);
			
			if($data AND count($data) > 0)
			{
				// Prepare event contribution data
				$cont_id = $db2->query('SELECT EVENT_APP_SEQ_CONT_ID.nextVal AS CONT_ID FROM dual')->row()->CONT_ID;
				
				$ec_data = array(
								'USER_ID'	=> $data['user_id'],
								'CONT_ID'	=> $cont_id,
								'EVENT_ID'	=> $data['event_id']
							);
				
				$status = @$db2->insert('EVENT_CONTRIBUTIONS', $ec_data);
				
				if($status) {
					
					// Prepare event contribution detail data
					$ecd_data = array(
									'CONT_ID'		=> $cont_id,
									'TYPE_ID'		=> $data['type_id'],
									'CONT_TITLE'	=> $data['cont_title'],
									'CONT_FILE'		=> $file,
									'FILE_PATH'		=> $path,
									'STATUS'		=> 'N'
								);	
					$status = @$db2->insert('EVENT_CONTRIBUTIONS_DETAIL', $ecd_data);
					
					if($status) { 
						return true;
					} else {
						return false;
					}
				}
				
			} else {
				return false;
			}
		}
		
		# Save contribution picture
		public function save_contribution_video($data, $file, $path) 
		{
			$db2= $this->load->database('trans', TRUE);
			
			if($data AND count($data) > 0)
			{
				// Prepare event contribution data
				$cont_id = $db2->query('SELECT EVENT_APP_SEQ_CONT_ID.nextVal AS CONT_ID FROM dual')->row()->CONT_ID;
				
				$ec_data = array(
								'USER_ID'	=> $data['user_id'],
								'CONT_ID'	=> $cont_id,
								'EVENT_ID'	=> $data['event_id']
							);
				
				$status = @$db2->insert('EVENT_CONTRIBUTIONS', $ec_data);
				
				if($status) {
					
					// Prepare event contribution detail data
					$ecd_data = array(
									'CONT_ID'		=> $cont_id,
									'TYPE_ID'		=> $data['type_id'],
									'CONT_TITLE'	=> $data['cont_title'],
									'CONT_FILE'		=> $file,
									'FILE_PATH'		=> $path,
									'STATUS'		=> 'N'
								);	
					$status = @$db2->insert('EVENT_CONTRIBUTIONS_DETAIL', $ecd_data);
					
					if($status) { 
						return true;
					} else {
						return false;
					}
				}
				
			} else {
				return false;
			}
		}
		
		# User network
		public function get_user_network($user_id) 
		{
			$response = array();
			
			if($user_id) {
				
				$users = @$this->db->query("SELECT MN.NETWORK_ID,
											  MN.NETWORK_USER_ID,
											  NVL(MN.ACCEPT_STATUS, 'N') ACCEPT_STATUS,
											  EU.USER_ID,
											  EU.FIRST_NAME,
											  EU.LAST_NAME,
											  EU.IMAGE
											FROM
											  (SELECT NETWORK_ID,
												ADD_USER_ID NETWORK_USER_ID,
												DECODE(ACCEPT_STATUS, 'N', 'P', 'Y') ACCEPT_STATUS
											  FROM EVENT_USER_NETWORK
											  WHERE USER_ID                      = '$user_id'
											  --AND UPPER(NVL(ACCEPT_STATUS, 'Y')) = 'Y'
											  UNION
											  SELECT NETWORK_ID,
												USER_ID NETWORK_USER_ID,
												DECODE(ACCEPT_STATUS, 'N', 'A', 'Y') ACCEPT_STATUS
											  FROM EVENT_USER_NETWORK
											  WHERE ADD_USER_ID                  = '$user_id'
											  --AND UPPER(NVL(ACCEPT_STATUS, 'Y')) = 'Y'
											  ) MN,
											  EVENT_USERS EU
											WHERE MN.NETWORK_USER_ID = EU.USER_ID");

				if(!$users) 
				{
					$this->log_db_error($this->db->error(), 'get_user_network', '');
					
					return $response;
				}	
				
				if($users AND $users->num_rows() > 0) {
					
					foreach($users->result() as $user) 
					{
						$data = array(
									'network_id' 		=> $user->NETWORK_ID,
									'network_user_id' 	=> $user->NETWORK_USER_ID,
									'user_id' 			=> $user->USER_ID,
									'first_name' 		=> ucwords($user->FIRST_NAME),
									'last_name' 		=> ucwords($user->LAST_NAME),
									'display_name'		=> ucwords($user->FIRST_NAME) . ' ' . ucwords($user->LAST_NAME),
									'image_url' 		=> $user->IMAGE,
									'status'			=> $user->ACCEPT_STATUS
								);
								
						array_push($response, $data);
					}
				}
			}
			return $response;
		}
		
		# Save user network
		public function save_user_network($user_id, $add_user_id) 
		{			
			if($user_id AND $add_user_id) {
				
				$db2= $this->load->database('trans', TRUE);	
				
				$network_id = 	$db2->query('SELECT EVENT_APP_SEQ_NETWORK_ID.nextVal AS NETWORK_ID FROM dual')->row()->NETWORK_ID;
				
				$db2->set('NETWORK_ID', $network_id);
				$db2->set('USER_ID', $user_id);
				$db2->set('ADD_USER_ID', $add_user_id);
				$db2->set('ACCEPT_STATUS', 'N');
				$db2->set('CREATE_DATE', '(SELECT SYSDATE FROM DUAL)', FALSE);
				$status = @$db2->insert('EVENT_USER_NETWORK');
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'save_user_network', '');
					return false;
				} 
				else 
				{					
					$this->schedule_user_network_notification($user_id, $add_user_id);
					return true;
				}
			
			} else {
				return false;
			}
		}
		
		# Save user network
		public function accept_user_network($network_id, $add_user_id) 
		{			
			if($network_id AND $add_user_id) {
				
				$db2= $this->load->database('trans', TRUE);	

				$db2->set('ACCEPT_STATUS', 'Y');
				$db2->set('ACCEPT_DATE', '(SELECT SYSDATE FROM DUAL)', FALSE);
				$db2->where('NETWORK_ID', $network_id);
				$db2->where('ADD_USER_ID', $add_user_id);
				$status = @$db2->update('EVENT_USER_NETWORK');
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'accept_user_network', '');
					
					return false;
				} else {
					return true;
				}
			
			} else {
				return false;
			}
		}
		
		# Decline user network
		public function decline_user_network($network_id, $add_user_id) 
		{			
			if($network_id AND $add_user_id) {
				
				$db2= $this->load->database('trans', TRUE);	
				
				$query = @$db2->query("SELECT NETWORK_ID,
											  USER_ID,
											  PARTICIPANT_ID,
											  ADD_USER_ID,
											  'D' ACCEPT_STATUS,
											  ACCEPT_DATE,
											  CREATE_DATE,
											  SYSDATE UPDT_DATE
											FROM EVENT_USER_NETWORK
											WHERE NETWORK_ID = '$network_id'
											AND ADD_USER_ID  = '$add_user_id'");
				
				if($query->num_rows() == 1)
				{
					$row = $query->row();
					//$arr = $query->result_array();
					//print_r($arr[0]); exit;
					$data = array( 
								'NETWORK_ID' 		=> $row->NETWORK_ID,
								'USER_ID' 			=> $row->USER_ID,
								'PARTICIPANT_ID'	=> $row->PARTICIPANT_ID,
								'ADD_USER_ID' 		=> $row->ADD_USER_ID,
								'ACCEPT_STATUS'		=> $row->ACCEPT_STATUS,
								'ACCEPT_DATE' 		=> $row->ACCEPT_DATE,
								'CREATE_DATE' 		=> $row->CREATE_DATE,
								'UPDT_DATE' 		=> $row->UPDT_DATE
							);
							
					$insert = @$db2->insert('EVENT_USER_NETWORK_HISTORY', $data); 
					
					if(!$insert) 
					{
						$this->log_db_error($db2->error(), 'decline_user_network', '');
						
						return false;
					} else {
						
						$db2->where('NETWORK_ID', $network_id);
						$db2->where('ADD_USER_ID', $add_user_id);
						$status = @$db2->delete('EVENT_USER_NETWORK');
						
						if(!$status) 
						{
							$this->log_db_error($db2->error(), 'decline_user_network', '');
							
							return false;
						} else {
							return true;
						}
					}
				}
				else
				{ 
					return false;
				}			
			
			} else {
				return false;
			}
		}
		
		# schedule user network notification
		public function schedule_user_network_notification($from_user_id, $to_user_id) 
		{			
			$db2= $this->load->database('trans', TRUE);	
			
			$query = @$db2->query("SELECT NETWORK_ID,
										  USER_ID,
										  PARTICIPANT_ID,
										  ADD_USER_ID,
										  'D' ACCEPT_STATUS,
										  ACCEPT_DATE,
										  CREATE_DATE,
										  SYSDATE UPDT_DATE
										FROM EVENT_USER_NETWORK
										WHERE NETWORK_ID = '$network_id'
										AND ADD_USER_ID  = '$add_user_id'");
			
			if($query->num_rows() == 1)
			{
				$row = $query->row();
				//$arr = $query->result_array();
				//print_r($arr[0]); exit;
				$data = array( 
							'NETWORK_ID' 		=> $row->NETWORK_ID,
							'USER_ID' 			=> $row->USER_ID,
							'PARTICIPANT_ID'	=> $row->PARTICIPANT_ID,
							'ADD_USER_ID' 		=> $row->ADD_USER_ID,
							'ACCEPT_STATUS'		=> $row->ACCEPT_STATUS,
							'ACCEPT_DATE' 		=> $row->ACCEPT_DATE,
							'CREATE_DATE' 		=> $row->CREATE_DATE,
							'UPDT_DATE' 		=> $row->UPDT_DATE
						);
						
				$insert = @$db2->insert('EVENT_USER_NETWORK_HISTORY', $data); 
				
				if(!$insert) 
				{
					$this->log_db_error($db2->error(), 'decline_user_network', '');
					
					return false;
				} else {
					
					$db2->where('NETWORK_ID', $network_id);
					$db2->where('ADD_USER_ID', $add_user_id);
					$status = @$db2->delete('EVENT_USER_NETWORK');
					
					if(!$status) 
					{
						$this->log_db_error($db2->error(), 'decline_user_network', '');
						
						return false;
					} else {
						return true;
					}
				}
			}
			else
			{ 
				return false;
			}			
		
		
		}
		
		# User messages
		public function get_user_messages($from_user_id, $to_user_id) 
		{
			$response = array('user_info' => array(), 'messages' => array(), 'unread_count' => 0);
			
			if($from_user_id AND $to_user_id) {
				
				$response['user_info'] = $this->get_user_info($to_user_id);
				
				$messages = @$this->db->query("SELECT MSG.*
												FROM
												  (SELECT MSG_ID,
													FROM_USER_ID,
													TO_USER_ID,
													MSG_DESC,
													TO_CHAR(SENT_DATE, 'hh24:mi dd-Mon-yyyy') SENT_DATE,
													SENT_DATE SORT_DATE,
													READ_DATE,
													READ_STATUS
												  FROM EVENT_MESSAGE
												  WHERE FROM_USER_ID = '$from_user_id'
												  AND TO_USER_ID     = '$to_user_id'
												  UNION
												  SELECT MSG_ID,
													FROM_USER_ID,
													TO_USER_ID,
													MSG_DESC,
													TO_CHAR(SENT_DATE, 'hh24:mi dd-Mon-yyyy') SENT_DATE,
													SENT_DATE SORT_DATE,
													READ_DATE,
													READ_STATUS
												  FROM EVENT_MESSAGE
												  WHERE FROM_USER_ID = '$to_user_id'
												  AND TO_USER_ID     = '$from_user_id'
												  ) MSG
												ORDER BY MSG.SORT_DATE ASC");
				
				if(!$messages) 
				{
					$this->log_db_error($this->db->error(), 'get_user_messages', '');
					
					return $response;
				}	
				
				if($messages AND $messages->num_rows() > 0) {
					
					foreach($messages->result() as $message) 
					{
						$data = array(
									'msg_id' 		=> $message->MSG_ID,
									'from_user_id' 	=> $message->FROM_USER_ID,
									'to_user_id' 	=> $message->TO_USER_ID,
									'msg_desc' 		=> $message->MSG_DESC,
									'sent_date' 	=> $message->SENT_DATE,
									'read_date' 	=> $message->READ_DATE,
									'sort_date'		=> $message->SORT_DATE,
									'read_status' 	=> $message->READ_STATUS,
								);
								
						array_push($response['messages'], $data);
					}
				}
				
				# Mark user notifications as read
				$response['unread_count'] = $this->user_messages_read($from_user_id, $to_user_id);
			}
			return $response;
		}
		
		# User messages
		public function user_messages_read($from_user_id, $to_user_id) 
		{
			$read_count = 0;
			
			if($from_user_id AND $to_user_id) 
			{
				$db2= $this->load->database('trans', TRUE);	

				$db2->set('READ_STATUS', 'Y');
				$db2->set('READ_DATE', '(SELECT SYSDATE FROM DUAL)', FALSE);
				$db2->where('FROM_USER_ID', $to_user_id);
				$db2->where('TO_USER_ID', $from_user_id);
				$db2->where('READ_STATUS', 'N');
				
				$status = @$db2->update('EVENT_MESSAGE');
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'save_user_messages', '');
				} 
				else 
				{
					$read_count = $db2->affected_rows();
				}
			}
			
			return $read_count;
		}
		
		# Save user messages
		public function save_user_messages($data) 
		{			
			if($data) {
				
				$db2= $this->load->database('trans', TRUE);	
				
				$msg_id = 	$db2->query('SELECT EVENT_APP_SEQ_MSG_ID.nextVal AS MSG_ID FROM dual')->row()->MSG_ID;
				
				$message = str_replace("'", "`", str_replace('"', '`', $data['message']));
				
				$db2->set('MSG_ID', $msg_id);
				$db2->set('FROM_USER_ID', $data['from_user_id']);
				$db2->set('TO_USER_ID', $data['to_user_id']);
				$db2->set('MSG_DESC', $message);
				$db2->set('READ_STATUS', 'N');
				$db2->set('SENT_DATE', '(SELECT SYSDATE FROM DUAL)', FALSE);
				
				$status = @$db2->insert('EVENT_MESSAGE');
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'save_user_messages', '');
					
					return false;
				} else {
					return true;
				}
			
			} else {
				return false;
			}
		}
		
		# User search
		public function get_user_search($user_id, $keyword) 
		{
			$response = array();
			
			if($keyword) {
				
				$user_network_users = '';
				
				if($user_id) {
					
					$query = @$this->db->query("SELECT NETWORK_ID,
												  rtrim (xmlagg (xmlelement (e, NETWORK_USER_ID
												  || ',')).extract ('//text()'), ',') NETWORK_USER_ID
												FROM
												  (SELECT $user_id NETWORK_ID,
													ADD_USER_ID NETWORK_USER_ID,
													ACCEPT_STATUS
												  FROM EVENT_USER_NETWORK
												  WHERE USER_ID                      = '$user_id'
												  --AND UPPER(NVL(ACCEPT_STATUS, 'Y')) = 'Y'
												  UNION
												  SELECT $user_id NETWORK_ID,
													USER_ID NETWORK_USER_ID,
													ACCEPT_STATUS
												  FROM EVENT_USER_NETWORK
												  WHERE ADD_USER_ID                  = '$user_id'
												  --AND UPPER(NVL(ACCEPT_STATUS, 'Y')) = 'Y'
												  )
												GROUP BY NETWORK_ID");
					if(!$query)
					{
						$this->log_db_error($this->db->error(), 'get_user_search', '');
					}
					
					if($query AND $query->num_rows() > 0) 
					{
						$user_network_users = "AND eu.user_id NOT IN (".$query->row()->NETWORK_USER_ID.",$user_id)";
					}
				}
											
											
				$users = @$this->db->query("SELECT eu.user_id,
											  NVL(eu.participant_id, 0) participant_id,
											  eu.first_name,
											  eu.last_name,
											  eu.email,
											  eu.image,
											  eu.qr,
											  upper(eu.gender) gender,
											  es.allow_add_network
											FROM BSSDATA.event_users eu,
												event_settings es
											WHERE eu.user_id                          = es.user_id
											AND upper(NVL(es.allow_add_network, 'N')) = 'Y'
											AND (trim(lower(eu.first_name))
												  || ' '
												  || trim(lower(eu.last_name)) LIKE trim(lower('%$keyword%')))
											AND rownum < 1000
											$user_network_users");
				if(!$users) 
				{
					$this->log_db_error($this->db->error(), 'get_user_search', '');
					
					return $response;
				}	
				
				if($users AND $users->num_rows() > 0) {
					
					foreach($users->result() as $user) 
					{
						$data = array(
								'user_id' 		=> $user->USER_ID,
								'participant_id'=> $user->PARTICIPANT_ID,	
								'first_name' 	=> $user->FIRST_NAME,
								'last_name' 	=> $user->LAST_NAME,
								'email' 		=> $user->EMAIL,
								'image' 		=> $user->IMAGE,
								'gender'		=> $user->GENDER
							);
								
						array_push($response, $data);
					}
				}
			}
			return $response;
		}
		
		# User Notifications
		public function get_user_notifications_xxxxxxxxxxxxxxx($user_id, $notification_id = '') 
		{			
			$result = array();
			
			if($user_id) {
	
				$where_notification_id = '';
				
				if($notification_id != '') {
					$where_notification_id = " AND EN.NOTIFICATION_ID = '$notification_id'";
				}
				
				$query = @$this->db->query("SELECT EN.NOTIFICATION_ID,
											  EN.TITLE,
											  EN.MSG,
											  TO_CHAR(EN.SENT_DATE, 'fmDay, dd-Mon-yyyy hh:mi AM') DISPLAY_DATE,
											  TO_CHAR(EN.SENT_DATE, 'yyyy/mm/dd') DATE_FILTER,
											  ENU.USER_ID,
											  NVL(ENU.STATUS, 'N') READ_STATUS
											FROM EVENT_NOTIFICATION EN,
											  EVENT_NOTIFICATION_USERS ENU
											WHERE EN.NOTIFICATION_ID = ENU.NOTIFICATION_ID
											AND ENU.USER_ID          = '$user_id'
											$where_notification_id
											ORDER BY EN.SENT_DATE DESC");
				//echo $this->db->last_query(); exit;	
				
				if(!$query) 
				{
					$this->log_db_error($this->db->error(), 'get_user_notifications', '');

				}
							
				if ($query AND $query->num_rows() > 0) {
					
					foreach ($query->result() as $row)
					{
	
						$data = 	array(
											"notification_id"	=> $row->NOTIFICATION_ID,
											"user_id"			=> $row->USER_ID,
											"title"				=> $row->TITLE,
											"body"				=> $row->MSG,
											"display_date"		=> $row->DISPLAY_DATE,
											"date_filter"		=> $row->DATE_FILTER,
											"read_status" 		=> $row->READ_STATUS
									);
										
						array_push($result, $data);
					}					
				} 
			}
	
			return $result;
		}
		
		# User Notifications
		public function get_user_notifications($user_id, $notification_id = '', $read_status) 
		{			
			$result = array('user_notifications' => array(), 'unread_count' => 0);
			
			if($user_id) {
	
				$where_notification_id = '';
				
				if($notification_id != '') {
					$where_notification_id = " AND NOTIFICATION_ID = '$notification_id'";
				}
				
				$query = @$this->db->query("SELECT NOTIFICATION_ID,
											  EVENT_ID,
											  SCHEDULE_ID,
											  USER_ID,
											  MESSAGE,
											  TO_CHAR(SCHEDULE_DATE, 'fmDay, FMDD-Mon-yyyy, HH:MI AM') DISPLAY_DATE,
											  TO_CHAR(SCHEDULE_DATE, 'yyyy/mm/dd') DATE_FILTER,
											  NVL(READ_STATUS, 0) READ_STATUS,
											  NVL(NOTIFICATION_TYPE, 'notification') NOTIFICATION_TYPE,
											  CLICK_ACTION
											FROM EVENT_USER_NOTIFICATIONS
											WHERE USER_ID 	  = '$user_id'
											AND FOR_ADMIN 	  = 'N'
											AND SCHEDULE_DATE < SYSDATE
											$where_notification_id
											ORDER BY READ_STATUS ASC, SCHEDULE_DATE DESC");
				//echo $this->db->last_query(); exit;	
				
				if(!$query) 
				{
					$this->log_db_error($this->db->error(), 'get_user_notifications', '');
	
				}
				
				$unread_count = 0;
							
				if ($query AND $query->num_rows() > 0) {
					
					foreach ($query->result() as $row)
					{
						$unread_count += ($row->READ_STATUS == 0) ? 1 : 0;
						
						$data = 	array(
											"notification_id"	=> $row->NOTIFICATION_ID,
											"event_id"			=> $row->EVENT_ID,
											"schedule_id"		=> $row->SCHEDULE_ID,
											"user_id"			=> $row->USER_ID,
											"message"			=> $row->MESSAGE,
											"display_date"		=> $row->DISPLAY_DATE,
											"date_filter"		=> $row->DATE_FILTER,
											"read_status" 		=> $row->READ_STATUS,
											"notification_type" => $row->NOTIFICATION_TYPE,
											"click_action" 		=> $row->CLICK_ACTION
											//"device_token"		=> $row->DEVICE_TOKEN,
											//"device_identifier" => $row->DEVICE_IDENTIFIER
									);
										
						array_push($result['user_notifications'], $data);
					}					
				} 
				
				# Mark user notifications as read
				if($read_status == 'Y') 
				{
					$result['unread_count'] = $this->read_user_notifications($user_id, '1');
				} 
				else {
					$result['unread_count'] = $unread_count;
				}
			}
	
			return $result;
		}
		
		# Mark notification as read
		public function read_user_notifications($user_id, $status_read = '0')
		{
			$unread_count = 0;
			
			$db2= $this->load->database('trans', TRUE);	
			
			$db2->set("READ_STATUS", $status_read);
			$db2->set('READ_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);
			$db2->where("USER_ID", $user_id, false);
			$db2->where("NVL(READ_STATUS, '0') = ", '0');
			$db2->where("SCHEDULE_DATE < ", '(SELECT SYSDATE FROM DUAL)', FALSE);
			$status = @$db2->update('EVENT_USER_NOTIFICATIONS');
			
			if(!$status) 
			{
				$this->log_db_error($this->db->error(), 'read_user_notifications', '');
				$unread_count = 0;
			} else {
				$unread_count = $db2->affected_rows();
			} 
			
			return $unread_count;
		}
	
		# Event Exhibitors
		public function get_exhibitors($event_id) 
		{
			$response = array('categories' => array(), 'exhibitors' => array());
						
			if($event_id) {
				
				$categories = @$this->db->query("SELECT category_id,
												  category_name,
												  status
												FROM event_exhibitors_category
												WHERE status = 'Y'
												ORDER BY category_name");
				//echo $this->db->last_query(); exit;
				if(!$categories) 
				{
					$this->log_db_error($this->db->error(), 'get_exhibitors', '');
				}
				
				if($categories AND $categories->num_rows() > 0) {
					
					foreach ($categories->result() as $category)
					{					
						$category_id 	= $category->CATEGORY_ID;
						
						$data = array(
									'category_id' 	=> $category->CATEGORY_ID,
									'category_name'	=> $category->CATEGORY_NAME,
									'status'		=> $category->STATUS
								);
						
						array_push($response['categories'], $data);
					}
				}
				
	
				$exhibitors = @$this->db->query("SELECT exhibitor_id,
												  category_id,
												  event_id,
												  exhibitor_name,
												  exhibitor_details description,
												  image_name_thumb image_thumb,
												  image_name_detail image_large,
												  status
												FROM event_exhibitors
												WHERE event_id              = '$event_id'
												AND upper(NVL(status, 'Y')) = 'Y'");
				//echo $this->db->last_query(); exit;
				if(!$exhibitors) 
				{
					$this->log_db_error($this->db->error(), 'get_exhibitors', '');
				}
				
				if($exhibitors AND $exhibitors->num_rows() > 0) {
					
					foreach ($exhibitors->result() as $exhibitor)
					{					
						$exhibitor_id 	= $exhibitor->EXHIBITOR_ID;
						
						$x_data = array(
									'exhibitor_id'	=> $exhibitor->EXHIBITOR_ID,
									'category_id'	=> $exhibitor->CATEGORY_ID,
									'event_id'		=> $exhibitor->EVENT_ID,
									'exhibitor_name'=> $exhibitor->EXHIBITOR_NAME,
									'description'	=> $exhibitor->DESCRIPTION,
									'image_thumb'	=> 'http://beams.beaconhouse.edu.pk/admin/events/upload/exibitors/'.$exhibitor->IMAGE_THUMB,
									'image_large'	=> 'http://beams.beaconhouse.edu.pk/admin/events/upload/exibitors/'.$exhibitor->IMAGE_LARGE,
									'status'		=> $exhibitor->STATUS,
									'highlights'	=> array()
								);
						
						$highlights = @$this->db->query("SELECT EXHIBITOR_ID,
														  HIGHLIGHT_ID,
														  HIGHLIGHT_DESC,
														  STATUS
														FROM EVENT_EXHIBITORS_HIGHLIGHTS
														WHERE EXHIBITOR_ID          = '$exhibitor_id'
														AND UPPER(NVL(STATUS, 'Y')) = 'Y'");
						if(!$highlights)
						{
							$this->log_db_error($this->db->error(), 'get_exhibitors', '');
						}
						
						if($highlights AND $highlights->num_rows() > 0) {
							
							foreach($highlights->result() as $highlight) {
								
								$h_data = array(
										  	'hilight_id'	=> $highlight->HIGHLIGHT_ID,
											'hilight_desc'	=> $highlight->HIGHLIGHT_DESC,
											'status'		=> $highlight->STATUS
										  );
								
								array_push($x_data['highlights'], $h_data);								
							}
						}
						
						array_push($response['exhibitors'], $x_data);
					}
				}
			}

			return $response;
		}
		
		# Event Exhibitors
		public function get_fullsteam($event_id) 
		{
			$response = array('fullsteam' => array());
						
			if($event_id) {				
	
				$fullsteams = @$this->db->query("SELECT exhibitor_id,
												  event_id,
												  exhibitor_name,
												  exhibitor_details description,
												  image_name_thumb image_thumb,
												  image_name_detail image_large,
												  status
												FROM event_fullsteam
												WHERE event_id              = '$event_id'
												AND upper(NVL(status, 'Y')) = 'Y'");
				//echo $this->db->last_query(); exit;
				if(!$fullsteams) 
				{
					$this->log_db_error($this->db->error(), 'get_fullsteam', '');
				}
				
				if($fullsteams AND $fullsteams->num_rows() > 0) {
					
					foreach ($fullsteams->result() as $fullsteam)
					{					
						$exhibitor_id 	= $fullsteam->EXHIBITOR_ID;
						
						$x_data = array(
									'exhibitor_id'	=> $fullsteam->EXHIBITOR_ID,
									'event_id'		=> $fullsteam->EVENT_ID,
									'exhibitor_name'=> $fullsteam->EXHIBITOR_NAME,
									'description'	=> $fullsteam->DESCRIPTION,
									'image_thumb'	=> 'http://beams.beaconhouse.edu.pk/admin/events/upload/exibitors/'.$fullsteam->IMAGE_THUMB,
									'image_large'	=> 'http://beams.beaconhouse.edu.pk/admin/events/upload/exibitors/'.$fullsteam->IMAGE_LARGE,
									'status'		=> $fullsteam->STATUS,
									'highlights'	=> array()
								);
						
						$highlights = @$this->db->query("SELECT EXHIBITOR_ID,
														  HIGHLIGHT_ID,
														  HIGHLIGHT_DESC,
														  STATUS
														FROM EVENT_FULLSTEAM_HIGHLIGHTS
														WHERE EXHIBITOR_ID          = '$exhibitor_id'
														AND UPPER(NVL(STATUS, 'Y')) = 'Y'");
						if(!$highlights)
						{
							$this->log_db_error($this->db->error(), 'get_exhibitors', '');
						}
						
						if($highlights AND $highlights->num_rows() > 0) {
							
							foreach($highlights->result() as $highlight) {
								
								$h_data = array(
										  	'hilight_id'	=> $highlight->HIGHLIGHT_ID,
											'hilight_desc'	=> $highlight->HIGHLIGHT_DESC,
											'status'		=> $highlight->STATUS
										  );
								
								array_push($x_data['highlights'], $h_data);								
							}
						}
						
						array_push($response['fullsteam'], $x_data);
					}
				}
			}

			return $response;
		}
		
		public function get_happenings($event_id) 
		{
			$response = array('happenings' => array());
						
			$happenings = @$this->db->query("SELECT hp.id,
											  hp.title,
											  hp.categories,
											  SUBSTRB(DBMS_LOB.SUBSTR(hp.description, 4000, 1 ), 1, 4000) description,
											  hp.url,
											  hp.event_id,
											  hp.activity_id,
											  hp.image image_url,
											  hp.status,
											  to_char(entry_date, 'yyyy/mm/dd') date_filter
											FROM event_happenings hp
											WHERE nvl(hp.status, 'N') = 'Y'
											ORDER BY hp.title");
			
			if(!$happenings) 
			{
				$this->log_db_error($this->db->error(), 'get_happenings', '');
				
				 return $response;
			}
			
			if($happenings AND $happenings->num_rows() > 0) {
				
				foreach ($happenings->result() as $happening)
				{					
					
					$data = array(
								'id' 			=> $happening->ID,
								'title'			=> $happening->TITLE,
								'categories'	=> $happening->CATEGORIES,
								'description'	=> $happening->DESCRIPTION,
								'url'			=> $happening->URL,
								'event_id'		=> $happening->EVENT_ID,
								'activity_id'	=> $happening->ACTIVITY_ID,
								'image_url'		=> 'http://beams.beaconhouse.edu.pk/admin/events/'.$happening->IMAGE_URL,
								'status'		=> $happening->STATUS,
								"date_filter"	=> $happening->DATE_FILTER,
							);
					
					array_push($response['happenings'], $data);
				}
			}
			
			return $response;
		}
		
		public function get_event_teams($event_id) 
		{
			$response = array();
			
			if($event_id) {
							
				$teams = @$this->db->query("SELECT ET.TEAM_ID,
											  ET.TEAM_NAME,
											  ET.STATUS,
											  ET.EVENT_ID
											FROM EVENT_TEAMS ET
											WHERE ET.STATUS   = 'Y'
											AND ET.EVENT_ID   = '$event_id'
											ORDER BY ET.TEAM_ID");		
							  	
				if(!$teams) 
				{
					$this->log_db_error($this->db->error(), 'get_event_teams', '');
				}
				
				if($teams AND $teams->num_rows() > 0) {
					
					foreach ($teams->result() as $team)
					{					
						$team_id = $team->TEAM_ID;
						
						$data = array(
									'team_id' 		=> $team->TEAM_ID,
									'team_name'		=> $team->TEAM_NAME,
									'event_id'		=> $team->EVENT_ID,
									'status'		=> $team->STATUS,
									'team_users'	=> array()
								);
								
						$users = @$this->db->query("SELECT EU.USER_ID,
													  EU.FIRST_NAME,
													  EU.LAST_NAME,
													  EU.GENDER,
													  EU.IMAGE,
													  ETU.DESIG,
													  ETU.SORT_ORDER
													FROM EVENT_TEAM_USERS ETU,
													  EVENT_USERS EU
													WHERE ETU.USER_ID  = EU.USER_ID
													AND ETU.TEAM_ID  = '$team_id'
													AND ETU.STATUS   = 'Y'
													ORDER BY ETU.SORT_ORDER");	
						
						if(!$users) 
						{
							$this->log_db_error($this->db->error(), 'get_event_teams', '');
						}
						
						if($users AND $users->num_rows() > 0) {
							
							foreach ($users->result() as $user)
							{							
								$user_data = array(
												'user_id'		=> $user->USER_ID,
												'first_name'	=> $user->FIRST_NAME,
												'last_name'		=> $user->LAST_NAME,
												'gender'		=> $user->GENDER,
												'image'			=> $user->IMAGE,
												'desig'			=> $user->DESIG,
												'display_order' => $user->SORT_ORDER
											);
									
								array_push($data['team_users'], $user_data);
							}
						}
						
						array_push($response, $data);
					}
				}
			}
			
			return $response;
		}
		
		public function get_resources($event_id) 
		{
			$response = array();
			
			if($event_id) {
							
				$resources = @$this->db->query("SELECT RESOURCE_ID,
												  ACTIVITY_ID,
												  THEME_ID,
												  EVENT_ID,
												  TITLE,
												  RESOURCE_LINK,
												  1 DISPLAY_ORDER
												FROM EVENT_RESOURCES
												WHERE EVENT_ID = '$event_id'
												ORDER BY TITLE");			
				if(!$resources) 
				{
					$this->log_db_error($this->db->error(), 'get_resources', '');
					
					 return $response;
				}
				
				if($resources AND $resources->num_rows() > 0) {
					
					foreach ($resources->result() as $resource)
					{					
						
						$data = array(
									'resource_id' 	=> $resource->RESOURCE_ID,
									'activity_id'	=> $resource->ACTIVITY_ID,
									'theme_id'		=> $resource->THEME_ID,
									'event_id'		=> $resource->EVENT_ID,
									'title'			=> $resource->TITLE,
									'resource_link'	=> $resource->RESOURCE_LINK,
									'display_order'	=> $resource->DISPLAY_ORDER
								);
						
						array_push($response, $data);
					}
				}
			}
			
			return $response;
		}
		
		public function get_info_desk($event_id) 
		{
			$response = array();
			
			if($event_id) {
							
				$infos = @$this->db->query("SELECT INFO.INFO_ID,
											  INFO.EVENT_ID,
											  INFO.INFO_TITLE,
											  INFO.SHORT_DESC,
											  SUBSTRB(DBMS_LOB.SUBSTR(INFO.LONG_DESC, 4000, 1 ), 1, 4000) LONG_DESC,
											  INFO.STATUS,
											  INFO.SORT_ORDER
											FROM EVENT_INFORMATION_DESK INFO
											WHERE INFO.STATUS = 'Y'
											AND INFO.EVENT_ID = '$event_id'
											--AND INFO.INFO_ID NOT IN (3)
											ORDER BY INFO.SORT_ORDER");			
				if(!$infos) 
				{
					$this->log_db_error($this->db->error(), 'get_info_desk', '');
					
					 return $response;
				}
				
				if($infos AND $infos->num_rows() > 0) {
					
					foreach ($infos->result() as $info)
					{					
						
						$data = array(
									'info_id' 		=> $info->INFO_ID,
									'event_id'		=> $info->EVENT_ID,
									'title'			=> $info->INFO_TITLE,
									'short_desc'	=> $info->SHORT_DESC,
									'long_desc'		=> $info->LONG_DESC,
									'status'		=> $info->STATUS,
									'display_order'	=> $info->SORT_ORDER
								);
						
						array_push($response, $data);
					}
				}
			}
			
			return $response;
		}
		
		# Media coverage
		public function get_press_coverage($event_id) 
		{
			$response = array();
			
			if($event_id) {
							
				$resources = @$this->db->query("SELECT EMC.EVENT_ID,
												  EM.MEDIA_ID,
												  EM.MEDIA_NAME,
												  EM.STATUS MEDIA_STATUS,
												  EMC.COVERAGE_ID,
												  EMC.COVERAGE_TITLE,
												  EMC.RESOURCE_LINK,
												  TO_CHAR(EMC.ENTRY_DATE, 'fmDay, dd-Mon-yyyy hh:mi AM') DISPLAY_DATE,
												  EMC.ENTRY_DATE
												FROM EVENT_MEDIA EM,
												  EVENT_MEDIA_COVERAGE EMC
												WHERE EM.MEDIA_ID              = EMC.MEDIA_ID
												AND EMC.EVENT_ID               = '$event_id'
												AND UPPER(NVL(EM.STATUS, 'Y')) = 'Y'
												ORDER BY EMC.ENTRY_DATE DESC");			
				if(!$resources) 
				{
					$this->log_db_error($this->db->error(), 'get_media_coverage', '');
					
					 return $response;
				}
				
				if($resources AND $resources->num_rows() > 0) {
					
					foreach ($resources->result() as $resource)
					{					
						
						$data = array(
									'event_id' 			=> $resource->EVENT_ID,
									'media_id'			=> $resource->MEDIA_ID,
									'media_name'		=> $resource->MEDIA_NAME,
									'media_status'		=> $resource->MEDIA_STATUS,
									'coverage_id'		=> $resource->COVERAGE_ID,
									'coverage_title'	=> $resource->COVERAGE_TITLE,
									'resource_link'		=> $resource->RESOURCE_LINK,
									'display_date'		=> $resource->DISPLAY_DATE
								);
						
						array_push($response, $data);
					}
				}
			}
			
			return $response;
		}
		
		# Entertainments
		public function get_event_entertainment($event_id, $user_id) 
		{
			$response = array();
			
			if($event_id) {
							
				$ents = @$this->db->query("SELECT EN.EVENT_ID,
											  EN.ENTERTAINMENT_ID,
											  EN.TYPE_ID,
											  EN.IMAGE_NAME,
											  SUBSTRB(DBMS_LOB.SUBSTR(EN.ENTERTAINMENT_DETAIL, 4000, 1 ), 1, 4000) ENTERTAINMENT_DETAIL,
											  EN.STATUS,
											  TO_CHAR(EN.ENTERTAINMENT_DATE, 'fmDay, dd-Mon-yyyy hh:mi AM') DISPLAY_DATE,
											  NVL(EU.USER_ID, -1) USER_ID,
											  EU.REQUEST_STATUS
											FROM bssdata.EVENT_ENTERTAINMENT EN ,
											  (SELECT DISTINCT EVENT_ID,
												ENTERTAINMENT_ID,
												USER_ID,
												STATUS REQUEST_STATUS
											  FROM bssdata.EVENT_ENTERTAINMENT_USERS
											  WHERE EVENT_ID = '$event_id'
											  AND USER_ID    = '$user_id'
											  ) EU
											WHERE EN.ENTERTAINMENT_ID = EU.ENTERTAINMENT_ID (+)
											AND '$user_id'               = EU.USER_ID (+)
											AND EN.EVENT_ID           = '$event_id'
											AND EN.STATUS             = 'Y'");			
				//echo $this->db->last_query(); exit;
				if(!$ents) 
				{
					$this->log_db_error($this->db->error(), 'get_event_entertainments', '');
					
					 return $response;
				}
				
				if($ents AND $ents->num_rows() > 0) {
					
					foreach ($ents->result() as $ent)
					{					
						
						$data = array(
									'event_id' 			=> $ent->EVENT_ID,
									'entertainment_id'	=> $ent->ENTERTAINMENT_ID,
									'user_id'			=> $ent->USER_ID,
									'request_status'	=> $ent->REQUEST_STATUS,
									'type_id'			=> $ent->TYPE_ID,
									//'type_desc'			=> $ent->TYPE_DESC,
									'image_name'		=> 'http://beams.beaconhouse.edu.pk/admin/events/upload/entertainment/'.$ent->IMAGE_NAME,
									'description'		=> $ent->ENTERTAINMENT_DETAIL,
									'status'			=> $ent->STATUS,
									'display_date'		=> $ent->DISPLAY_DATE
								);
						
						array_push($response, $data);
					}
				}
			}
			
			return $response;
		}
		
		# Save event entertainment request
		public function save_event_entertainment_request($event_id, $entertainment_id, $user_id)
		{
			$response = array();
			
			$db2 = $this->load->database('trans', TRUE);
			
			$db2->set('EVENT_ID', $event_id);
			$db2->set('ENTERTAINMENT_ID', $entertainment_id);
			$db2->set('USER_ID', $user_id);
			$db2->set('ENTRY_DATE', '(SELECT SYSDATE FROM DUAL)', FALSE);
			
			$status = @$db2->insert('EVENT_ENTERTAINMENT_USERS');
			
			if(!$status) 
			{
				$this->log_db_error($db2->error(), 'save_event_entertainment_request', '');
				
				return false;
			} else {
				return true;
			}
			
		}
		
		public function get_speaker_itinerary($event_id, $participant_id = -1) 
		{			
			$result = array();
			
			if($participant_id) {
				
				$participant = @$this->db->query("SELECT participant_id,
												  first_name
												  || ' '
												  || last_name participant_name,
												  gender,
												  photo,
												  designation,
												  experties
												FROM event_participant
												WHERE participant_id = '$participant_id'");	
				if(!$participant) 
				{
					$this->log_db_error($this->db->error(), 'get_speaker_itinerary', '');
					
					 return $result;
				}
				
				if($participant AND $participant->num_rows() == 1) {
					
					$row = $participant->row();
					
					$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/'.$row->PHOTO;
							
					if($this->check_file($row->PHOTO, 'http://beams.beaconhouse.edu.pk/admin/events/photo/')) {
						$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/'.$row->PHOTO;
					} else {
						if($row->GENDER == 'F' || $participant->GENDER == 'O') {
							$participant_photo = "http://beams.beaconhouse.edu.pk/admin/events/photo/avatar-female.jpg";
						} else {
							$participant_photo = 'http://beams.beaconhouse.edu.pk/admin/events/photo/avatar-male.jpg';
						}
					}
					
					$data = array(
								'participant_id'	=> $row->PARTICIPANT_ID,
								'participant_name'	=> $row->PARTICIPANT_NAME,
								'gender'			=> $row->GENDER,
								'photo'				=> $participant_photo,
								'designation'		=> $row->DESIGNATION,
								'experties'			=> $row->EXPERTIES,
								'itinerary'			=> array()
							);
					
					$itinerary = @$this->db->query("SELECT EVENT_ID,
													  PARTICIPANT_ID,
													  TO_CHAR(ITINERARY_DATE, 'DD/MM/YYYY') ITINERARY_DATE,
													  TO_CHAR(ITINERARY_DATE, 'HH:MI AM') ITINERARY_TIME,
													  ITINERARY_DESC
													FROM EVENT_SPEAKER_ITINERARY
													WHERE PARTICIPANT_ID = '$participant_id'
													AND EVENT_ID		 = '$event_id'
													ORDER BY ITINERARY_DATE ASC");
					if(!$itinerary) 
					{
						$this->log_db_error($this->db->error(), 'get_speaker_itinerary', '');
					}
					
					if($itinerary AND $itinerary->num_rows() > 0) {
						
						foreach($itinerary->result() as $irow) 
						{
							$idata = array(
										'event_id'			=> $irow->EVENT_ID,
										'participant_id'	=> $irow->PARTICIPANT_ID,
										'itinerary_date'	=> $irow->ITINERARY_DATE,
										'itinerary_time'	=> $irow->ITINERARY_TIME,
										'itinerary_desc'	=> $irow->ITINERARY_DESC
									  );
									  
							array_push($data['itinerary'], $idata);
						}
					}
					
					array_push($result, $data);
				}				
			}
			
			return $result;
		}
		
		public function save_ask_question($data) 
		{			
			if($data) {
				$db2= $this->load->database('trans', TRUE);	
				
				$data['QUESTION_ID'] = 	$db2->query("SELECT NVL(MAX(QUESTION_ID), 0)+1 QUESTION_ID FROM EVENT_ASK_QUESTION")->row()->QUESTION_ID;
				
				$db2->set('ENTRY_DATE', '(SELECT SYSDATE FROM DUAL)', FALSE);
				$status = @$db2->insert('EVENT_ASK_QUESTION', $data);
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'save_ask_question', '');
					
					return false;
				} else {
					return true;
				}
			
			} else {
				return false;
			}
		}
		
		public function get_event_faqs($event_id) 
		{
			$response = array();
			
			if($event_id) {
							
				$faqs = @$this->db->query("SELECT QUESTION_ID,
											  EVENT_ID,
											  QUESTION_TITLE,
											  SUBSTRB(DBMS_LOB.SUBSTR(QUESTION_DETAIL, 4000, 1 ), 1, 4000) QUESTION_DETAIL,
											  STATUS,
											  FAQ
											FROM EVENT_ASK_QUESTION
											WHERE EVENT_ID           = '$event_id'
											AND UPPER(NVL(FAQ, 'Y')) = 'Y'
											ORDER BY QUESTION_TITLE");			
				if(!$faqs) 
				{
					$this->log_db_error($this->db->error(), 'get_event_faqs', '');
				}
				
				if($faqs AND $faqs->num_rows() > 0) {
					
					foreach ($faqs->result() as $faq)
					{					
						
						$data = array(
									'question_id' 		=> $faq->QUESTION_ID,
									'question_title'	=> $faq->QUESTION_TITLE,
									'question_detail'	=> $faq->QUESTION_DETAIL,
									'event_id'			=> $faq->EVENT_ID,
									'status'			=> $faq->STATUS
								);
						
						array_push($response, $data);
					}
				}
			}
			
			return $response;
		}
		
		
		
		
		public function update_sms_response($request_id, $sms_response)
        {
			$db2= $this->load->database('trans', TRUE);	
					
			$db2->set('SMS_RESPONSE', $sms_response);
			$db2->where('REQUEST_ID', $request_id);
			
			@$db2->update("APP_REQUESTS"); 

			/*if($status) {
				return array('status' => $status, 'last_query' => $this->db->last_query());
			} else {
				return array('status' => 'Fail', 'last_query' => $this->db->last_query());
			} */      
		}

		public function set_user_password($sms_number, $user_id, $password)
        {
			$response = array();
			
			$db2= $this->load->database('trans', TRUE);	
			
			// Verify user_id alongwith sms number
			$count = @$db2->query("SELECT COUNT(*) CNT
									FROM BSSDATA.app_users
									WHERE user_id     = '$user_id'
									AND sms_number    = '$sms_number'")->row()->CNT;
			
			if(!$count) 
			{
				$this->log_db_error($db2->error(), 'set_user_password', '');
				
				return false;
			}
				
			if($count AND $count > 0) {

				$db2->set('USER_PASSWORD', $password);
				$db2->set('PASSWORD_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);
				$db2->set('LAST_LOGIN_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);
				
				$db2->where('USER_ID', $user_id);
				$db2->where('SMS_NUMBER', $sms_number);
				
				$status = @$db2->update("APP_USERS"); 
				
				if(!$status) 
				{
					$this->log_db_error($db2->error(), 'set_user_password', '');
					
					return false;
				}
				
				if($status) {
					return true;
				} else {
					return false;	
				}
			} else {
				return false;
			}
        }
				
		public function get_dates($data) 
		{
			$result = array();
			
			foreach($data as $val) {
	
				$from_date 	= $val['from_date'];
				$to_date	= $val['to_date'];
				$remarks	= $val['reason'];
			
				$rs = @$this->db->query("SELECT TRUNC(DATE '$from_date'-1 +lvl) leave_date
										FROM
										  (SELECT level lvl
										  FROM dual
											CONNECT BY level <= (DATE '$to_date'+1 - DATE '$from_date')
										  )
										ORDER BY 1");
				if(!$rs) 
				{
					$this->log_db_error($this->db->error(), 'get_dates', '');
					
					return $result; //false;
				}
			
				if ($rs AND $rs->num_rows() > 0) {
					foreach ($rs->result() as $row) {
						
						$leaves = array('date' => nice_date($row->LEAVE_DATE, 'l, d M-Y'), 'reason' => $remarks); 
						
						array_push($result, $leaves);
					}
				}
			}
			
			return $result;
		}
		
		public function get_current_db_time() 
		{
			$current_db_time = $this->db->query("SELECT TO_CHAR(sysdate, 'YYYYMMDDHH24MISS') current_db_time FROM dual")->row()->CURRENT_DB_TIME;
			
			return $current_db_time;
		}
		
		public function check_file($file_name, $file_path) 
		{	
			if($file_name AND $file_name != "") {
				$file = $file_path.$file_name;
				
				$file_headers = @get_headers($file);
				if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
					return false;
				}
				else {
					return true;
				}	
			} else {
				return false;
			}
		}
		
		public function get_test_query()
		{
			$result = array();
			
			//$this->db->simple_query("SELECT COUNT(*) FROM SAM.v_sam_std_result WHERE acad_year_id = 22");
			$query = @$this->db->query("SELECT count(*)  FROM com_branchess where region_id = 2")->row();
			echo 'xxxxxx'; exit;
			if(!$query) {
				//print_r($this->db->error()); exit;
				$this->log_db_error($this->db->error(), 'get_test_query', 'test error log');
				
				return $result;
			}

			if($query) {

			} else {
				echo 'xxxxxx'; exit;
				//print_r($error); exit;
			}
			
			return $result;
		}
		
		#log DB errors
		public function log_db_error($error, $service_name, $remarks = '') 
		{
			if($error AND count($error) > 0) {
				
				$db2= $this->load->database('trans', TRUE);	
				
				$data['LOG_ID'] 		= $db2->query('SELECT EVENT_APP_SEQ_ERROR_LOGID.nextVal AS LOG_ID FROM dual')->row()->LOG_ID;
				$data['APP_NAME']		= 'STO Events App';
				$data['SERVICE_NAME']	= $service_name;
				$data['ERROR_CODE']		= $error['code'];
				$data['MESSAGE']		= $error['message'];
				$data['OFFSET']			= $error['offset'];
				$data['SQLTEXT']		= $error['sqltext'];
				$data['REMARKS']		= $remarks;
				
				@$db2->insert('EVENT_APP_ERRORS_LOG', $data);
				
				$message  = '<p> SERVICE NAME : ' . $service_name . '</p>';
				$message .= '<p> ERROR CODE : ' . $error['code'] . '</p>';
				$message .= '<p> ERROR MESSAGE : ' . $error['message'] . '<p>';
				$message .= '<p> SQL STATEMENT : <br><br> ' . $error['sqltext'] . '<p>';
				
				@$this->send_email('samee.ullah@bh.edu.pk, shahzad.rafique@bh.edu.pk, jabbar.khan@bh.edu.pk', 'SOT EVENTS APP DATABASE ERROR', $message);
			}
			
		}
		
		# Send email
		private function send_email_old($email_address, $subject, $message, $attachments = '')
		{
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'ssl://smtp.gmail.com';
			$config['smtp_port'] = '465';
			$config['smtp_user'] = 'beaconhouseapp.noreply@bh.edu.pk'; 
			$config['smtp_pass'] = 'Beacon@4545'; 
			$config['mailtype'] = 'html';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard
		
			// Load email library and passing configured values to email library
			$this->load->library('email', $config);
			
			// Sender email address
			$this->email->from('beaconhouseapp.noreply@bh.edu.pk', 'SOT Events App');
			// Receiver email address.for single email
			$this->email->to($email_address);
			// Subject of email
			$this->email->subject($subject);
			// Message in email
			$this->email->message($message);
			
			if($attachments && $attachments != '') {
				$this->email->attach($attachments);
			}
			// It returns boolean TRUE or FALSE based on success or failure
			if($this->email->send()) {
				return true;
			} else {
				return false;
			}
		}
		
		public function send_email($email_address, $subject, $message, $attachments = '', $bcc_email = '')
		{
			
			require_once(APPPATH.'libraries/sendgrid3.2/vendor/autoload.php');
			require_once(APPPATH.'libraries/sendgrid3.2/lib/SendGrid.php');

			$sendgrid = new SendGrid('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');
			$email = new SendGrid\Email();
			$from_email = 'sot.team@bh.edu.pk';
			$from_name = 'SOT Team';
			
			$email->setFrom($from_email);
			$email->setFromName($from_name);
			$email->setSubject($subject);
			$email->setHtml($message);
			if($attachments && $attachments != '') {
				$email->AddAttachment($attachments);
			}
			
			/*if(is_array($email_address))
			{
				foreach($email_address as $eml)
				{
					$email->addTo($eml);
				}
			} else {
				$email->addTo($email_address);
			}*/
			$to_array = explode(',', $email_address);
			foreach ($to_array as $to) {
				$email->addTo($to);
			}
			//$email->AddBcc('shahzad.rafique@bh.edu.pk');
			if($email_address != '')
				$sendgrid->send($email);
			
			return true;
			/*
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'ssl://smtp.gmail.com';
			$config['smtp_port'] = '465';
			$config['smtp_user'] = 'beaconhouseapp.noreply@bh.edu.pk'; 
			$config['smtp_pass'] = 'Beacon@4545'; 
			$config['mailtype'] = 'html';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard
		
			// Load email library and passing configured values to email library
			$this->load->library('email', $config);
			
			// Sender email address
			$this->email->from('beaconhouseapp.noreply@bh.edu.pk', 'Beaconhouse App');
			// Receiver email address.for single email
			$this->email->to($email_address);
			
			if($bcc_email AND $bcc_email != '') {
				$this->email->bcc($bcc_email);
			}
			// Subject of email
			$this->email->subject($subject);
			// Message in email
			$this->email->message($message);
			
			if($attachments && $attachments != '') {
				$this->email->attach($attachments);
			}
			// It returns boolean TRUE or FALSE based on success or failure
			if($this->email->send()) {
				return true;
			} else {
				return false;
			}
			*/
			
		}
		
		public function save_app_access_log($sms_number, $activity_type, $device_type, $device_identifier) 
		{			
			$data = array('SMS_NUMBER' => $sms_number, 'ACTIVITY_TYPE' => $activity_type, 'IP_ADDRESS' => $this->input->ip_address(), 'DEVICE_IDENTIFIER' => $device_identifier, 'DEVICE_TYPE' => $device_type);
			
			$db2 = $this->load->database('trans', TRUE);	
									
			@$db2->insert('APP_ACCESS_LOG', $data);
		}
		
		public function save_user_login_log($user_id, $device_token, $device_identifier, $device_type = 'Android') 
		{
			
			$db2 = $this->load->database('trans', TRUE);	

			$db2->set('USER_ID', $user_id);
			$db2->set('LOGIN_DATE', "(SELECT SYSDATE FROM DUAL)", FALSE);
			$db2->set('IP_ADDRESS', $this->input->ip_address());
			$db2->set('DEVICE_TOKEN', $device_token);
			$db2->set('DEVICE_IDENTIFIER', $device_identifier);
			$db2->set('DEVICE_TYPE', $device_type);
			$db2->set('AUTH_TOKEN', md5($user_id.time()));
			
			$status = @$db2->insert("EVENT_APP_LOGIN_LOG");
			
			return true;
		}
		
		public function get_countries() 
		{
			
			$countries = $this->db->query("SELECT country_id, country_name FROM COMMON.com_countries ORDER BY country_name");
			
			$response = array();
			
			foreach ($countries->result() as $country)
			{
				$data = array(
							'country_id' 		=> $country->COUNTRY_ID,
							'country_name'		=> $country->COUNTRY_NAME,
							'country_states'	=> $this->get_states($country->COUNTRY_ID)
						);
				array_push($response, $data);
			}
			
			return $response;
		}
		
		public function get_states($country_id) 
		{
			
			$states = $this->db->query("SELECT state_id, state_name FROM COMMON.com_states WHERE country_id = $country_id ORDER BY state_name");
			
			$response = array();
			
			foreach ($states->result() as $state)
			{
				$data = array(
							'state_id' 		=> $state->STATE_ID,
							'state_name'	=> $state->STATE_NAME,
							'state_cities'	=> $this->get_cities($country_id, $state->STATE_ID)
						);
				array_push($response, $data);
			}
			
			return $response;
		}
		
		public function get_cities($country_id, $state_id) 
		{
			
			$cities = $this->db->query("SELECT city_id, city_name FROM COMMON.com_cities WHERE country_id = $country_id and state_id = $state_id ORDER BY city_name");
			
			$response = array();
			
			foreach ($cities->result() as $city)
			{
				$data = array(
							'city_id' 		=> $city->CITY_ID,
							'city_name'		=> $city->CITY_NAME,
							'city_towns'	=> $this->get_towns($country_id, $state_id, $city->CITY_ID)
						);
				array_push($response, $data);
			}
			
			return $response;
		}
		
		public function get_towns($country_id, $state_id , $city_id) 
		{
			
			$towns = $this->db->query("SELECT town_id, town_name FROM COMMON.com_towns WHERE country_id = $country_id and state_id = $state_id and city_id = $city_id ORDER BY town_name");
			
			$response = array();
			
			foreach ($towns->result() as $town)
			{
				$data = array(
							'town_id' 	=> $town->TOWN_ID,
							'town_name'	=> $town->TOWN_NAME
						);
				array_push($response, $data);
			}
			
			return $response;
		}
		
		public function get_associations() 
		{
			$response = array();
			
			$assocs = $this->db->query("SELECT ASSOCIATION_ID,
										  ASSOCIATION_NAME,
										  STATUS
										FROM EVENT_BSS_ASSOCIATION
										WHERE UPPER(NVL(STATUS, 'Y')) = 'Y'
										ORDER BY ASSOCIATION_ID");

			foreach ($assocs->result() as $assoc)
			{
				$data = array(
							'association_id' 	=> $assoc->ASSOCIATION_ID,
							'association_name'	=> $assoc->ASSOCIATION_NAME
						);
				array_push($response, $data);
			}
			
			return $response;
		}
		
		public function get_sources() 
		{
			$response = array();
			
			$sources = $this->db->query("SELECT SOURCE_ID,
										  SOURCE_DESC,
										  STATUS
										FROM EVENT_SOURCE
										WHERE NVL(STATUS, 1) = 1 AND SOURCE_ID != 99
										ORDER BY SOURCE_DESC ASC");

			foreach ($sources->result() as $source)
			{
				$data = array(
							'source_id' 	=> $source->SOURCE_ID,
							'source_name'	=> $source->SOURCE_DESC
						);
				array_push($response, $data);
			}

			$sources = $this->db->query("SELECT SOURCE_ID,
										  SOURCE_DESC,
										  STATUS
										FROM EVENT_SOURCE
										WHERE NVL(STATUS, 1) = 1 AND SOURCE_ID = 99");

			foreach ($sources->result() as $source)
			{
				$data = array(
							'source_id' 	=> $source->SOURCE_ID,
							'source_name'	=> $source->SOURCE_DESC
						);
				array_push($response, $data);
			}
			
			return $response;
		}
		
		public function get_user_source($user_id) 
		{
			$response = array('selected_sources' => array());
			
			$sources = $this->db->query("SELECT SOURCE_ID, OTHER_DESC OTHER_SOURCE
										FROM EVENT_USER_SOURCE
										WHERE USER_ID = '$user_id'
										ORDER BY SOURCE_ID");
			$other_source = '';
			foreach ($sources->result() as $source)
			{
				$data = $source->SOURCE_ID;
				
				array_push($response['selected_sources'], $data);
				
				if($source->SOURCE_ID == 99) 
				{
					$other_source = $source->OTHER_SOURCE;
				}
			}
			$response['other_source'] = $other_source;
			
			return $response;
		}
		
		public function update_sources($sources = array(), $user_id = '', $event_id = '', $other_desc = ''){

			$db2= $this->load->database('trans', TRUE);	

			$status = '';			

			$db2->where('USER_ID', $user_id);
			$db2->where('EVENT_ID', $event_id);
			@$db2->delete("EVENT_USER_SOURCE");

			if($sources AND count($sources) > 0)
			{
				foreach($sources as $src) 
				{
					$source = array(
								'USER_ID'		=> $user_id,
								'SOURCE_ID'		=> $src,
								'OTHER_DESC'	=> ($src == 99) ? $other_desc : '',
								'EVENT_ID'		=> $event_id
							  );
					
					$status = @$db2->insert('EVENT_USER_SOURCE', $source);
					
				}
			}

			// $count = @$db2->query("SELECT *
			// 						FROM BSSDATA.event_user_source									
			// 						WHERE user_id = $user_id ");
			// print_r($count->result()); exit;							

			return $status;

		}
		
		public function get_pending_poll_eval_count($user_id, $event_id)
		{
			$counts = $this->db->query("SELECT SUM(
										  CASE
											WHEN (POLL_EXISTS  = 'Y'
											AND POLL_ACTIVE    = 'Y'
											AND POLL_SUBMITTED = 'N')
											THEN 1
											ELSE 0
										  END) POLLS_COUNT,
										  SUM(
										  CASE
											WHEN (EVALUATION_EXISTS  = 'Y'
											AND EVALUATION_ACTIVE    = 'Y'
											AND EVALUATION_SUBMITTED = 'N')
											THEN 1
											ELSE 0
										  END) EVALS_COUNT
										FROM
										  (SELECT
											(SELECT DECODE(COUNT(*), 0, 'N', 'Y')
											FROM event_poll
											WHERE schedule_id = es.schedule_id
											  --AND upper(NVL(status, 'N')) = 'N'
											) poll_exists,
											(SELECT DECODE(COUNT(*), 0, 'N', 'Y')
											FROM event_poll
											WHERE schedule_id           = es.schedule_id
											AND upper(NVL(status, 'N')) = 'Y'
											) poll_active,
											(SELECT DECODE(COUNT(*), 0, 'N', 'Y')
											FROM EVENT_POLL_RESPONSE
											WHERE USER_ID = eus.user_id
											AND poll_id  IN
											  (SELECT poll_id FROM event_poll WHERE schedule_id = es.schedule_id
											  )
											) poll_submitted,
											(SELECT DECODE(COUNT(*), 0, 'N', 'Y')
											FROM event_evaluation_sessions
											WHERE schedule_id = es.schedule_id
											  --AND upper(NVL(status, 'N')) = 'N'
											) evaluation_exists,
											(SELECT DECODE(COUNT(*), 0, 'N', 'Y')
											FROM event_evaluation_sessions
											WHERE schedule_id           = es.schedule_id
											AND upper(NVL(status, 'N')) = 'Y'
											) evaluation_active,
											(SELECT DECODE(COUNT(*), 0, 'N', 'Y')
											FROM EVENT_EVALUATION_RESPONSE
											WHERE USER_ID   = eus.user_id
											AND schedule_id = es.schedule_id
											) evaluation_submitted
										  FROM event_schedule es,
											event_user_schedule eus
										  WHERE es.schedule_id = eus.schedule_id
										  AND es.event_id      = '$event_id'
										  AND eus.user_id      = '$user_id'
										  )")->row();
			
			$result = array('polls_count' => (int)$counts->POLLS_COUNT, 'evals_count' => (int)$counts->EVALS_COUNT);
			
			return $result;							  
			
		}
		
		public function func_log_user_event($db, $user_id){			
			
			$sql = "SELECT DECODE(
					  (SELECT MAX(event_id) FROM events WHERE upper(TRIM(event_catagory) ) = 'SOT'
					  AND active                                                           = 'Y'
					  ),NULL,
					  (SELECT MAX(event_id)
					  FROM events
					  WHERE upper(TRIM(event_catagory) ) = 'SOT'
					  ),
					  (SELECT MAX(event_id)
					  FROM events
					  WHERE upper(TRIM(event_catagory) ) = 'SOT'
					  AND active                         = 'Y'
					  ) ) event_id
					FROM dual";
			
			$event_id = $db->query($sql)->row()->EVENT_ID;
			
			if($event_id)
			{
				$sql = "
				
					SELECT
						COUNT(*) CNT
					FROM
						event_user_registration
					WHERE
						event_id = '".$event_id."'
						AND user_id = '".$user_id."'
				";
				
				$rcount = $db->query($sql)->row()->CNT;
				
				if($rcount == 0)
				{
					$ip_address = $this->input->ip_address();
					
					$data = array(
								
								'EVENT_ID' 			=> $event_id,
								'USER_ID'			=> $user_id,
								'REGISTER_SOURCE'	=> 'M',
								'ENTRY_IP'			=> $this->input->ip_address()
							);
							
					@$db->insert('EVENT_USER_REGISTRATION', $data);

				}
			}
		}
}